;; This file contains the capability-aware instructions for CHERI-Y86-64
(in-package "ACL2")

(include-book "std/util/bstar" :dir :system)
(include-book "centaur/gl/gl" :dir :system)
(include-book "y86-64")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                  ;;
;;                        Capability-Inspection                     ;;
;;                                                                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; CHERI-RISC-V format
;; | 31 - 25 | 24 - 20 | 19 - 15 | 14 - 12 | 11 - 07 | 06 - 0 |
;; |---------|---------|---------|---------|---------|--------|
;; |   0x7f  |   0xx   |   cs1   |   0x0   |    rd   |  0x5b  |

;; CHERI-Y86-64 format for capability-inspection 
;; |      Byte 0       |  Byte 1 |      Byte 2       |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 |
;; |---------|---------|---------|-------- |---------|
;; |   0xc   |   0x0   |   0xxx  |   cs1   |    rd   |
;;                                    rA        rB
;; CGetPerm rd, cs1; 0xxx = 0x00
(in-theory (enable acap-p get-reg-capability))
(defund y86-cPerm-to-rB (y86-64)
  ;; Get permissions capability in rA, store in rB
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 3-byte instruction?
       ((unless (< pc *2^64-4*))
        (!ms (list :at-location pc
                   :instruction 'cPerm-to-rB
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'cPerm-to-rB
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability
       (cs1 (get-reg-capability rA y86-64))
       ;; Get permissions
       (cPerm  (n128 (acap->perms cs1)))

       ;; Store result and flags
       (y86-64 (!rgfi rB cPerm y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 3) y86-64)))
    y86-64))

(defund y86-cType-to-rB (y86-64)
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :guard-hints (("Goal" :use ((:instance return-type-of-acap->ot
                                                         (x (get-reg-capability
                                                             (logand (ash (rm08 (+ 2 (rip y86-64)) y86-64) -4)
                                                                     15)
                                                             y86-64))))
                                        :in-theory (disable return-type-of-acap->ot)))))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 3-byte instruction?
       ((unless (< pc *2^64-4*))
        (!ms (list :at-location pc
                   :instruction 'cType-to-rB
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'cType-to-rB
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability
       (cs1 (get-reg-capability rA y86-64))
       ;; Get type 
       (cType (n128 (acap->ot cs1)))

       ;; Store result and flags
       (y86-64 (!rgfi rB cType y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 3) y86-64)))
    y86-64))

(defund y86-cBase-to-rB (y86-64)
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 3-byte instruction?
       ((unless (< pc *2^64-4*))
        (!ms (list :at-location pc
                   :instruction 'cBase-to-rB
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'cBase-to-rB
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability
       (cs1 (get-reg-capability rA y86-64))
       ;; Get base
       (cBase (n128 (acap->base cs1)))

       ;; Store result and flags
       (y86-64 (!rgfi rB cBase y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 3) y86-64)))
    y86-64))
  
(defund y86-cLen-to-rB (y86-64)
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 3-byte instruction?
       ((unless (< pc *2^64-4*))
        (!ms (list :at-location pc
                   :instruction 'cLen-to-rB
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'cLen-to-rB
                   :ra rA :rb rB 
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability
       (cs1 (get-reg-capability rA y86-64))
       ;; Get len
       (cLen (n128 (acap->len cs1)))
       (cBase (n128 (acap->base cs1)))
       ((unless (<= 0 cLen))
        (!ms (list :at-location pc
                   :instruction 'cLen-to-rB
                   :ra rA :rb rB 
                   :top (+ cBase cLen) :base cBase
                   :reason 'negative-length)
             y86-64))

       ;; Store result and flags
       (y86-64 (!rgfi rB cLen y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 3) y86-64)))
    y86-64))

(defund y86-cTag-to-rB (y86-64)
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 3-byte instruction?
       ((unless (< pc *2^64-4*))
        (!ms (list :at-location pc
                   :instruction 'cTag-to-rB
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'cTag-to-rB
                   :ra rA :rb rB 
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability
       (cs1 (get-reg-capability rA y86-64))
       ;; Get tag
       (cTag (n128 (acap->tag cs1)))

       ;; Store result and flags
       (y86-64 (!rgfi rB cTag y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 3) y86-64)))
    y86-64))

;; CGetOffset rd, cs1; 0xxx = 0x06
(defund y86-gcOff (y86-64)
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 3-byte instruction?
       ((unless (< pc *2^64-4*))
        (!ms (list :at-location pc
                   :instruction 'gcOff
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'gcOff
                   :ra rA :rb rB 
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability
       (cs1 (get-reg-capability rA y86-64))
       ;; Get offset
       (cOffset (n128 (cGetOffset cs1)))

       ;; Store result and flags
       (y86-64 (!rgfi rB cOffset y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 3) y86-64)))
    y86-64))

;; CGetFlags rd, cs1; 0xxx = 0x07
(defund y86-gcflags (y86-64)
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 3-byte instruction?
       ((unless (< pc *2^64-4*))
        (!ms (list :at-location pc
                   :instruction 'gcFlags
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'gcFlags
                   :ra rA :rb rB 
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability
       (cs1 (get-reg-capability rA y86-64))
       ;; Get flags
       (cFlags (n128 (cGetFlags cs1)))

       ;; Store result and flags
       (y86-64 (!rgfi rB cFlags y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 3) y86-64)))
    y86-64))

;; CGetHi rd, cs1; 0xxx = 0x08
(defund y86-gcHi (y86-64)
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 3-byte instruction?
       ((unless (< pc *2^64-4*))
        (!ms (list :at-location pc
                   :instruction 'gcHi
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'gcHi
                   :ra rA :rb rB 
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability
       (cs1 (get-reg-capability rA y86-64))
       ;; Get high half
       (cHi (cGetHi cs1))

       ;; Store result and flags
       (y86-64 (!rgfi rB cHi y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 3) y86-64)))
    y86-64))


;; CGetTop rd, cs1; 0xxx = 0x09
;; GCLIM
(defund y86-gcLim (y86-64)
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 3-byte instruction?
       ((unless (< pc *2^64-4*))
        (!ms (list :at-location pc
                   :instruction 'gcLim
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'gcLim
                   :ra rA :rb rB 
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability
       (cs1 (get-reg-capability rA y86-64))
       ;; Get limit aka top
       (cLim (n128 (cGetTop cs1)))

       ;; Store result and flags
       (y86-64 (!rgfi rB cLim y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 3) y86-64)))
    y86-64))


(in-theory (disable get-reg-capability))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                  ;;
;;                      Capability-Modification                     ;;
;;                                                                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; CHERI-RISC-V format
;; | 31 - 25 | 24 - 20 | 19 - 15 | 14 - 12 | 11 - 07 | 06 - 0 |
;; |---------|---------|---------|---------|---------|--------|
;; |   0xx   |   cs2   |   cs1   |   0x0   |    cd   |  0x5b  |

;; CHERI-Y86-64 format for capability-modification

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   cs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CSeal cd cs1 cs2; 0xxx = 0x00
(in-theory (enable get-reg-capability))
(defund y86-cSeal (y86-64)
  ;; Copy capability rA into rC, and seal rC with otype equal to
  ;; address / base / lower bound of capability rB
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cSeal
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; cs1
       (rA (n04 (ash rA-rB -4)))         ;; cs2
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cSeal
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capabilities
       (cs1 (get-reg-capability rA y86-64))
       (cs2 (get-reg-capability rB y86-64))

       ;; Check if the capabilities have valid tags
       ((unless (and (cValidp cs1) (cValidp cs2)))
        (!ms (list :at-location pc
                   :instruction 'cSeal
                   :ra cs1 :rb cs2
                   :reason 'capability-not-valid)
             y86-64))

       ;; Check if the capabilities are sealed
       ((if (or (cSealedp cs1) (cSealedp cs2)))
        (!ms (list :at-location pc
                   :instruction 'cSeal
                   :ra cs1 :rb cs2
                   :reason 'capability-sealed)
             y86-64))

       ;; Check if cs2 has permission Permit Seal
       ((unless (cPermitSealp cs2))
       (!ms (list :at-location pc
                  :instruction 'cSeal
                  :rb cs2
                  :reason 'capability-does-not-permit-seal)
            y86-64))

       ;; Check if cs2.base < cs2.address <= cs2.top
       (addr (cGetAddress cs2))
       ((unless (and (<= (acap->base cs2) addr)
                     (< addr (+ (acap->base cs2)
                                (acap->len cs2)))))
       (!ms (list :at-location pc
                  :instruction 'cSeal
                  :rb cs2
                  :addr addr
                  :reason 'capability-address-not-within-bounds)
            y86-64))

       ;; Check if cs2.address > max_otype
       ((unless (<= addr *max_otype*))
       (!ms (list :at-location pc
                  :instruction 'cSeal
                  :rb cs2
                  :addr addr
                  :reason 'capability-address->-max_otype)
            y86-64))

       ;; Get address of cs2 for new otype
       (ot  (aot-to-mot addr))
       ;; Update otype of cs1 to new otype
       (cs1 (change-acap cs1 :ot ot))
       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap cs1))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))


;; CHERI-RISC-V format
;; | 31 - 25 | 24 - 20 | 19 - 15 | 14 - 12 | 11 - 07 | 06 - 0 |
;; |---------|---------|---------|---------|---------|--------|
;; |   0xx   |   cs2   |   cs1   |   0x0   |    cd   |  0x5b  |

;; CHERI-Y86-64 format for capability-modification

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   cs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CUnseal cd cs1 cs2; 0xxx = 0x01
(defund y86-cUnseal (y86-64)
  ;; Copy capability rA into rC, and seal rC with otype equal to
  ;; address / base / lower bound of capability rB
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cSeal
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; cs1
       (rA (n04 (ash rA-rB -4)))         ;; cs2
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cUnseal
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capabilities
       (cs1 (get-reg-capability rA y86-64))
       (cs2 (get-reg-capability rB y86-64))

       (cs2Addr (cGetAddress cs2))
       (cs2Top  (cGetTop     cs2))
       (cs2Base (cGetBase    cs2))
       ;(cs2Tag  (cGetTag     cs2))

       (cs1ot   (cGetOtype   cs1))

       (permitted (and (cValidp  cs2)
                       (cSealedp cs1)
                       (not (cSealedp cs2))
                       (not (cReservedOTypep cs1))
                       (equal cs2Addr cs1ot)
                       (cPermitUnsealp cs2)
                       (<= cs2Base cs2Addr)
                       (<  cs2Addr cs2top)))
       (newGlobal (and (cPermitGlobal cs1) (cPermitGlobal cs2)))
       (newPerms  (setGlobal (acap->perms cs1) newGlobal))
       (newCap    (change-acap cs1 :perms newPerms))
       (newCap    (if permitted newCap (change-acap newCap :tag 0)))

       ;; Turn newCap into a 128-bit nat
       (raw (acap-to-mcap newCap))
       ;; Store newCap in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))

    y86-64))

;; CHERI-RISC-V format
;; | 31 - 25 | 24 - 20 | 19 - 15 | 14 - 12 | 11 - 07 | 06 - 0 |
;; |---------|---------|---------|---------|---------|--------|
;; |   0xx   |   rs2   |   cs1   |   0x0   |    cd   |  0x5b  |

;; CHERI-Y86-64 format for capability-modification

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   rs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CAndPerm cd cs1 rs2; 0xxx = 0x02
(defund y86-cAndPerm (y86-64)
  ;; bitwise-and perms  of rA with (rs2[0:cap_hperms_width-1]),
  ;; Copy updated capability rA into rC
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cAndPerm
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; rs2
       (rA (n04 (ash rA-rB -4)))         ;; cs1 
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cAndPerm
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability and content of rs2
       (cs1 (get-reg-capability rA y86-64))
       (rs2 (n128 (rgfi rB y86-64))) 

       ;; Check if the capability has valid tag
       ((unless (cValidp cs1))
        (!ms (list :at-location pc
                   :instruction 'cAndPerm
                   :ra cs1 :rb rs2
                   :reason 'capability-not-valid)
             y86-64))

       ;; Check if the capability is sealed
       ((if (cSealedp cs1))
        (!ms (list :at-location pc
                   :instruction 'cAndPerm
                   :ra cs1 :rb rs2
                   :reason 'capability-sealed)
             y86-64))

       ;; Get address of cs1 for perms
       (perms  (acap->perms cs1))
       ;; compute new perms by logand
       (new-perms (logand (nb-ash 0 *pw* rs2) perms))
       ;; Update perms of cs1 to new perms 
       (cs1 (change-acap cs1 :perms new-perms))
       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap cs1))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))


;; CHERI-RISC-V format
;; | 31 - 25 | 24 - 20 | 19 - 15 | 14 - 12 | 11 - 07 | 06 - 0 |
;; |---------|---------|---------|---------|---------|--------|
;; |   0xx   |   rs2   |   cs1   |   0x0   |    cd   |  0x5b  |

;; CHERI-Y86-64 format for capability-modification

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   rs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CAndPerm cd cs1 rs2; 0xxx = 0x04
(defund y86-cSetOffset (y86-64)
  ;; update offset of rA with (rA->offset + rs2), copy updated capability
  ;; rA into rC.
  ;; If the resulting capability cannot be represented exactly,
  ;; set rC->tag to 0, rC->addr remains unchanged, and all other fields
  ;; of rC to the values of the corresponding fields in rA.
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cSetOffset
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; rs2
       (rA (n04 (ash rA-rB -4)))         ;; cs1 
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cSetOffset
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability and content of rs2
       (cs1 (get-reg-capability rA y86-64))
       (rs2 (n128 (rgfi rB y86-64))) 

       ;; Check if the capability is sealed
       ((if (and (cValidp cs1)
                 (cSealedp cs1)))
        (!ms (list :at-location pc
                   :instruction 'cSetOffset
                   :ra cs1 :rb rs2
                   :reason 'capability-sealed)
             y86-64))

       ;; Get offset of cs1
       (offset (cGetOffset cs1))
       ((cons success new-cap)
        (cSetOffset cs1 (+ offset rs2)))
       (new-cap (if success
                    new-cap
                    (cInvalidateCap new-cap)))

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))


;; CHERI-RISC-V format
;; | 31 - 25 | 24 - 20 | 19 - 15 | 14 - 12 | 11 - 07 | 06 - 0 |
;; |---------|---------|---------|---------|---------|--------|
;; |   0xx   |   rs2   |   cs1   |   0x0   |    cd   |  0x5b  |

;; CHERI-Y86-64 format for capability-modification

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   rs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CSetAddr cd cs1 rs2; 0xxx = 0x05
(defund y86-cSetAddr (y86-64)
  ;; update addr of rA with rs2, copy updated rA into rC.
  ;; If the resulting capability cannot be represented exactly,
  ;; set rC->tag to 0, rC->addr = rs2, and all other fields
  ;; of rC to the values of the corresponding fields in rA.
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cSetAddr
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; rs2
       (rA (n04 (ash rA-rB -4)))         ;; cs1 
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cSetAddr
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability and content of rs2
       (cs1 (get-reg-capability rA y86-64))
       (rs2 (n128 (rgfi rB y86-64))) 

       ;; Check if the capability is sealed
       ((if (and (cValidp cs1)
                 (cSealedp cs1)))
        (!ms (list :at-location pc
                   :instruction 'cSetAddr
                   :ra cs1 :rb rs2
                   :reason 'capability-sealed)
             y86-64))

       ;; try set addr of cs1 to rs2
       ((cons success new-cap)
        (cSetAddr cs1 rs2))
       (new-cap (if success
                    new-cap
                    (cInvalidateCap new-cap)))

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))

;; | 31 - 25 | 24 - 20 | 19 - 15 | 14 - 12 | 11 - 07 | 06 - 0 |
;; |---------|---------|---------|---------|---------|--------|
;; |   0xx   |   rs2   |   cs1   |   0x0   |    cd   |  0x5b  |

;; CHERI-Y86-64 format for capability-modification

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   rs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CIncOffset cd cs1 rs2; 0xxx = 0x06
(defund y86-cIncOffset (y86-64)
  ;; update addr of rA with (rA->addr + rs2), copy updated rA into rC.
  ;; If the resulting capability cannot be represented exactly,
  ;; set rC->tag to 0, rC->addr = rA->addr + rs2, and all other fields
  ;; of rC to the values of the corresponding fields in rA.
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cSetIncOffset
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; rs2
       (rA (n04 (ash rA-rB -4)))         ;; cs1 
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cSetIncOffset
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability and content of rs2
       (cs1 (get-reg-capability rA y86-64))
       (rs2 (n128 (rgfi rB y86-64))) 

       ;; Check if the capability is sealed
       ((if (and (cValidp cs1)
                 (cSealedp cs1)))
        (!ms (list :at-location pc
                   :instruction 'cSetIncOffset
                   :ra cs1 :rb rs2
                   :reason 'capability-sealed)
             y86-64))

       ;; try set addr of cs1 to rs2
       ((cons success new-cap)
        (cIncOffset cs1 rs2))
       (new-cap (if success
                    new-cap
                    (cInvalidateCap new-cap)))

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))

;; Format for CHERI-y86-64 Imm instructions
;;
;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |    Byte 4    |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |    32 - 40   |
;; |---------|---------|---------|-------- |---------|---------|-------- |--------------|
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   cd    |      imm[0:8]     |   imm[8:16]  |
;;                                    rA       rC                    immm
;;
;; Note: CHERI-RISC-V specifies imm[11:0], CHERI-x86 does not have an imm
;; offset instruction, but imm bounds specifies imm32
;;
;; CIncOffsetImm cd cs1 imm; 0xxx = 0x07
(defund y86-cIncOffsetImm (y86-64)
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 5-byte instruction?
       ((unless (< pc *2^64-6*))
        (!ms (list :at-location pc
                   :instruction 'cIncOffsetImm
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rC (rm08 (+ pc 2) y86-64))
       (rC (n04 rA-rC))                  ;; cd
       (rA (n04 (ash rA-rC -4)))         ;; cs1 
       (imm (rm16 (+ pc 3) y86-64))      ;; imm

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cIncOffsetImm
                   :ra rA :rb rC
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability and content of rs2
       (cs1 (get-reg-capability rA y86-64))

       ;; Check if the cs1 capability has valid tag
       ((unless (cValidp cs1))
        (!ms (list :at-location pc
                   :instruction 'cIncOffsetImm
                   :ra cs1 :rb rC
                   :reason 'capability-not-valid)
             y86-64))

       ;; Check if the capability is sealed
       ((if (cSealedp cs1))
        (!ms (list :at-location pc
                   :instruction 'cIncOffsetImm
                   :ra cs1 :rb rC
                   :reason 'capability-sealed)
             y86-64))
       
       ;; Get current address
       (cs1-addr (cGetAddress cs1))
       ;; New address = cs1.address + imm
       (new-addr (n64+ cs1-addr imm))
       ;; Try to update address of cs1
       ((cons success new-cap) (cSetAddr cs1 new-addr))
       (new-cap (if success new-cap (cSetTag new-cap 0)))

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 5) y86-64)))
    y86-64))


;; | 31 - 25 | 24 - 20 | 19 - 15 | 14 - 12 | 11 - 07 | 06 - 0 |
;; |---------|---------|---------|---------|---------|--------|
;; |   0xx   |   rs2   |   cs1   |   0x0   |    cd   |  0x5b  |

;; CHERI-Y86-64 format for capability-modification

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   rs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CSetBounds cd cs1 rs2; 0xxx = 0x08
(defund y86-cSetBounds (y86-64)
  ;; update base of rA to rA->addr, length of rA to rB. Store result in rC
  ;; if rA->addr < rA->base, round down base to addr
  ;; if rA->top < rA->base/addr + length, round up length to rA->top - rA->base/addr
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cSetBounds
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; rs2
       (rA (n04 (ash rA-rB -4)))         ;; cs1 
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cSetBounds
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability and content of rs2
       (cs1 (get-reg-capability rA y86-64))
       (rs2 (n128 (rgfi rB y86-64))) 

       ;; Check if the cs1 capability has valid tag
       ((unless (cValidp cs1))
        (!ms (list :at-location pc
                   :instruction 'cSetBounds
                   :ra cs1 :rb rs2
                   :reason 'capability-not-valid)
             y86-64))

       ;; Check if the capability is sealed
       ((if (cSealedp cs1))
        (!ms (list :at-location pc
                   :instruction 'cSetBounds
                   :ra cs1 :rb rs2
                   :reason 'capability-sealed)
             y86-64))
       
       (n-base (cGetAddress cs1))
       ;; Check if the new bounds are valid
       ((unless (cInCapBounds cs1 n-base rs2))
        (!ms (list :at-location pc
                   :instruction 'cSetBounds
                   :ra cs1 :rb rs2
                   :reason 'capability-length-not-valid)
             y86-64))

       (new-cap (cdr (cSetBounds cs1 n-base (+ n-base rs2) (E rs2))))  ;; offset-updated cap

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))

;; CHERI-Y86-64 format for capability-modification

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   rs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CSetBounds cd cs1 rs2; 0xxx = 0x09
(defund y86-cSetBoundsExact (y86-64)
  ;; same as cSetBounds, except for that the bounds have to be exact.
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cSetBoundsExact
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; rs2
       (rA (n04 (ash rA-rB -4)))         ;; cs1 
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cSetBoundsExact
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability and content of rs2
       (cs1 (get-reg-capability rA y86-64))
       (rs2 (n128 (rgfi rB y86-64))) 

       ;; Check if the cs1 capability has valid tag
       ((unless (cValidp cs1))
        (!ms (list :at-location pc
                   :instruction 'cSetBoundsExact
                   :ra cs1 :rb rs2
                   :reason 'capability-not-valid)
             y86-64))

       ;; Check if the capability is sealed
       ((if (cSealedp cs1))
        (!ms (list :at-location pc
                   :instruction 'cSetBoundsExact
                   :ra cs1 :rb rs2
                   :reason 'capability-sealed)
             y86-64))
       
       (n-base (cGetAddress cs1))
       ;; Check if the new bounds are valid
       ((unless (cInCapBounds cs1 n-base rs2))
        (!ms (list :at-location pc
                   :instruction 'cSetBoundsExact
                   :ra cs1 :rb rs2
                   :reason 'capability-length-not-valid)
             y86-64))

       ((cons exactp new-cap)
        (cSetBounds cs1 n-base (+ n-base rs2) (E rs2)))

       ;; Check if bounds can be represented exactly
       ((unless exactp)
        (!ms (list :at-location pc
                   :instruction 'cSetBoundsExact
                   :ra cs1 :rb rs2
                   :reason 'capability-inexact-bounds)
             y86-64))
       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))


;; Format for CHERI-y86-64 Imm instructions
;;
;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |    Byte 4    |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |    32 - 40   |
;; |---------|---------|---------|-------- |---------|---------|-------- |--------------|
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   cd    |     uimm[0:8]     |  uimm[8:16]  |
;;                                    rA       rC                   uimmm
;;
;; Note: CHERI-RISC-V specifies uimm[11:0], CHERI-x86 specifies imm32
;;
;; CSetBoundsImm cd cs1 rs2; 0xxx = 0x0a
(defund y86-cSetBoundsImm (y86-64)
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 5-byte instruction?
       ((unless (< pc *2^64-6*))
        (!ms (list :at-location pc
                   :instruction 'cSetBoundsImm
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rC (rm08 (+ pc 2) y86-64))
       (rC (n04 rA-rC))                  ;; cd
       (rA (n04 (ash rA-rC -4)))         ;; cs1 
       (imm (rm16 (+ pc 3) y86-64))      ;; imm

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cSetBoundsImm
                   :ra rA :rb rC
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability and content of rs2
       (cs1 (get-reg-capability rA y86-64))

       ;; Check if the cs1 capability has valid tag
       ((unless (cValidp cs1))
        (!ms (list :at-location pc
                   :instruction 'cSetBoundsImm
                   :ra cs1 :rb rC
                   :reason 'capability-not-valid)
             y86-64))

       ;; Check if the capability is sealed
       ((if (cSealedp cs1))
        (!ms (list :at-location pc
                   :instruction 'cSetBoundsImm
                   :ra cs1 :rb rC
                   :reason 'capability-sealed)
             y86-64))
       
       ;; New base is address of cs1
       (new-base (cGetAddress cs1))

       ;; Check if the new bounds are valid
       ((unless (cInCapBounds cs1 new-base imm))
        (!ms (list :at-location pc
                   :instruction 'cSetBoundsImm
                   :ra cs1 :rb rC
                   :reason 'capability-length-not-valid)
             y86-64))

       ;; Try to update bounds of cs1
       ((cons success new-cap) (cSetBounds cs1 new-base imm (- imm new-base)))
       (new-cap (if success new-cap (cSetTag new-cap 0)))

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 5) y86-64)))
    y86-64))

;; CHERI-Y86-64 format for capability-modification

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   rs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CClearTag cd cs1; 0xxx = 0xb
(defund y86-cClearTag (y86-64)
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cClearTag
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rA (n04 (ash rA-rB -4)))         ;; cs1 
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cClearTag
                   :ra rA :rb rC
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capability 
       (cs1 (get-reg-capability rA y86-64))

       (new-cap (cSetTag cs1 0))

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))

;; CHERI-Y86-64 format for capability-modification

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   cs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CBuildCap cd cs1 cs2; 0xxx = 0x0c
(defund y86-cBuildCap (y86-64)
  ;; build capability from cs1 replaced by the cap fields of cs2, and
  ;; store it in cd. If cs2 is a sentry, then cs is also sealed as a sentry
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cBuildCap
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; cs2
       (rA (n04 (ash rA-rB -4)))         ;; cs1 
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cBuildCap
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capabilities
       (cs1 (get-reg-capability rA y86-64))
       (cs2 (get-reg-capability rB y86-64))

       ;; Check if the cs1 capability has valid tag
       ((unless (cValidp cs1))
        (!ms (list :at-location pc
                   :instruction 'cBuildCap
                   :ra cs1 :rb cs2
                   :reason 'capability-not-valid)
             y86-64))

       ;; Check if the capability is sealed
       ((if (cSealedp cs1))
        (!ms (list :at-location pc
                   :instruction 'cBuildCap
                   :ra cs1 :rb cs2
                   :reason 'capability-sealed)
             y86-64))

       ;; raises exception if
       ;; 1) cs2->base < cs1->base, OR
       ;; 2) cs2->top > cs1->top, OR
       ;; 3) cs2->base < cs2->top, OR
       (cs1-base (cGetBase cs1))
       (cs1-top (cGetTop cs1))
       (cs2-base (cGetBase cs2))
       (cs2-top (cGetTop cs2))

       ((if (or (< cs2-base cs1-base)
                (> cs2-top cs1-top)
                (> cs2-base cs2-top)))
        (!ms (list :at-location pc
                   :instruction 'cBuildCap
                   :ra cs1 :rb cs2
                   :reason 'capability-length-not-valid)
             y86-64))

       (cs1-perms (cGetPerms cs2))
       (cs2-perms (cGetPerms cs2))
       ;; raise an exception if cs2->perms grants more than cs1->perms does
       ((unless (equal (logand cs1-perms cs2-perms)
                       cs2-perms))
        (!ms (list :at-location pc
                   :instruction 'cBuildCap
                   :ra cs1 :rb cs2
                   :reason 'capability-user-def-violation)
             y86-64))
       
       ;; get all the fields of cs2
       (cs2-offset (cGetOffset cs2))

       ;; set all the fields of cs1
       (cs1 (cdr (cSetBounds cs1 cs2-base cs2-top (- cs2-top cs2-base)))) ;; set bounds
       (cs1 (cdr (cSetOffset cs1 cs2-offset)))       ;; set addr 
       (cs1 (cSetPerms cs1 cs2-perms))               ;; set perms 
       
      ;; seal as a sentry
       (new-cap (if (= (cGetOtype cs2) *sentry*)
                    (cSealCap cs1 (aot-to-mot *sentry*))
                    cs1))

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))
;; 2^XLEN−2 Sealedentry(“sentry”)capabilities;seeSection3.8


;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   cs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CCopyType cd cs1 cs2; 0xxx = 0x0d
(defund y86-cCopyType (y86-64)
  ;; if cs2->type is not reserved, set cs1->addr to cs2->otype, and  move cs1 to cd
  ;; otherwise set cd to null capability, then set cd->addr to cs2->otype
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cCopyType
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; cs2
       (rA (n04 (ash rA-rB -4)))         ;; cs1 
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cCopyType
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capabilities
       (cs1 (get-reg-capability rA y86-64))
       (cs2 (get-reg-capability rB y86-64))

       ;; Check if the cs1 capability has valid tag
       ((unless (cValidp cs1))
        (!ms (list :at-location pc
                   :instruction 'cCopyType
                   :ra cs1 :rb cs2
                   :reason 'capability-not-valid)
             y86-64))

       ;; Check if the capability is sealed
       ((if (cSealedp cs1))
        (!ms (list :at-location pc
                   :instruction 'cCopyType
                   :ra cs1 :rb cs2
                   :reason 'capability-sealed)
             y86-64))

       ;; if otype is reserved, generate null cap and
       ;; replace addr with cs2->otype
       ((mv non-reservedp cs1)
        (if (cReservedOtypep cs2)
            (mv nil (acap-to-mcap (null-mcap-to-acap)))
            (mv t cs1)))
       
       ;; raises exception if
       ;; 1) cs2->otype < cs1->base, OR
       ;; 2) cs2->otype >= cs1->top
       (cs1-base (cGetBase cs1))
       (cs1-top (cGetTop cs1))
       (ot (cGetOtype cs2))

       ((if (and non-reservedp
                 (or (< ot cs1-base)
                     (>= ot cs1-top))))
        (!ms (list :at-location pc
                   :instruction 'cCopyType
                   :ra cs1 :rb cs2
                   :reason 'capability-length-not-valid)
             y86-64))

       ;; change cs1->addr to cs2->otype
       (new-cap (change-acap cs1 :offset (- ot cs1-base)))

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))


;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   cs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CCSeal cd cs1 cs2; 0xxx = 0x0e
(defund y86-cCSeal (y86-64)
  ;; replace cd with cs1, and conditionally seal cs->otype with cs2->addr
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cCSeal
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; cs2
       (rA (n04 (ash rA-rB -4)))         ;; cs1 
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cCSeal
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capabilities
       (cs1 (get-reg-capability rA y86-64))
       (cs2 (get-reg-capability rB y86-64))

       ;; Check if the cs1 capability has valid tag
       ((unless (cValidp cs1))
        (!ms (list :at-location pc
                   :instruction 'cCSeal
                   :ra cs1 :rb cs2
                   :reason 'capability-not-valid)
             y86-64))

       ;; set cs to cs1 w/o changing otype if
       ;; 1) cs1 is sealed, OR
       ;; 1) cs2 invalid
       ;; 2) cs2->cursor < cs2->base
       ;; 3) cs2->cursor >= cs2->top
       ;; 4) cs2->addr = unsealed
       ;; Note: in our current model, 2+3 won't happen since cursor = addr
       (cursor (cGetAddress cs2))
       (cs1-unchangedp (or (cSealedp cs1)
                           (not (cValidp cs2))
                           (< cursor (cGetBase cs2))
                           (<= (cGetTop cs2) cursor)
                           (= cursor *unsealed*)))

       ;; Check if cs2 is sealed
       ((if (and (not cs1-unchangedp)
                 (cSealedp cs2)))
        (!ms (list :at-location pc
                   :instruction 'cCSeal
                   :ra cs1 :rb cs2
                   :reason 'capability-sealed)
             y86-64))
       
       ;; (seal-flag (not (or (cSealedp cs1)
       ;;                     (= (cGetAddress cs2) *unsealed*))))
       ((unless (or cs1-unchangedp
                    (cPermitSealp cs2)))
        (!ms (list :at-location pc
                   :instruction 'cCSeal
                   :ra cs1 :rb cs2
                   :reason 'capability-does-not-permit-seal)
             y86-64))

       ;; exception if cursor > max_otype (= *unsealed*)
       ((unless (or cs1-unchangedp
                    (<= cursor *max_otype*)))
        (!ms (list :at-location pc
                   :instruction 'cCSeal
                   :ra cs1 :rb cs2
                   :reason 'capability-length-not-valid)
             y86-64))

       ;; change cs1->otype to cs2->cursor
       (new-cap (if cs1-unchangedp
                    cs1
                    (cSealCap cs1 cursor)))

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   0x0   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; CSealEntry cd cs1 cs2; 0xxx = 0x0f
(defund y86-cSealEntry (y86-64)
  ;; replace cd with cs1, and seal as an sentry
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cSealEntry
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rA (n04 (ash rA-rB -4)))         ;; cs1 
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cSealEntry
                   :ra rA ;:rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capabilities
       (cs1 (get-reg-capability rA y86-64))

       ;; Check if the cs1 capability has valid tag
       ((unless (cValidp cs1))
        (!ms (list :at-location pc
                   :instruction 'cSealEntry
                   :ra cs1 ;:rb cs2
                   :reason 'capability-not-valid)
             y86-64))

       ((if (cSealedp cs1))
        (!ms (list :at-location pc
                   :instruction 'cSealEntry
                   :ra cs1 ;:rb rs2
                   :reason 'capability-sealed)
             y86-64))

       ((unless (cPermitExecute cs1))
        (!ms (list :at-location pc
                   :instruction 'cSealEntry
                   :ra cs1 ;:rb rs2
                   :reason 'permit-execution-violation)
             y86-64))

       ;; change cs1->otype to *sentry*
       (new-cap (cSealCap cs1 (aot-to-mot *sentry*)))

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap new-cap))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rC raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))
(in-theory (disable get-reg-capability))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                  ;;
;;                      Pointer-Arithmetic                          ;;
;;                                                                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; There is only one non-deprecated pointer-arithmetic instruction in the draft v9 specification

;; CHERI-RISC-V format for CMove
;; | 31 - 25  | 24 - 20 | 19 - 15 | 14 - 12 | 11 - 07 | 06 - 0 |
;; |----------|---------|---------|---------|---------|--------|
;; |   0x7f   |   0xa   |   cs1   |   0x0   |    cd   |  0x5b  |


;; CHERI-Y86-64 
;; |      Byte 0       | Byte 1  |      Byte 2       |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 |
;; |---------|---------|---------|-------- |---------|
;; |   0xc   |   0x2   |   0x00  |   cs1   |    cd   |
;;                                    rA        rB    
;; CMove cs1 cd; Move capability
(defund y86-capMove (y86-64)
  ;; Capability register cd is replaced with the contents of cs1
  ;; Note, this "should" be called cMove, but that's already "conditional move" in Y86
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 3-byte instruction?
       ((unless (< pc *2^64-4*))
        (!ms (list :at-location pc
                   :instruction 'cMove
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; cs1
       (rA (n04 (ash rA-rB -4)))         ;; cd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'cMove
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capabilities
       (cs1 (get-reg-capability rA y86-64))

       ;; Turn cs1 into a 128-bit nat
       (raw (acap-to-mcap cs1))
       ;; Store cs1 in cd
       (y86-64 (!rgfi rB raw y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 3) y86-64)))
    y86-64))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                  ;;
;;                      Pointer-Comparison                          ;;
;;                                                                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; There is only one non-deprecated pointer-arithmetic instruction in the draft v9 specification

;; CHERI-RISC-V format for CMove
;; | 31 - 25  | 24 - 20 | 19 - 15 | 14 - 12 | 11 - 07 | 06 - 0 |
;; |----------|---------|---------|---------|---------|--------|
;; |   0x7f   |   0xa   |   cs1   |   0x0   |    cd   |  0x5b  |


;; CHERI-Y86-64 
;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x3   |   0xxx  |   cs1   |   cs2   |   0x0   |   rd    |
;;                                    rA        rB                 rC
;; CTestSubset cs1 cs2 rd; Test if cs2 is a subset of cs1
(defund y86-cTestSubset (y86-64)
  ;; Register rd is set to 1 if tags of cs1 and cs2 are the same and the bounds
  ;; and permissions of cs2 are a subset of those of cs1
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 4-byte instruction?
       ((unless (< pc *2^64-5*))
        (!ms (list :at-location pc
                   :instruction 'cTestSubset
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 2) y86-64))
       (rB (n04 rA-rB))                  ;; cs1
       (rA (n04 (ash rA-rB -4)))         ;; cs2
       (rC (n04 (rm08 (+ pc 3) y86-64))) ;; rd

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15) (= rC 15)))
        (!ms (list :at-location pc
                   :instruction 'cTestSubset
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get capabilities
       (cs1 (get-reg-capability rA y86-64))
       (cs2 (get-reg-capability rB y86-64))

       ;; Flag for whether cs2 is a subset of cs1
       ;; Initially 1 unless at least one in four cases
       (flag 1)

       ;; Check if the capabilities have the same tags
       (flag (if (equal (cValidp cs1) (cValidp cs2)) flag 0))

       ;; Check if cs2.base > cs1.base
       (cs1_base (cGetBase cs1))
       (cs2_base (cGetBase cs2))
       (flag (if (> cs2_base cs1_base) flag 0))

       ;; Check if cs2.top < cs1.top
       (cs1_top (cGetTop cs1))
       (cs2_top (cGetTop cs2))
       (flag (if (< cs2_top cs1_top) flag 0))

       ;; check if cs2.perms & cs1.perms == cs2_perms
       (cs1_perms (cGetPerms cs1))
       (cs2_perms (cGetPerms cs2))
       (flag (if (equal (logand cs2_perms cs1_perms) cs2_perms) flag 0))

       ;; Store cs1 in cd
       (y86-64 (!rgfi rC flag y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 4) y86-64)))
    y86-64))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                  ;;
;;                           Control-Flow                           ;;
;;                                                                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO: Needs DDC and PCC special registers to implement



(defund y86-cap-step (y86-64 cap-list)
  (declare (xargs :guard t
                  :stobjs (y86-64)
                  :verify-guards nil))
  (b* ((pc (rip y86-64))
       (byte-at-pc (rm08 pc y86-64))
       ((unless (equal (logand byte-at-pc #xF0) #xC0)) (y86-step y86-64 cap-list))
       ((unless (< pc *2^64-3*))
        (!ms (list :at-location pc
                   :instruction 'capability-aware-instruction
                   :rip-too-large t)
             y86-64))
       (fn (rm08 (+ pc 1) y86-64)))
      (case byte-at-pc
        ;; Capability inspection instructions
        (#xC0
         (case fn
           (#x00 (y86-cPerm-to-rB y86-64))
           (#x01 (y86-cType-to-rB y86-64))
           (#x02 (y86-cBase-to-rB y86-64))
           (#x03 (y86-cLen-to-rB y86-64))
           (#x04 (y86-cTag-to-rB y86-64))
           ;; TODO: #x05
           (#x06 (y86-gcOff      y86-64))
           (#x07 (y86-gcFlags    y86-64))
           (#x08 (y86-gcHi       y86-64))
           (#x09 (y86-gcLim      y86-64))
           (t    (y86-illegal-opcode y86-64))))
        ;; Capability modification instructions
        (#xC1
         (case fn
           (#x00 (y86-cSeal           y86-64))
           (#x02 (y86-cAndPerm        y86-64))
           (#x04 (y86-cSetOffset      y86-64))
           (#x05 (y86-cSetAddr        y86-64))
           (#x06 (y86-cIncOffset      y86-64))
           (#x07 (y86-cIncOffsetImm   y86-64))
           (#x08 (y86-cSetBounds      y86-64))
           (#x09 (y86-cSetBoundsExact y86-64))
           (#x0a (y86-cSetBoundsImm   y86-64))
           (#x0b (y86-cClearTag       y86-64))
           (#x0c (y86-cBuildCap       y86-64))
           (#x0d (y86-cCopyType       y86-64))
           (#x0e (y86-cCSeal          y86-64))
           (#x0f (y86-cSealEntry      y86-64))
           (t    (y86-illegal-opcode y86-64))))
        (t (y86-illegal-opcode y86-64)))))



;;;;; TESTING ;;;;;;;;

(defun n24p-n08p-alistp (alst)
  ;; Recognizes an alist of form: ( (addr . value) ... ). 
  ;; Each addr must be an n24p and each value a n08p.
  (declare (xargs :guard t))
  (if (atom alst)
      t
    (if (atom (car alst))
        nil
      (let ((symbol (caar alst))
            (val    (cdar alst))
            (rest   (cdr  alst)))
        (and (n24p symbol)
             (n08p val)
             (n24p-n08p-alistp rest))))))

(defun n24p-listp (lst)
  (if (atom lst)
      t
    (and (n24p (car lst))
         (n24p-listp (cdr lst)))))

(defun n24p-n08p-alist-list-keys (alst)
  (if (atom alst)
      nil
    (cons (caar alst)
          (n24p-n08p-alist-list-keys (cdr alst)))))

(defthm n24p-n08p-alist-list-keys-n24p-listp
  (implies (n24p-n08p-alistp alst)
           (n24p-listp (n24p-n08p-alist-list-keys alst))))

(defun y86-mem-program-bytes-loadedp (program-bytes y86-64)
  ;; Given an alist of the form ( ... (addr . byte-value) ... ), this
  ;; predicate checks whether it's loaded into the y86-64 memory.
  (declare (xargs :stobjs (y86-64)
                  :guard (n24p-n08p-alistp program-bytes)))
  (if (atom program-bytes)
      t
    (let ((addr (caar program-bytes))
          (val  (cdar program-bytes))
          (rest (cdr  program-bytes)))
      (and (equal (rm08 addr y86-64) val)
           (y86-mem-program-bytes-loadedp rest y86-64)))))

(defconst *cap-code*
   '((20 .  0)
    (19 .  0)
    (18 .  0)
    (17 .  0)
    (16 .  0)
    (15 .  0)  
    (14 . 13)  
    (13 . 127) 
    (12 . 255) 
    (11 . 248)
    (10 . 11)
    ( 9 .  0)
    ( 8 . 12)  
    ( 7 .  0)
    ( 6 .  0)
    ( 5 .  0)
    ( 4 .  0)  
    ( 3 .  0)
    ( 2 .  0)
    ( 1 .  0)
    ( 0 . 31)
   ))

;; (acap 1 1 13 (1- (expt 2 *xlen*)) 19 12 32)
;; ((TAG . 1)
;;  (FLAGS . 1)
;;  (PERMS . 13)
;;  (OT . 18446744073709551615)
;;  (OFFSET . 19)
;;  (BASE . 12)
;;  (LEN . 32))

;; Testing temporary 128-capability
(in-theory (enable get-capability))
(defthmd get-capability-from-cap-code
 (implies (and (y86-64p y86-64)
               (y86-mem-program-bytes-loadedp *cap-code* y86-64))
          (b* ((cap (get-capability 0 y86-64))
               (addr (+ (acap->base cap)
                        (acap->offset cap)))
               (lb   (acap->base cap))
               (ub   (+ (acap->base cap)
                        (acap->len cap)))
               (ot   (acap->ot cap))
               (flag (acap->flags cap))
               (perm (acap->perms cap))
               (tag  (acap->tag cap)))
              (and (equal lb 12)
                   (equal ub 44)
                   (equal addr 31)
                   (equal ot (1- (expt 2 *xlen*)))
                   (equal flag 1)
                   (equal perm 13)
                   (equal tag 1)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                         ;;
;; TESTING Y86-CPERM-TO-RB ;;
;;                         ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;         Byte 0      |  Byte 1 |      Byte 2
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 |
;; |---------|---------|---------|-------- |---------|
;; |   0xc   |   0x0   |   0x00  |   cs1   |    rd   |
;;                                   0x1       0x0

(defconst *cap-code-1* 
  '(
    ;; get capability tag machine code
    (129 . 20) ;; 0x13 - (cax: 1 -> cbx: 4)
    (128 . 4)  ;; 0x03 - fn
    (127 . 192)
    ;; get capability len machine code
    (126 . 19) ;; 0x13 - (cax: 1 -> cbx: 3)
    (125 . 3)  ;; 0x03 - fn
    (124 . 192)
    ;; get capability base machine code
    (123 . 19) ;; 0x13 - (cax: 1 -> cbx: 3)
    (122 . 2)  ;; 0x02 - fn
    (121 . 192)
    ;; get capability otype machine code
    (120 . 18) ;; 0x12 - (cax: 1 -> cbx: 2)
    (119 . 1)  ;; 0x01 - fn
    (118 . 192)
    ;; get capability permissions machine code
    (117 . 16) ;; 0x10 - (cax: 1 -> cbx: 0)
    (116 . 0)
    (115 . 192)

    ;; Capability
    (20 .  0)
    (19 .  0)
    (18 .  0)
    (17 .  0)
    (16 .  0)
    (15 .  0)  ;; tag
    (14 . 13)  
    (13 . 127)  ;; perms
    (12 . 255)  ;; otype
    (11 . 248)
    (10 . 11)
    ( 9 .  0)
    ( 8 . 12)  ;; t
    ( 7 .  0)
    ( 6 .  0)
    ( 5 .  0)
    ( 4 .  0)  ;; b
    ( 3 .  0)
    ( 2 .  0)
    ( 1 .  0)
    ( 0 . 31)
   ))

;; (acap 1 1 13 (1- (expt 2 *xlen*)) 19 12 32)
;; ((TAG . 1)
;;  (FLAGS . 1)
;;  (PERMS . 13)
;;  (OT . 18446744073709551615)
;;  (OFFSET . 19)
;;  (BASE . 12)
;;  (LEN . 32))

(in-theory (disable get-reg-capability)) 
;; Testing reading cap from memory and asserting in register
(defthmd read-cap-perm-1
  (b* ((cap (get-reg-capability 1 y86-64))
       (perms (acap->perms cap)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cap-code-1* y86-64)
                    (equal (rgfi 1 y86-64) (rm128 0 y86-64)))
               (equal perms 13)))
  :hints (("Goal" :in-theory (enable get-reg-capability))))

;; Testing reading cap perm instruction
(defthmd read-cap-perm
  (b* ((cap (get-reg-capability 1 y86-64))
       (perms (n128 (acap->perms cap))))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cap-code-1* y86-64)
    		    (equal (rip y86-64) 115)
                    (equal (rgfi 1 y86-64) (rm128 0 y86-64)))
    	       (and (equal (y86-cap-step y86-64 cap-list)
    	    	           (!rip 118 (!rgfi 0 perms y86-64))))))
  :hints (("Goal" :in-theory (enable y86-cap-step
                                     y86-cperm-to-rB))))
;; Testing reading cap from memory and asserting in register
(defthmd read-cap-type-1
  (b* ((cap (get-reg-capability 1 y86-64))
       (ot (acap->ot cap)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cap-code-1* y86-64)
                    (equal (rgfi 1 y86-64) (rm128 0 y86-64)))
               (equal ot (1- (expt 2 *xlen*)))))
  :hints (("Goal" :in-theory (enable get-reg-capability))))

;; Testing reading cap type instruction
(defthmd read-cap-type
  (b* ((cap (get-reg-capability 1 y86-64))
       (ot (n128 (acap->ot cap))))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cap-code-1* y86-64)
    		    (equal (rip y86-64) 118)
                    (equal (rgfi 1 y86-64) (rm128 0 y86-64)))
    	       (and (equal (y86-cap-step y86-64 cap-list)
    	    	           (!rip 121 (!rgfi 2 ot y86-64))))))
  :hints (("Goal" :in-theory (enable y86-cap-step
                                     y86-ctype-to-rB))))

;; Testing reading cap from memory and asserting in register
(defthmd read-cap-base-1
  (b* ((cap (get-reg-capability 1 y86-64))
       (base (acap->base cap)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cap-code-1* y86-64)
                    (equal (rgfi 1 y86-64) (rm128 0 y86-64)))
               (equal base 12)))
  :hints (("Goal" :in-theory (enable get-reg-capability))))

;; Testing reading cap base instruction
(defthmd read-cap-base
  (b* ((cap (get-reg-capability 1 y86-64))
       (base (n128 (acap->base cap))))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cap-code-1* y86-64)
    		    (equal (rip y86-64) 121)
                    (equal (rgfi 1 y86-64) (rm128 0 y86-64)))
    	       (and (equal (y86-cap-step y86-64 cap-list)
    	    	           (!rip 124 (!rgfi 3 base y86-64))))))
  :hints (("Goal" :in-theory (enable y86-cap-step
                                     y86-cBase-to-rB))))

;; todo: test when length is negative, ms should be updated
(defthmd read-cap-len-1
  (b* ((cap (get-reg-capability 1 y86-64))
       (len (acap->len cap)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cap-code-1* y86-64)
                    (equal (rgfi 1 y86-64) (rm128 0 y86-64)))
               (equal len 32)))
  :hints (("Goal" :in-theory (enable get-reg-capability))))

;; Testing reading cap base instruction
(defthmd read-cap-len
  (b* ((cap (get-reg-capability 1 y86-64))
       (len (acap->len cap)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cap-code-1* y86-64)
    		    (equal (rip y86-64) 124)
                    (equal (rgfi 1 y86-64) (rm128 0 y86-64)))
    	       (and (equal (y86-cap-step y86-64 cap-list)
    	    	           (!rip 127 (!rgfi 3 len y86-64))))))
  :hints (("Goal" :in-theory (enable get-reg-capability
                                     y86-cap-step
                                     y86-cLen-to-rB))))

;; Testing reading cap from memory and asserting in register
(defthmd read-cap-tag-1
  (b* ((cap (get-reg-capability 1 y86-64))
       (tag (acap->tag cap)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cap-code-1* y86-64)
                    (equal (rgfi 1 y86-64) (rm128 0 y86-64)))
               (equal tag 1)))
  :hints (("Goal" :in-theory (enable get-reg-capability))))

;; Testing reading cap base instruction
(defthmd read-cap-tag
    (b* ((cap (get-reg-capability 1 y86-64))
       (tag (acap->tag cap)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cap-code-1* y86-64)
    		    (equal (rip y86-64) 127)
                    (equal (rgfi 1 y86-64) (rm128 0 y86-64)))
    	       (and (equal (y86-cap-step y86-64 cap-list)
    	    	           (!rip 130 (!rgfi 4 tag y86-64))))))
  :hints (("Goal" :in-theory (enable get-reg-capability
                                     y86-cap-step
                                     y86-cTag-to-rB))))



;;;;;;;;;;;;;;;;;;;;;;;
;;                   ;;
;; TESTING Y86-CSEAL ;;
;;                   ;;
;;;;;;;;;;;;;;;;;;;;;;;


;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   cs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC
;; reg0:
;; (acap 1 1 13 (1- (expt 2 *xlen*)) 19 12 32)

;; reg 1:
;; (acap 1 1 320 (1- (expt 2 *xlen*)) 512 (expt 2 60) 1024)
;; 1664131140994017187347530097048420864
;; ((TAG . 1)
;;  (FLAGS . 1)
;;  (PERMS . 320)
;;  (OT . 18446744073709551488)
;;  (OFFSET . 512)
;;  (BASE . 1152921504606846976)
;;  (LEN . 1024))


;; CSeal cd cs1 cs2; 0xxx = 0x00
(defconst *cSeal-code* 
  '(
    ;; get capability permissions machine code
    (118 .   2) ;; #x02, i.e. move sealed cap into reg 2
    (117 .   1) ;; #x01, i.e. get cap from reg 0, seal with address from reg 1
    (116 .   0)
    (115 . 193)

    ;; tag: 1 | flags: 1 | perms: 320 | ot: (1- (expt 2 *xlen*)) |
    ;; offset: 512 | base: 256 | len: 1024 
    ;; cap in reg 1

    (47 .   1)    
    (46 .  64)    
    (45 . 127)     
    (44 . 255) ;; otype   
    (43 . 249)
    (42 .  64)
    (41 .   1)
    (40 .   0)    
    (39 .   0)
    (38 .   0)
    (37 .   0)
    (36 .   0)    
    (35 .   0)
    (34 .   0)
    (33 .   3)
    (32 .   0)   

    ;; cap in reg 0:
    ;; tag: 1 | flags: 1 | perms: 13 | ot: (1- (expt 2 *xlen*)) |
    ;; offset: 19 | base: 12 | len: 32
    (15 .  0)  
    (14 . 13)  
    (13 . 127) 
    (12 . 255)
    (11 . 248)
    (10 . 11)
    ( 9 .  0)
    ( 8 . 12) 
    ( 7 .  0)
    ( 6 .  0)
    ( 5 .  0)
    ( 4 .  0) 
    ( 3 .  0)
    ( 2 .  0)
    ( 1 .  0)
    ( 0 . 31)
   ))


;; Testing cSeal
(defthmd test-cSealedp
  (b* ((cs1 (get-reg-capability 0 y86-64)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cSeal-code* y86-64)
    		    ;(equal (rip y86-64) 115)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64)))
    	       (not (cSealedp cs1))))
   :hints (("Goal" :in-theory (enable get-reg-capability cSealedp))))


;; Testing cSeal - ot <= max_otype
(defthmd seal-cap-test-1
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (cs2 (get-reg-capability 1 y86-64))
       (ot  (+ (acap->base cs2)
               (acap->offset cs2)))
       (cs1 (change-acap cs1 :ot ot))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *cSeal-code* y86-64)
    		    (equal (rip y86-64) 115)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 32 y86-64)))
    	       (and (equal (y86-cap-step y86-64 cap-list) 
    	    	           (!rip 119 (!rgfi 2 raw y86-64))))))
   :hints (("Goal" :in-theory (enable y86-cap-step y86-cSeal
                                      get-reg-capability
                                      cValidp
                                      cSealedp
                                      ))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        ;;
;; TESTING Y86-CSETOFFSET ;;
;;         Y86-CANDPERM   ;;
;;                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   rs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC

(defconst *mod-instr-code-1* 
  '(
    (134 .   2) ;; #x02, i.e. move perm-updated cap into reg 2
    (133 .   1) ;; #x01, i.e. get cap from reg 0, add len with value in reg 1
    (132 .   8) ;; cSetBounds
    (131 . 193)

    (130 .   2) ;; #x02, i.e. move perm-updated cap into reg 2
    (129 .   1) ;; #x01, i.e. get cap from reg 0, add addr with value in reg 1
    (128 .   6) ;; cIncOffset
    (127 . 193)

    (126 .   2) ;; #x02, i.e. move perm-updated cap into reg 2
    (125 .   1) ;; #x01, i.e. get cap from reg 0, add addr with value in reg 1
    (124 .   5) ;; cSetAddr
    (123 . 193)

    (122 .   2) ;; #x02, i.e. move perm-updated cap into reg 2
    (121 .   1) ;; #x01, i.e. get cap from reg 0, add offset with value in reg 1
    (120 .   4) ;; cSetOffset
    (119 . 193)
    ;; get capability permissions machine code
    (118 .   2) ;; #x02, i.e. move perm-updated cap into reg 2
    (117 .   1) ;; #x01, i.e. get cap from reg 0, logand perm with value in reg 1
    (116 .   2) ;; cAndPerm
    (115 . 193)

    ;; value in reg 3
    (63 .  101)
    (62 .  13)    
    (61 .   0)
    (60 .   0)
    (59 .   0)
    (58 .   0)    
    (57 .   0)
    (56 .  77)
    (55 .   0)    
    (54 .   0)    
    (53 .   0)     
    (52 .   0)   
    (51 .   0)
    (50 .   0)
    (49 .  12)
    (48 .   1)    

    ;; value in reg 1
    (31 .   0)    
    (30 .   0)    
    (29 .   0)     
    (28 .   0)   
    (27 .   0)
    (26 .   0)
    (25 .   0)
    (24 .   0)    
    (23 .   0)
    (22 .   0)
    (21 .   0)
    (20 .   0)    
    (19 .   0)
    (18 .   0)
    (17 .   3)
    (16 .   1)    

    ;; cap in reg 0:
    ;; tag: 1 | flags: 1 | perms: 13 | ot: (1- (expt 2 *xlen*)) |
    ;; offset: 1024 | base: 512 | len: 2048
    (15 .  0)  
    (14 . 13)  
    (13 . 127) 
    (12 . 255)
    (11 . 250)
    (10 . 128)
    ( 9 .  2)
    ( 8 .  0) 
    ( 7 .  0)
    ( 6 .  0)
    ( 5 .  0)
    ( 4 .  0) 
    ( 3 .  0)
    ( 2 .  0)
    ( 1 .  6)
    ( 0 .  0)
   ))

;; Testing cSetOffset
(defthmd test-cSealedp-sOffset-aPerm-code
  (b* ((cs1 (get-reg-capability 0 y86-64)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-1* y86-64)
    		    ;(equal (rip y86-64) 115)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64)))
    	       (not (cSealedp cs1))))
   :hints (("Goal" :in-theory (enable get-reg-capability cSealedp))))

;; Testing cAndPerm
(defthmd andPerm-cap-test-1
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (rs2 (rgfi 1 y86-64))
       (perms (acap->perms cs1))
       (new-perms (logand perms (n16 rs2)))
       (cs1 (change-acap cs1 :perms new-perms))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-1* y86-64)
    		    (equal (rip y86-64) 115)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 48 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list) 
    	    	      (!rip 119 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-candperm
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defthmd setOffset-cap-test
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (rs2 (rgfi 1 y86-64))
       (offset (acap->offset cs1))
       (new-offset (+ offset rs2))
       (cs1 (change-acap cs1 :offset new-offset))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-1* y86-64)
    		    (equal (rip y86-64) 119)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 16 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 123 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cSetOffset
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

;; invalid offset set return invalid cap
(defthmd setOffset-invalid-cap-test
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (cs1 (change-acap cs1 :tag 0))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-1* y86-64)
    		    (equal (rip y86-64) 119)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 48 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 123 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cSetOffset
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defthmd setAddr-cap-test
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (rs2 (rgfi 1 y86-64))
       (cs1 (change-acap cs1 :offset (- rs2 (acap->base cs1))))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-1* y86-64)
    		    (equal (rip y86-64) 123)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 16 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 127 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cSetAddr
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defthmd setAddr-invalid-cap-test
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (cs1 (change-acap cs1 :tag 0))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-1* y86-64)
    		    (equal (rip y86-64) 123)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 48 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 127 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cSetAddr
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defthmd incOffset-cap-test
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (rs2 (rgfi 1 y86-64))
       (offset (acap->offset cs1))
       (cs1 (change-acap cs1 :offset (+ offset rs2)))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-1* y86-64)
    		    (equal (rip y86-64) 127)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 16 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 131 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cIncOffset
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defthmd incOffset-invalid-cap-test
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (cs1 (change-acap cs1 :tag 0))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-1* y86-64)
    		    (equal (rip y86-64) 127)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 48 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 131 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cIncOffset
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defthmd setBounds-cap-test
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (rs2 (rgfi 1 y86-64))
       (addr (+ (acap->base cs1)
                (acap->offset cs1)))
       (cs1 (change-acap cs1 :base addr))
       (cs1 (change-acap cs1 :len rs2))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-1* y86-64)
    		    (equal (rip y86-64) 131)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 16 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 135 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cSetBounds
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defthmd setBounds-cap-bounded
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (rs2 (rgfi 1 y86-64))
       (addr (+ (acap->base cs1)
                (acap->offset cs1)))
       (cs1 (change-acap cs1 :base addr))
       (cs1 (change-acap cs1 :len rs2))
       (new-cap (get-reg-capability 2
                                    (y86-cap-step y86-64 cap-list))))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-1* y86-64)
    		    (equal (rip y86-64) 131)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 16 y86-64)))
    	       (and (<= (cGetBase cs1) (cGetBase new-cap))
                    (<= (cGetTop new-cap) (cGetTop cs1)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cSetBounds
                                      setBounds-cap-test
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))
(defthmd CSetBoundsCapRestriction-weak
    (b* (;; Set up instruction parameters
         (pc (rip y86-64))
         (y86-64-next (y86-cSetBounds y86-64))
         (rA-rB (rm08 (+ pc 2) y86-64))
         (rB (n04 rA-rB)) ;; rs2
         (rA (n04 (ash rA-rB -4))) ;; cs1
         (rC (n04 (rm08 (+ pc 3) y86-64))) ;; cd
         (rs2 (n128 (rgfi rB y86-64)))

         ;; Get capabilities & bounds
         (cs1 (get-reg-capability rA y86-64))
         (n-base (cGetAddress cs1))
         (new-cap (cdr (cSetBounds cs1 n-base (+ n-base rs2) (E rs2))))
         (raw (acap-to-mcap new-cap))
         (cs2 (rgfi rC y86-64-next)))
        (implies (and (y86-64p y86-64)
                      (not (ms y86-64))
                      (not (ms y86-64-next))
                      (nbp n-base *xlen*)
                      (nbp (e rs2) 6)
                      (<= (+ n-base rs2) (expt 2 *xlen*)))
                 (and (equal cs2 raw)
                      (cInCapBounds cs1 n-base rs2))))
  :hints (("goal" :in-theory (enable y86-cSetBounds))))

              

;; |      Byte 0       | Byte 1  |      Byte 2       |       Byte 3      |
;; | 00 - 03 | 04 - 07 | 08 - 15 | 16 - 19 | 20 - 24 | 25 - 28 | 29 - 31 |
;; |---------|---------|---------|-------- |---------|---------|-------- |
;; |   0xc   |   0x1   |   0xxx  |   cs1   |   rs2   |   0x0   |   cd    |
;;                                    rA        rB                 rC

(defconst *mod-instr-code-2* 
  '(
    ;; get capability permissions machine code
    (122 .   2)  ;; #x02, i.e. move perm-updated cap into reg 2
    (121 .   1)  ;; #x01, i.e. get cap from reg 0, logand perm with value in reg 1
    (120 .  13)  ;; cCopyType
    (119 . 193)
    
    (118 .   2)  ;; #x02, i.e. move perm-updated cap into reg 2
    (117 .   1)  ;; #x01, i.e. get cap from reg 0, logand perm with value in reg 1
    (116 .  12)  ;; cBuildCap
    (115 . 193)

    ;; value in reg 1'
    ;; (acap 1 1 35072 1000 824 612 1024)
    ;; 182106831564029563715553307299877488028
    ;; tag: 1 | flags: 1 | perms: 35072, 10001001 00000000 |
    ;; ot: 1000 |
    ;; offset: 824 | base: 612 | len: 1024 
    (47 . 137)    
    (46 .   0)    
    (45 .  96)     
    (44 .  31) ;; otype   
    (43 .  65)
    (42 . 153)
    (41 .   2)
    (40 . 100)    
    (39 .   0)
    (38 .   0)
    (37 .   0)
    (36 .   0)    
    (35 .   0)
    (34 .   0)
    (33 .   5)
    (32 . 156)   

    ;; cap in reg 1:
    ;; (acap 1 1 35072 (- (expt 2 *xlen*) 2) 824 612 1024)
    ;; 182106831566505443794124067849675736476
    ;; tag: 1 | flags: 1 | perms: 35072, 10001001 00000000 |
    ;; ot: (- (expt 2 *xlen*) 2) |
    ;; offset: 824 | base: 612 | len: 1024 
    (31 . 137)    
    (30 .   0)    
    (29 . 127)    
    (28 . 255) ;; otype   
    (27 . 241)
    (26 . 153)
    (25 .   2)
    (24 . 100)    
    (23 .   0)
    (22 .   0)
    (21 .   0)
    (20 .   0)    
    (19 .   0)
    (18 .   0)
    (17 .   5)
    (16 . 156)    
    
    ;; cap in reg 0:
    ;; (acap 1 1 36608 (1- (expt 2 *xlen*)) 1024 512 2048)
    ;; 190082199543970079129774242729558017536
    ;; tag: 1 | flags: 1 | perms: 36608, 10001111 00000000 |
    ;; ot: (1- (expt 2 *xlen*)) |
    ;; offset: 1024 | base: 512 | len: 2048
    ;; cap in reg 0
    (15 . 143)   ;; tag
    (14 .   0)   ;;       10010111
    (13 . 127)   ;; perms 00001101
    (12 . 255)   ;; otype
    (11 . 250)
    (10 . 128)
    ( 9 .   2)
    ( 8 .   0)   ;; top
    ( 7 .   0)
    ( 6 .   0)
    ( 5 .   0)
    ( 4 .   0)    ;; base
    ( 3 .   0)
    ( 2 .   0)
    ( 1 .   6)
    ( 0 .   0)    ;; addr
   ))

;; test for cBuildCap
(defthmd buildCap-sentry-cap-test
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (cs2 (get-reg-capability 1 y86-64))
       (base (acap->base cs2))
       (len (acap->len cs2))
       (offset (acap->offset cs2))
       (perms (acap->perms cs2))
       (ot (acap->ot cs2))

       (cs1 (change-acap cs1 :base base))
       (cs1 (change-acap cs1 :len len))
       (cs1 (change-acap cs1 :offset offset))
       (cs1 (change-acap cs1 :perms perms))
       (cs1 (change-acap cs1 :ot ot))

       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-2* y86-64)
    		    (equal (rip y86-64) 115)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 16 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 119 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cBuildCap
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defthmd buildCap-cap-test
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (cs2 (get-reg-capability 1 y86-64))
       (base (acap->base cs2))
       (len (acap->len cs2))
       (offset (acap->offset cs2))
       (perms (acap->perms cs2))

       (cs1 (change-acap cs1 :base base))
       (cs1 (change-acap cs1 :len len))
       (cs1 (change-acap cs1 :offset offset))
       (cs1 (change-acap cs1 :perms perms))

       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-2* y86-64)
    		    (equal (rip y86-64) 115)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 32 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 119 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cBuildCap
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defthmd copyType-cap-test
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (cs2 (get-reg-capability 1 y86-64))
       (ot (acap->ot cs2))
       (base (acap->base cs1))
       (cs1 (change-acap cs1 :offset (- ot base)))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-2* y86-64)
    		    (equal (rip y86-64) 119)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 32 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 123 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cCopyType
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defconst *mod-instr-code-3* 
  '(
    (122 .   2)  ;; #x02, i.e. move perm-updated cap into reg 2
    (121 .   0)  ;; #x00, i.e. get cap from reg 0
    (120 .  15)  ;; cSealEntry
    (119 . 193)
    
    (118 .   2)  ;; #x02, i.e. move perm-updated cap into reg 2
    (117 .   1)  ;; #x01, i.e. get cap from reg 0, logand perm with value in reg 1
    (116 .  14)  ;; cCSealCap
    (115 . 193)

    ;; value in reg 1:
    ;; (acap 1 1 448 128 1024 1000 2222)
    ;; 2328096421832011589443369175567304680
    ;; tag: 1 | flags: 1 | perms: 448, 00000001 11000000 |
    ;; ot: 128 |
    ;; offset: 1024 | base: 1000 | len: 2222
    (79 .   1)    
    (78 . 192)    
    (77 .  96)     
    (76 .   4) ;; otype   
    (75 .   3)
    (64 .  37)
    (73 . 131)
    (72 . 232)    
    (71 .   0)
    (70 .   0)
    (69 .   0)
    (68 .   0)    
    (67 .   0)
    (66 .   0)
    (65 .   7)
    (64 . 232)

    ;; cap in reg 0'
    ;; (acap 1 1 13 (1- (expt 2 64)) 1024 128 2048)
    ;; 70096005771998101468000856508990592
    ;; tag: 1 | flags: 1 | perms: 13, 00000000 00001101 |
    ;; ot: (1- (expt 2 64)) |
    ;; offset: 1024 | base: 128 | len: 2048
    (63 .   0)    
    (62 .  13)    
    (61 . 127)    
    (60 . 255)    
    (59 . 250)
    (58 .  32)
    (57 .   0)
    (56 . 128)    
    (55 .   0)
    (54 .   0)
    (53 .   0)
    (52 .   0)    
    (51 .   0)
    (50 .   0)
    (49 .   4)
    (48 . 128)     

    ;; value in reg 1'
    ;; (acap 1 1 0 128 (1- (expt 2 64)) 0 (expt 2 64))
    ;; 1947429474354137666571897508200447
    ;; tag: 1 | flags: 1 | perms: 0 |
    ;; ot: 128 |
    ;; offset: (- (expt 2 64) 1) | base: 0 | len: (expt 2 64)
    (47 .   0)    
    (46 .   0)    
    (45 .  96)     
    (44 .   4) ;; otype   
    (43 .   4)
    (42 .   1)
    (41 . 128)
    (40 .   4)    
    (39 . 255)
    (38 . 255)
    (37 . 255)
    (36 . 255)    
    (35 . 255)
    (34 . 255)
    (33 . 255)
    (32 . 255)    

    ;; value in reg 1:
    ;; (acap 1 1 0 128 1024 1000 2222)
    ;; 1947429208408811861706820076701672
    ;; tag: 1 | flags: 1 | perms: 0 |
    ;; ot: 128 |
    ;; offset: 1024 | base: 1000 | len: 2222
    (31 .   0)    
    (30 .   0)    
    (29 .  96)     
    (28 .   4) ;; otype   
    (27 .   3)
    (26 .  37)
    (25 . 131)
    (24 . 231)    
    (23 .   0)
    (22 .   0)
    (21 .   0)
    (20 .   0)    
    (19 .   0)
    (18 .   0)
    (17 .   7)
    (16 . 232)    

    ;; cap in reg 0:
    ;; (acap 1 1 13 128 1024 128 2048)
    ;; 69447288053211383642545468441887872
    ;; tag: 1 | flags: 1 | perms: 13, 00000000 00001101 |
    ;; ot: 128 |
    ;; offset: 1024 | base: 128 | len: 2048
    (15 .   0)    ;; tag
    (14 .  13)    ;;       10010111
    (13 .  96)    ;; perms 00001100
    (12 .   4)    ;; otype - sealed
    (11 .   2)
    (10 .  32)
    ( 9 .   0)
    ( 8 . 128)    ;; top
    ( 7 .   0)
    ( 6 .   0)
    ( 5 .   0)
    ( 4 .   0)    ;; base
    ( 3 .   0)
    ( 2 .   0)
    ( 1 .   4)
    ( 0 . 128)    ;; addr
   ))

;; test cases for ccseal:
;; 1) cs1 sealed, cs2 address != *unsealed*     -> cs1
;; 2) cs1 not sealed, cs2 address = *unsealed*  -> cs1
;; 3) cs1 not sealed, cs2 address != *unsealed* -> cs1->otype <- cs2->addr(< max_otype)
(defthmd cCSeal-cap-test-1
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-3* y86-64)
    		    (equal (rip y86-64) 115)
                    (equal (rgfi 0 y86-64) (rm128  0 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 16 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 119 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cCSeal
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defthmd cCSeal-cap-test-2
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-3* y86-64)
    		    (equal (rip y86-64) 115)
                    (equal (rgfi 0 y86-64) (rm128 48 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 32 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 119 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cCSeal
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))

(defthmd cCSeal-cap-test-3
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (cs2 (get-reg-capability 1 y86-64))
       (addr (+ (acap->base cs2)
                (acap->offset cs2)))
       (cs1 (change-acap cs1 :ot addr))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-3* y86-64)
    		    (equal (rip y86-64) 115)
                    (equal (rgfi 0 y86-64) (rm128 48 y86-64))
                    (equal (rgfi 1 y86-64) (rm128 64 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 119 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cCSeal
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))
 
(defthmd cSealEntry-cap-test
  (b* ((cs1 (get-reg-capability 0 y86-64))
       (cs1 (change-acap cs1 :ot (aot-to-mot *sentry*)))
       (raw (acap-to-mcap cs1)))
      (implies (and (y86-64p y86-64)
    		    (y86-mem-program-bytes-loadedp *mod-instr-code-3* y86-64)
    		    (equal (rip y86-64) 119)
                    (equal (rgfi 0 y86-64) (rm128 48 y86-64)))
    	       (equal (y86-cap-step y86-64 cap-list)
                      (!rip 123 (!rgfi 2 raw y86-64)))))
   :hints (("goal" :in-theory (enable y86-cap-step y86-cSealEntry
                                      get-reg-capability
                                      cValidp
                                      cSealedp))))
