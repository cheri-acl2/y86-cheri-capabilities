; helpers.lisp

;; These are some macros I like to use in proofs.  Most are pc macros.

(in-package "ACL2")

(defun apt () (accumulated-persistence t))
(defun apn () (accumulated-persistence nil))
(defun sap () (show-accumulated-persistence))

(define-pc-macro ls ()
  (mv nil 's-prop state))

(define-pc-macro sp ()
  (mv nil 's-prop state))

(define-pc-macro sr1 ()
  (mv nil '(show-rewrites 1) state))

(define-pc-macro sr2 ()
  (mv nil '(show-rewrites 2) state))

(define-pc-macro sr3 ()
  (mv nil '(show-rewrites 3) state))

(define-pc-macro sr4 ()
  (mv nil '(show-rewrites 4) state))

(define-pc-macro sr5 ()
  (mv nil '(show-rewrites 5) state))

(define-pc-macro sr6 ()
  (mv nil '(show-rewrites 6) state))

(define-pc-macro sr7 ()
  (mv nil '(show-rewrites 7) state))

(define-pc-macro sr8 ()
  (mv nil '(show-rewrites 8) state))

(define-pc-macro promtoe ()
  (mv nil 'promote state))

(define-pc-macro push ()
  (mv nil '(= & t t) state))

(define-pc-macro 1p ()
  (mv nil '(do-all 1 p) state))

;;  end macros section
