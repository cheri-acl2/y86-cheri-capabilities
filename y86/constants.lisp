
; constants.lisp                             

; y86 specific constants.  Where possible, these are meant to exactly
; follow the numbers used by X86 binary representations.

(in-package "ACL2")

(defconst *b-size*   8)  ; Byte
(defconst *w-size*  16)  ; Word
(defconst *d-size*  32)  ; Double
(defconst *q-size*  64)  ; Quad

(defconst *a-size*  64)  ; Physical address size
(defconst *m-size*  64)  ; Machine size

;  Memory size

(defconst *mem-size-in-bytes*  (expt 2 64))
(defconst *mem-size-in-words*  (floor *mem-size-in-bytes* 2))
(defconst *mem-size-in-dwords* (floor *mem-size-in-bytes* 4))
(defconst *mem-size-in-qwords* (floor *mem-size-in-bytes* 8))

; Some "useful" constants.  We define these because the ACL2
; definition mechanism does not evaluate and "fold" constants.


(defconst *2^0*       (expt 2  0))
(defconst *2^1*       (expt 2  1))
(defconst *2^2*       (expt 2  2))
(defconst *2^3*       (expt 2  3))
(defconst *2^4*       (expt 2  4))
(defconst *2^5*       (expt 2  5))
(defconst *2^6*       (expt 2  6))
(defconst *2^7*       (expt 2  7))
(defconst *2^8*       (expt 2  8))
(defconst *2^16*      (expt 2 16))
(defconst *2^24*      (expt 2 24))
(defconst *2^30*      (expt 2 30))
(defconst *2^32*      (expt 2 32))
(defconst *2^64*      (expt 2 64))

(defconst *2^16-1*    (- *2^16* 1))

(defconst *2^24-1*    (- *2^24*  1))
(defconst *2^24-2*    (- *2^24*  2))
(defconst *2^24-3*    (- *2^24*  3))
(defconst *2^24-4*    (- *2^24*  4))
(defconst *2^24-5*    (- *2^24*  5))
(defconst *2^24-6*    (- *2^24*  6))
(defconst *2^24-7*    (- *2^24*  7))
(defconst *2^24-8*    (- *2^24*  8))
(defconst *2^24-9*    (- *2^24*  9))
(defconst *2^24-10*   (- *2^24* 10))
(defconst *2^24-11*   (- *2^24* 11))
(defconst *2^24-12*   (- *2^24* 12))
(defconst *2^24-13*   (- *2^24* 13))
(defconst *2^24-14*   (- *2^24* 14))
(defconst *2^24-15*   (- *2^24* 15))
(defconst *2^24-16*   (- *2^24* 16))

(defconst *2^32-1*    (- *2^32*  1))
(defconst *2^32-2*    (- *2^32*  2))
(defconst *2^32-3*    (- *2^32*  3))
(defconst *2^32-4*    (- *2^32*  4))
(defconst *2^32-5*    (- *2^32*  5))
(defconst *2^32-6*    (- *2^32*  6))
(defconst *2^32-7*    (- *2^32*  7))
(defconst *2^32-8*    (- *2^32*  8))
(defconst *2^32-9*    (- *2^32*  9))
(defconst *2^32-10*   (- *2^32* 10))
(defconst *2^32-11*   (- *2^32* 11))
(defconst *2^32-12*   (- *2^32* 12))
(defconst *2^32-13*   (- *2^32* 13))
(defconst *2^32-14*   (- *2^32* 14))
(defconst *2^32-15*   (- *2^32* 15))
(defconst *2^32-16*   (- *2^32* 16))

(defconst *2^64-1*    (- *2^64*  1))
(defconst *2^64-2*    (- *2^64*  2))
(defconst *2^64-3*    (- *2^64*  3))
(defconst *2^64-4*    (- *2^64*  4))
(defconst *2^64-5*    (- *2^64*  5))
(defconst *2^64-6*    (- *2^64*  6))
(defconst *2^64-7*    (- *2^64*  7))
(defconst *2^64-8*    (- *2^64*  8))
(defconst *2^64-9*    (- *2^64*  9))
(defconst *2^64-10*   (- *2^64* 10))
(defconst *2^64-11*   (- *2^64* 11))
(defconst *2^64-12*   (- *2^64* 12))
(defconst *2^64-13*   (- *2^64* 13))
(defconst *2^64-14*   (- *2^64* 14))
(defconst *2^64-15*   (- *2^64* 15))
(defconst *2^64-16*   (- *2^64* 16))

; y86-specific information.

; These numberings are the same as used by the X86 processor family.

(defconst *y86-64-reg-numbers*
  '((:rax .  0)  (%eax . 0)  (r00 .  0)
    (:rcx .  1)  (%ecx . 1)  (r01 .  1)
    (:rdx .  2)  (%edx . 2)  (r02 .  2)
    (:rbx .  3)  (%ebx . 3)  (r03 .  3)
    (:rsp .  4)  (%esp . 4)  (r04 .  4)
    (:rbp .  5)  (%ebp . 5)  (r05 .  5)
    (:rsi .  6)  (%esi . 6)  (r06 .  6)
    (:rdi .  7)  (%edi . 7)  (r07 .  7)
    (:r08 .  8)              (r08 .  8)
    (:r09 .  9)              (r09 .  9)
    (:r10 . 10)              (r10 . 10)
    (:r11 . 11)              (r11 . 11)
    (:r12 . 12)              (r12 . 12)
    (:r13 . 13)              (r13 . 13)
    (:r14 . 14)              (r14 . 14)
;   (:r15 . 15)              (r15 . 15)   ; But, the y86-64 doesn't use R15
    ))

(defun reg-to-num (reg)
  (declare (xargs :guard (symbolp reg)))
  (cdr (assoc reg *y86-64-reg-numbers*)))

;; added these so the correspondence would be 
;; clearer in theorems.

(defconst *rax* 0)
(defconst *rcx* 1)
(defconst *rdx* 2)
(defconst *rbx* 3)
(defconst *rsp* 4)
(defconst *rbp* 5)
(defconst *rsi* 6)
(defconst *rdi* 7)
(defconst *r08* 8)
(defconst *r09* 9)
(defconst *r10* 10)
(defconst *r11* 11)
(defconst *r12* 12)
(defconst *r13* 13)
(defconst *r14* 14)

(defun reg-nump (n)
  ;; n is a legal register number
  (and (natp n)
       (<= n 14)))


;; >> This should actually be 15, but changing it causes
;;    problems later, since many lemmas about regi refer
;;    to n04p.  Warren seems to have prevented using R15 
;;    by making restrictions in the asm definitions. 
(defconst *y86-64-reg-names-len* 16)
(defconst *y86-64-max-mem-address* *2^24*)


