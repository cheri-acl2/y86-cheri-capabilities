;  y86-64-mem-init.lisp                      

; (ld "y86-64-mem-init.lisp" :ld-pre-eval-print t)

(in-package "ACL2")

(include-book "y86-64")
(local (include-book "centaur/gl/gl" :dir :system))

; Functions to gather items from the registers and memory.
; Functions to initialize the memory.

(defund rmbytes (n addr y86-64)
  ;; Collect n consecutive bytes in y86 memory 
  ;; beginning at address addr.
  (declare (xargs :guard (and (natp n)
                              (n64p addr))
                  :stobjs (y86-64)))
  (if (mbe :logic (zp n) :exec (= n 0))
      nil
    (cons (list addr (rm08 addr y86-64))
          (rmbytes (1- n) (n64+ addr 1) y86-64))))

(defund y86-clear-mem (y86-64 addr)
  ;; Clear memory from addr to address zero
  ;; Note, addr limited to less than 2^60 for performance
  (declare (xargs :guard (n60p addr)
                  :stobjs (y86-64)))
  (let ((addr (the (unsigned-byte 60) addr)))
    (if (mbe :logic (zp addr) :exec (= addr 0))
        ;; Clear address 0 (zero)
        (wm08 0 0 y86-64)
      ;; Clear address ADDR
      (let ((y86-64 (wm08 addr 0 y86-64)))
        (y86-clear-mem y86-64 (1- addr))))))

(defun y86-regp (updates)
  ;; Recognizer for a list of register updates of form:
  ;; ( (reg-name . n64-value) ... )
  (declare (xargs :guard t))
  (if (atom updates)
      t
    (b* ((update (car updates))
         (rest   (cdr updates)))
        (and (consp update)
             (b* ((field (car update))
                  (value (cdr update)))
                 (and (keywordp field)
                      (assoc field *y86-64-reg-numbers*)
                      (n64p value)
                      (y86-regp rest)))))))

(defund y86-reg-updates (y86-64 updates)
  ;; Perform the list of register updates to the y86 state.
  (declare (xargs :guard (y86-regp updates)
                  :stobjs (y86-64)))
  (if (atom updates)
      y86-64
    (b* ((update (car updates))
         (rest   (cdr updates))
         (field  (car update))
         (value  (cdr update))
         (y86-64 (!rgfi (reg-to-num field) value y86-64)))
        (y86-reg-updates y86-64 rest))))

(defun y86-memp (updates)
  ;; Recognizer for a list of memory updates of the form:
  ;; ( (address . byte-value) ... )
  (declare (xargs :guard t))
  (if (atom updates)
      t
    (b* ((update (car updates))
         (rest   (cdr updates)))
        (and (consp update)
             (b* ((addr  (car update))
                  (value (cdr update)))
                 (and (n64p addr)
                      (n08p value)
                      (y86-memp rest)))))))

(defund y86-mem-updates (y86-64 updates)
  ;; Perform the list of memory updates to the y86 state.
  (declare (xargs :guard (y86-memp updates)
                  :stobjs (y86-64)))
  (if (atom updates)
      y86-64
    (b* ((update (car updates))
         (rest   (cdr updates))
         (addr   (car update))
         (value  (cdr update))
         (y86-64 (wm08 addr value y86-64)))
        (y86-mem-updates y86-64 rest))))

(defund m64-get-regs-and-flags (y86-64)
  ;; This seems to be for debugging, to show
  ;; the current values of registers and flags.
  ;; I can't find that it's ever used.
  ;; >> Why is it arranged in these sublists?
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (list
   (list :rip (rip y86-64))
   (list :rax (rgfi (reg-to-num :rax) y86-64)
         :rbx (rgfi (reg-to-num :rbx) y86-64)
         :rcx (rgfi (reg-to-num :rcx) y86-64)
         :rdx (rgfi (reg-to-num :rdx) y86-64))
   (list :rdi (rgfi (reg-to-num :rdi) y86-64)
         :rsi (rgfi (reg-to-num :rsi) y86-64)
         :rbp (rgfi (reg-to-num :rbp) y86-64)
         :rsp (rgfi (reg-to-num :rsp) y86-64))
   (list :r08 (rgfi (reg-to-num :r08) y86-64)
         :r09 (rgfi (reg-to-num :r09) y86-64)
         :r10 (rgfi (reg-to-num :r10) y86-64)
         :r11 (rgfi (reg-to-num :r11) y86-64))
   (list :r12 (rgfi (reg-to-num :r12) y86-64)
         :r13 (rgfi (reg-to-num :r13) y86-64)
         :r14 (rgfi (reg-to-num :r14) y86-64)
         ;; R15 doesn't seem to be included in the y86 architecture
         ;; :r15 (rgfi (reg-to-num :r15) y86-64)
         )
   (list :f-zf (zf y86-64)
         :f-sf (sf y86-64)
         :f-of (of y86-64))
   (list :mr-status (ms y86-64))))


(defund m64-get-mem-bytes (addr number y86-64)
  ;; Create a list of tuples (:address addr :byte-value val),
  ;; for number successive bytes of memory beginning at 
  ;; address addr. 
  (declare (xargs :guard (and (n64p addr)
                              (n64p number))
                  :stobjs (y86-64)))
  (if (zp number)
      nil
    (let ((value (rm08 addr y86-64)))
      (cons (list :address addr
                  :byte-value value)
            (m64-get-mem-bytes (n64+ addr 1) (1- number) y86-64)))))


(defund m64-get-mem-qwords (addr number y86-64)
  ;; Create a list of tuples (:address addr :qword-value val),
  ;; for number successive quadwords of memory beginning at 
  ;; address addr. 
  (declare (xargs :guard (and (n64p addr)
                              (n64p number))
                  :stobjs (y86-64)))
  (if (zp number)
      nil
    (let ((value (rm64 addr y86-64)))
      (cons (list :address addr
                  :qword-value value)
            (m64-get-mem-qwords (n64+ addr 8) (1- number) y86-64)))))

(defund y86-clear-regs (y86-64)
  ;; Clear all registers, including RIP and condition codes.
  (declare (xargs :stobjs (y86-64)))
  (b* ((y86-64 (!rgfi (reg-to-num :rax) 0 y86-64))
       (y86-64 (!rgfi (reg-to-num :rbx) 0 y86-64))
       (y86-64 (!rgfi (reg-to-num :rcx) 0 y86-64))
       (y86-64 (!rgfi (reg-to-num :rdx) 0 y86-64))

       (y86-64 (!rgfi (reg-to-num :rdi) 0 y86-64))
       (y86-64 (!rgfi (reg-to-num :rsi) 0 y86-64))
       (y86-64 (!rgfi (reg-to-num :rbp) 0 y86-64))
       (y86-64 (!rgfi (reg-to-num :rsp) 0 y86-64))

       (y86-64 (!rgfi (reg-to-num :r08) 0 y86-64))
       (y86-64 (!rgfi (reg-to-num :r09) 0 y86-64))
       (y86-64 (!rgfi (reg-to-num :r10) 0 y86-64))
       (y86-64 (!rgfi (reg-to-num :r11) 0 y86-64))

       (y86-64 (!rgfi (reg-to-num :r12) 0 y86-64))
       (y86-64 (!rgfi (reg-to-num :r13) 0 y86-64))
       (y86-64 (!rgfi (reg-to-num :r14) 0 y86-64))

       (y86-64 (!rip 0 y86-64))
       (y86-64 (y86-ALU-results-store-flgs 0 0 0 y86-64)))
      y86-64))

;; >> Note that this calls y86-clear-regs, so already 
;;    clears RIP and the flags.  

(defund y86-clear-all-state (y86-64)
  (declare (xargs :stobjs (y86-64)))
  (b* (;; Clear RIP (redundant)
       (y86-64 (!rip 0 y86-64))
       ;; Clear flags (redundant)
       (y86-64 (!zf 0 y86-64))
       (y86-64 (!sf 0 y86-64))
       (y86-64 (!of 0 y86-64))
       ;; Clear registers
       ;; >> This already clears RIP and flags:
       (y86-64 (y86-clear-regs y86-64))
       ;; Clear memory
       (y86-64 (y86-clear-mem y86-64 (expt 2 24)))
       ;; Y86-64 model
       (y86-64 (!ms nil y86-64)))
    y86-64))

(defund init-y86-state (mr-status pc regs flags mem y86-64)
  ;; Create an initial Y86 state from specified mr-status,
  ;; reg updates, flag updates, mem updates.
  (declare (xargs :guard (and (n64p pc)
                              (y86-regp regs)
                              (y86-memp mem)
                              (alistp flags))
                  :stobjs (y86-64)))
  (b* (;; Initialize the y86 for execution
       ;; First, clear the flags, registers, memory, and model state
;       (y86-64 (y86-clear-all-state y86-64))
       ;; Set the state...
       (y86-64 (!rip pc y86-64))
       ;; - caused the binding to be run in an implicit
       ;; progn
       (- (cw "Length mem ~p0~%" (len mem)))
       (- (cw "mem: ~p0~%" mem))
       (y86-64 (y86-mem-updates y86-64 mem))
       (y86-64 (y86-reg-updates y86-64 regs))
       ;; Clear the flags
       (zf (n01 (nfix (cdr (assoc :zf flags)))))
       (sf (n01 (nfix (cdr (assoc :sf flags)))))
       (of (n01 (nfix (cdr (assoc :of flags)))))
       (y86-64 (y86-ALU-results-store-flgs zf sf of y86-64))
       ;; Set model state to normal
       (y86-64 (!ms mr-status y86-64)))
    y86-64))
