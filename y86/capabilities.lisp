
;; capabilities.lisp
;;

;; This file contains events related to capabilities. Originally, it only
;; contained list-based capabilities but now we are transitioning to the
;; capabilities described in the CHERI ISA v8 specification. The CHERI ISA
;; describes roughly two kinds of capabilities, architectual and in-memory (or
;; memory-resident as we will sometimes say). Memory-resident
;; capabilities are stored in CHERI concentrate compression format at, in our
;; Y86-64 case, 64-bit addresses and have 128-bit sizes.  Architectual
;; capabilities contain software-accessible architectural fields which may differ
;; in content and size from memory-resident capabilities, though these fields
;; should be recoverable from their memory-resident analogs.

(in-package "ACL2")

(include-book "misc-events")
(include-book "operations")
(include-book "constants")


(include-book "operations-nbp")
(include-book "y86-64-state")
(include-book "std/util/define" :DIR :SYSTEM)
(include-book "std/util/bstar" :dir :system)
(include-book "std/util/defaggregate" :dir :system)
(include-book "centaur/gl/gl" :dir :system)

;; Capability constants
(defconst *xlen* 64)   ;; Architectural address size in bits
(defconst *clen* 128)  ;; Architectural capability size in bits
(defconst *mw*   14)   ;; Mantissa width
(defconst *tw*   12)   ;; width for encoded top
(defconst *eh*   3)    ;; half width used for storing E
(defconst *otw*    64) ;; otype width in acap
(defconst *motw*   18) ;; otype length in mcap
(defconst *pw*     16) ;; perms width in bits
(defconst *flglen*  1) ;; flags width in bits
(defconst *cmplen* 27) ;; compression length

(defconst *max_otype* (- (expt 2 *motw*) (+ 1 16))) ;; max otype = MAX(otype_width) - reserved otypes


;; MEMORY-RESIDENT CAPABILITIES
;; This section models memory-resident (or in-memory as referred to in the
;; CHERI documentation) capabilities according to the CHERI Concentrate 
;; Compression format found in CHERI ISA v8

;; Representation of CHERI Concentrate Compression capabilities:
;;
;;                         |-------bounds---------|
;;     p            otype   I   T    TE   B     BE
;; |--------|   |----------|-|-----|---|------|---|
;;     16     3      18     1   9    3    11    3
;; 63                                             0

;; Bounds encoding:

;;  MW - mantissa width, for 128-bit capabilities MW == 14

;;   B - MW-bit value that forms "base" of bounds, slightly compressed, in one
;;       of two formats depending on the I (I_E in documentation) bit

;;   T - MW-bit value that forms "base" of bounds, slightly compressed, in one
;;       of two formats depending on the I (I_E in documentation) bit, top two
;;       bits not explicit in memory

;;   I - internal exponent bit that selects between two formats, if set then
;;       exponent is stored instead of lower 3 bits of B & T (reduces precision 
;;       by 3 bits), otherwise exponent is zero and full width of B & T used

;;   E - 6-bit exponent, determines position at which B & T are inserted in 
;; -------------------------------------------------------
;; helper functions
;; -------------------------------------------------------
;; read last <len> bits after shifting shift bits to the right  
(defmacro nb-ash (shift len seq)
  `(let* ((ash-seq (ash ,seq (- ,shift)))
          (len-bit (1- (expt 2 ,len))))
     (logand ash-seq len-bit)))

;; recognizer for compression
(defun compressionp (c)
  (declare (xargs :guard t))
  (and (integerp c)
       (<= 0 c)
       (b* ((exp (expt 2 (1+ (+ *mw* *tw*))))
            (TE (nb-ash *mw* *eh* c))
            (BE (nb-ash 0 *eh* c))
            (IE (nb-ash (+ *tw* *mw*) 1 c))
            (E (logior (ash TE *eh*) BE)))
           (and (< c exp)
                (natp E)
                ;; if IE = 1, {TE, BE <= 52}
                (or (= IE 0) (<= E 52))))))

;; if n is an at most b bits nat
(defmacro nbp (n b)
  `(and (integerp ,n)
        (<= 0 ,n)
        (< ,n (expt 2 ,b))))

(std::defaggregate bounds
    ((top  (and (natp top) (<= top (expt 2 *xlen*))))
     (base (nbp base *xlen*))))

;; See Section 3.5.4 of CHERI ISA v8 for decoding
;; --------------------------------------------------------
;; input compression,
;; returns (E: block size, TS: T[12:0], BL: B[14:0],
;;         Lcarry: carry bit of length=top-base, Lmsb=most significant bit of len)

;; for small block size mode
(defun vars-small-seg (c)
  (declare (xargs :guard (compressionp c)
                  :guard-debug T))
  (b* ((TS (nb-ash *mw* *tw* c))        ;; tshort=t[11:0]
       (BS (nb-ash 0 *tw* c))           ;; bshort=b[11:0]
       (BL (nb-ash 0 *mw* c))           ;; Blong=B[13:0]
       (Lcarry (if (< TS BS) 1 0)))
    ;; (E TS BL Lcarry Lmsb)
    (mv 0 TS BL Lcarry 0)))

;; for large block size mode
(defun vars-large-seg (c)
  (declare (xargs :guard (compressionp c)))
  (b* ((TE (nb-ash *mw* *eh* c))                    ;; e[5:3]
       (BE (nb-ash 0 *eh* c))                       ;; E[2:0]
       (E (logior (ash TE *eh*) BE))                ;; E[5:0]
       (TSH (nb-ash (+ *Mw* *eh*) (- *tw* *eh*) c)) ;; tshorthigh=t[11:3]
       (BSH (nb-ash *EH* (- *tw* *eh*) c))          ;; bshorthigh=b[11:3]
       (BLH (nb-ash *EH* (- *mw* *eh*) c))          ;; blonghigh=b[13:3]
       (Lcarry (if (< TSH BSH) 1 0))
       (TS (ash TSH *eh*))                          ;; T[11:0]
       (BL (ash BLH *eh*)))                         ;; B[13:0]
    ;; (E TS BL Lcarry Lmsb)
    (mv E TS BL Lcarry 1)))

; -----------------------------------------------------------------
;; out-of-range truncation could happen to B and T when base and top
;; are compressed, in which case the least significant bit of B / T
;; needs to be incremented / decremented
;; 
;; corrections returns 1 if T / B needs to be incremented,
;;                     -1                     decremented,
;;                     0    there is no out-of-range truncation
(defun correction (A R bound)
  (declare (xargs :guard (and (nbp A 3) (nbp bound 3)
                              (integerp R) (<= -1 R) (< R 7))))
  (cond ((and (< A R) (<= R bound)) -1)
        ((and (<= R A) (< bound R)) 1)
        (T 0)))

;; returns correction bits for TL and BL
(defun corrections (addr TL BL E) 
  (declare (xargs :guard (and (nbp addr *xlen*)
                              (nbp BL *mw*)
                              (nbp TL *mw*)
                              (natp E)
                              (<= E 52))
                  :verify-guards nil))
  (b* ((shift (1- *tw*))
       (AM (nb-ash (+ E shift) *eh* addr))
       (BM (nb-ash shift *eh* BL))
       (TM (nb-ash shift *eh* TL))
       (R (1- BM)))
    (mv (correction AM R TM)
        (correction AM R BM))))


(verify-guards corrections :hints (("Goal" :in-theory (disable ash logand))))

(encapsulate
 ()
 (local (def-gl-thm helper-1
            :hyp   (compressionp c)
            :concl (and 
                    (< (logior (ash (logand (ash (+ 1
                                                    (logand (ash (logand (ash c 0) 16383) -12)
                                                            3))
                                                 0)
                                            3)
                                    12)
                               (logand (ash c -14) 4095))
                       16384)
                    (< (ash (logand (ash c -3) 2047) 3)
                       16384)
                    (<
                     (logior (ash (logand (ash (+ 1
                                                  (logand (ash (ash (logand (ash c -3) 2047) 3)
                                                               -12)
                                                          3))
                                               0)
                                          3)
                                  12)
                             (ash (logand (ash c -17) 511) 3))
                     16384)
                    (< (logior (ash (logand (ash (logand (ash (logand (ash c 0) 16383) -12)
                                                         3)
                                                 0)
                                            3)
                                    12)
                               (logand (ash c -14) 4095))
                       16384)
                    (<
                     (logior (ash (logand (ash (+ 2
                                                  (logand (ash (ash (logand (ash c -3) 2047) 3)
                                                               -12)
                                                          3))
                                               0)
                                          3)
                                  12)
                             (ash (logand (ash c -17) 511) 3))
                     16384))
            :g-bindings `((c ,(gl::g-int 0 1 28)))))

 (with-arithmetic-help-5
     (defthm nb-ash-nbp
         (implies (and (natp b)
                       (natp shift)
                       (natp n))
                  (nbp (nb-ash shift b n) b))))

 (local
  (defthm bounds-p-nbp
     (implies (and (nbp base *xlen*)
                   (nbp top *xlen*))
              (bounds-p (bounds top base)))))

 (define decode-helper ((c compressionp) (addr (nbp addr *xlen*)))
   (b* ((IE (nb-ash (+ *mw* *tw*) 1 c))
        ((mv E TS BL Lcarry Lmsb)
         (if (= IE 0)
             (vars-small-seg c)
             (vars-large-seg c)))
        (BLH (+ (nb-ash *tw* (- *mw* *tw*) BL) Lcarry Lmsb))
        (TLH (nb-ash 0 (- *mw* *tw*) BLH)) ;; B[13:12] (nbp TLH 2)
        (TL (logior (ash TLH *tw*) TS))
        ((mv CT CB) (corrections addr TL BL E))
        (AT (ash addr (- (+ *mw* E)))))
       (mv AT CT CB BL TL E)))

 (in-theory (enable decode-helper))
 (local
  (defthm decode-helper-return-type
     (implies (and (compressionp c)
                   (nbp addr *xlen*))
              (b* (((mv AT CT CB BL TL E)
                    (decode-helper c addr)))
                  (and (natp AT)
                       (integerp CT)
                       (integerp CB)
                       (natp BL)
                       (natp TL)
                       (natp E))))))
 (in-theory (disable decode-helper))

 (local
  (defthm decode-helper-return-type-1
     (implies (and (compressionp c)
                   (nbp addr *xlen*))
              (b* (((mv AT CT CB BL TL E)
                    (decode-helper c addr)))
                  (and (integerp AT)
                       (integerp CT)
                       (integerp CB)
                       (integerp BL)
                       (integerp TL)
                       (integerp E))))
   :hints (("Goal" :use (decode-helper-return-type)
                   :in-theory (disable decode-helper-return-type)))))

 (local
  (defthm intergerp-implies
     (implies (integerp a)
              (and (rationalp a)
                   (acl2-numberp a)))))

 (local
  (defthmd intergerp-implies-addition
     (implies (and (integerp a)
                   (integerp b))
              (integerp (+ a b)))))

(define decode-compression ((c compressionp) (addr (nbp addr *xlen*)))
  :returns (res bounds-p :hyp :guard)
  :guard-hints (("Goal" :use (decode-helper-return-type-1)
                        :in-theory (disable decode-helper-return-type-1))
                ("subgoal 5" :use ((:instance intergerp-implies-addition
                                              (a (mv-nth 0 (decode-helper c addr)))
                                              (b (mv-nth 2 (decode-helper c addr))))))
                ("subgoal 6" :use ((:instance intergerp-implies-addition
                                              (a (mv-nth 0 (decode-helper c addr)))
                                              (b (mv-nth 1 (decode-helper c addr))))))
                ("subgoal 11" :use ((:instance intergerp-implies-addition
                                               (a (mv-nth 0 (decode-helper c addr)))
                                               (b (mv-nth 2 (decode-helper c addr))))))
                ("subgoal 12" :use ((:instance intergerp-implies-addition
                                               (a (mv-nth 0 (decode-helper c addr)))
                                               (b (mv-nth 1 (decode-helper c addr)))))))
  (b* (((mv AT CT CB BL TL E) (decode-helper c addr))

       (top (nb-ash 0 (1+ *xlen*)
                    (ash (logior (ash (+ AT CT) *mw*) TL) E)))
       (base (nb-ash 0 *xlen*
                     (ash (logior (ash (+ AT CB) *mw*) BL) E)))
       (top (if (and (< E 51)
                     (< 1 (- (nb-ash 63 2 top) (nb-ash 63 1 base))))
                (nb-ash 0 *xlen* top)
                (if (<= top (expt 2 *xlen*))
                    top
                    (nb-ash 64 0 top)))))
      ;; top, base
      (bounds top base))))


(defthmd bounds-p-decode
     (implies (and (compressionp c)
                   (nbp addr *xlen*))
              (bounds-p (decode-compression c addr))))

(encapsulate
 ()
 (local (def-gl-thm helper1
            :hyp   (compressionp c)
            :concl (< (logior (ash (logand (ash (+ 1
                                                   (logand (ash (logand (ash c 0) 16383) -12) 3))
                                                0)
                                           3)
                                   12)
                              (logand (ash c -14) 4095))
                      16384)
            :g-bindings `((c ,(gl::g-int 0 1 28)))))
 (local (def-gl-thm helper2
            :hyp   (compressionp c)
            :concl (< (logior (ash (logand (ash (logand (ash (logand (ash c 0) 16383) -12)
                                                        3)
                                                0)
                                           3)
                                   12)
                              (logand (ash c -14) 4095))
                      16384)
            :g-bindings `((c ,(gl::g-int 0 1 28)))))

 (local (def-gl-thm helper3
            :hyp   (compressionp c)
            :concl (<
                    (logior (ash (logand (ash (+ 2
                                                 (logand (ash (ash (logand (ash c -3) 2047) 3)
                                                              -12)
                                                         3))
                                              0)
                                         3)
                                 12)
                            (ash (logand (ash c -17) 511) 3))
                    16384)
            :g-bindings `((c ,(gl::g-int 0 1 28)))))

 (local (def-gl-thm helper4
            :hyp   (compressionp c)
            :concl  (< (ash (logand (ash c -3) 2047) 3)
                       16384)
            :g-bindings `((c ,(gl::g-int 0 1 28)))))

 (local (def-gl-thm helper5
            :hyp   (compressionp c)
            :concl (<
                    (logior (ash (logand (ash (+ 1
                                                 (logand (ash (ash (logand (ash c -3) 2047) 3)
                                                              -12)
                                                         3))
                                              0)
                                         3)
                                 12)
                            (ash (logand (ash c -17) 511) 3))
                    16384) 
            :g-bindings `((c ,(gl::g-int 0 1 28)))))

 (verify-guards decode-compression))

    

;; -------------------------------------------------------
;; END - guard verification
;; -------------------------------------------------------

;; -------------------------------------------------------
;; encode compression
;; -------------------------------------------------------
;; {seq[top: n] + 1, zero'n} - seq[top:0]
(defun align-gap (seq n)
  (declare (xargs :guard (and (integerp seq) (natp n))))
  (1+ (logxor (nb-ash 0 n seq)
              (1- (expt 2 n)))))

;; compute E
(encapsulate
 ()
 (local (include-book "ihs/logops-lemmas" :dir :system))
 (defthm e-helper-measure
     (implies (and (integerp len) (< 0 len))
              (natp (ash len -1)))
   :hints (("Goal" :use ((:instance ash-goes-to-0
                                    (size 1)
                                    (i len)
                                    (count -1)))
                   :in-theory (enable ash))))

 (defun e-helper (len)
   (declare (xargs :guard (natp len)
                   :hints (("Subgoal 2" :use e-helper-measure))))
   (if (zp len)
       0
       (1+ (e-helper (ash len -1)))))

 (defun e (len)
   (declare (xargs :guard (nbp len (1+ *xlen*))))
   (e-helper (nb-ash (1+ *tw*) (- *xlen* *tw*) len)))

 (defun ie (E len)
   (declare (xargs :guard (and (natp E)
                               (<= E 52)
                               (nbp len (1+ *xlen*)))))
   (b* ((Lmsb (nb-ash *tw* 1 len)))
       (if (and (= E 0) (= Lmsb 0)) 0 1)))

 (defmacro E+3-aligned-p (base len)
   `(b* ((E (e ,len)))
        (and (= (nb-ash 0 (+ E *eh*) ,base) 0)
             (= (nb-ash 0 (+ E *eh*) ,len) 0))))

 ;; if there is a carry-bit on [13:12] bits of the bounds, which
 ;; will force e to increase by one
 (defun carry? (overflow-flg top base e)
   (b* ((comp-bits (- *tw* *eh*))
        (t-2 (nb-ash (+ e *tw*) 2 top))
        (b-2 (nb-ash (+ e *tw*) 2 base))
        (bsh (nb-ash (+ e *eh*) comp-bits base))
        (tsh (nb-ash (+ e *eh*) comp-bits top)))
       ;; if B[13:12] and T[13:12] are two bits away,
       ;; and B[11:3] <= T[11:3], there is a carry bit
       (and (not overflow-flg)
            (= (logxor t-2 b-2) 2)
            (<= bsh tsh))))

 ;; returns e, T[E+11:E+3], and B[E+13:E+3] when IE=1
 (defun rounding (len base)
   (b* ((e (e len))
        (top (+ len base))
        (align (+ e *eh*))
        (gap (align-gap top align))
        ((mv n-e len)
         (if (< 0 (nb-ash 0 align top))
             ;; align top, and reset e and length if Lmsb changes
             (mv (e (+ len gap)) (+ len gap)) 
             (mv e len)))
        (overflow-flg (not (equal e n-e)))                    ;; len overflow-flg = t if E changes
        (carry-flg (carry? overflow-flg (+ base len) base e)) ;; compute if there is a carry bit
        (e (if (or carry-flg overflow-flg) (1+ e) e))         
        (top (+ base len))
        (tsh (nb-ash (+ e *eh*) (- *tw* *eh*) top))
        (tsh (if (< 0 (nb-ash 0 (+ e *eh*) top))
                 (nb-ash 0 (- *tw* *eh*) (1+ tsh))
                 tsh))
        (blh (nb-ash (+ e *eh*) (- *mw* *eh*) base)))
       (mv e
           (logior (ash tsh *eh*) (nb-ash *eh* *eh* e))
           (logior (ash blh *eh*) (nb-ash 0 *eh* e))))))


(defun encode-compression (len base)
  (declare (xargs :guard (and (natp len)
                              (<= len (expt 2 *xlen*))
                              (nbp base *xlen*)
                              (<= (+ base len) (expt 2 *xlen*)))
                  :verify-guards nil))
  (b* ((e (e len))          ;; E = 52 - CountLeadingZeros(len[64:13])
       (ie (ie e len))      ;; IE = if (E = 0 and len[12] = 0) then 0 else 1
       (top (+ base len)) 
       ((mv & ts bl)        ;; binds ts and bl to T[11:0] and  B[13:0], respectively
        (if (= ie 0)
            (mv 0
                (nb-ash 0 *tw* top)     ;; T[2:0]
                (nb-ash 0 *mw* base))   ;; B[13:0]
            (rounding len base))))
      ;; Update to IE unnecessary, since rounding up when IE is set means IE should still be set
      (logior (ash ie (+ *tw* *mw*))  ;; (nbp * 26)
              (ash ts *mw*) bl))) ;; (nbp * 26) ;; nbp bl 

;; ------------------------------------------------------
;; helper functions for properties hypothesis
;; ------------------------------------------------------
(defmacro valid-addr-p (addr base len)
  `(and (natp ,addr)
        (<= ,base ,addr)
        (< ,addr (+ ,base ,len))))

(defmacro valid-b-l-p (base len)
  `(and (natp ,len)
        (nbp ,base *xlen*)
        (<= (+ ,base ,len) (expt 2 *xlen*))))

;; output for encode-compression is 27 bit
;; len < 2^12
(def-gl-thm compressionp-encode-small-seg
    :hyp   (and (valid-b-l-p base len)
                (< len (expt 2 *TW*)))
    :concl (compressionp (encode-compression len base))
    :g-bindings `((base ,(gl::g-int 0 3 65))
                  (len ,(gl::g-int 1 3 66))))

;; len >= 2^12
(def-gl-param-thm compressionp-encode-large-seg
    :hyp   (and (valid-b-l-p base len)
                (<= (expt 2 *tw*) len))
    :concl (compressionp (encode-compression len base))
    :param-bindings
    `((((low 12) (high 16)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 16) (high 20)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 20) (high 24)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 24) (high 28)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 28) (high 32)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 32) (high 36)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 36) (high 40)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 40) (high 44)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 44) (high 48)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 48) (high 52)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 52) (high 56)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 56) (high 60)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 60) (high 64)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))
      (((low 64) (high 65)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65)))))
    :param-hyp (and (<= (expt 2 low) len) (< len (expt 2 high)))
    :cov-bindings (gl::auto-bindings (:mix (:nat base 65) (:nat len 65))))

;; ------------------------------------------------------------------------
;; properties of encode / decode functions
;; ------------------------------------------------------------------------
;; when IE = 0, encode-decode returns the same top and base
(def-gl-thm decode-encode-equal-small-seg
    :hyp   (and (valid-addr-p addr base len)
                (valid-b-l-p base len)
                (< len (expt 2 *TW*)))
    :concl (equal (decode-compression (encode-compression len base) addr)
                  (bounds (+ len base) base))
    :g-bindings `((base ,(gl::g-int 0 3 65))
                  (len ,(gl::g-int 1 3 66))
                  (addr ,(gl::g-int 2 3 65))))

(def-gl-thm decode-encode-equal-large-seg
    :hyp   (and (valid-addr-p addr base len)
                (valid-b-l-p base len)
                (<= (expt 2 *tw*) len)
                (E+3-aligned-p base len))
    :concl (equal (decode-compression (encode-compression len base) addr)
                  (bounds (+ len base) base))
    :g-bindings `((base ,(gl::g-int 0 3 65))
                  (len ,(gl::g-int 1 3 66))
                  (addr ,(gl::g-int 2 3 65))))

(def-gl-param-thm decode-encode-b-bound-len>2^12
    :hyp   (and (valid-addr-p addr base len)
                (valid-b-l-p base len)
                (<= (expt 2 *tw*) len))
    :concl (<= (bounds->base (decode-compression (encode-compression len base) addr))
               base)
    :param-bindings
    `((((low 12) (high 16)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 16) (high 20)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 20) (high 24)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 24) (high 28)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 28) (high 32)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 32) (high 36)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 36) (high 40)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 40) (high 44)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 44) (high 48)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 48) (high 52)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 52) (high 56)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 56) (high 60)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 60) (high 64)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 64) (high 65)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65)))))
    :param-hyp (and (<= (expt 2 low) len) (< len (expt 2 high)))
    :cov-bindings (gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))

(def-gl-param-thm decode-encode-t-bound-len>2^12
    :hyp   (and (valid-addr-p addr base len)
                (valid-b-l-p base len)
                (<= (expt 2 *tw*) len))
    :concl (<= (+ base len) 
               (bounds->top (decode-compression (encode-compression len base) addr)))
    :param-bindings
    `((((low 12) (high 16)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 16) (high 20)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 20) (high 24)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 24) (high 28)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 28) (high 32)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 32) (high 36)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 36) (high 40)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 40) (high 44)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 44) (high 48)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 48) (high 52)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 52) (high 56)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 56) (high 60)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 60) (high 64)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 64) (high 65)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65)))))
    :param-hyp (and (<= (expt 2 low) len) (< len (expt 2 high)))
    :cov-bindings (gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))

;; for proving b-base-gap
(defmacro E+3-expt (base len)
  `(b* ((e (mv-nth 0 (rounding ,len ,base))))       
       (expt 2 (+ 3 e))))

(def-gl-param-thm decode-encode-b-gap
    :hyp   (and (valid-addr-p addr base len)
                (valid-b-l-p base len)
                (<= (expt 2 *tw*) len))
    :concl (< (- base
                 (bounds->base (decode-compression (encode-compression len base) addr)))
              (E+3-expt base len))
    :param-bindings
    `((((low 12) (high 16)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 16) (high 20)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 20) (high 24)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 24) (high 28)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 28) (high 32)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 32) (high 36)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 36) (high 40)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 40) (high 44)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 44) (high 48)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 48) (high 52)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 52) (high 56)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 56) (high 60)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 60) (high 64)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 64) (high 65)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65)))))
    :param-hyp (and (<= (expt 2 low) len) (< len (expt 2 high)))
    :cov-bindings (gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))

(def-gl-param-thm decode-encode-t-gap
    :hyp   (and (valid-addr-p addr base len)
                (valid-b-l-p base len)
                (<= (expt 2 *tw*) len))
    :concl (< (+ (- (+ base len))
                 (bounds->top (decode-compression (encode-compression len base) addr)))
              (E+3-expt base len))
    :param-bindings
    `((((low 12) (high 16)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 16) (high 20)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 20) (high 24)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 24) (high 28)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 28) (high 32)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 32) (high 36)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 36) (high 40)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 40) (high 44)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 44) (high 48)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 48) (high 52)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 52) (high 56)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 56) (high 60)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 60) (high 64)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))
      (((low 64) (high 65)) ,(gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65)))))
    :param-hyp (and (<= (expt 2 low) len) (< len (expt 2 high)))
    :cov-bindings (gl::auto-bindings (:mix (:nat base 65) (:nat len 65) (:nat addr 65))))


;; CHERI Concentrate Compression permissions

;; Architectural capability permission bits
(std::defaggregate acperm
    ((gb  (nbp gb 1))    ;; GLOBAL - Allow cap to be stored via caps that do
                         ;; not themselves have PERMIT_STORE_LOCAL_CAPABILITY set
     (x   (nbp x 1))     ;; PERMIT_EXECUTE - Allow cap to be used in PCC register as cap
                         ;; for program counter, constraining control flow
     (l   (nbp l 1))     ;; PERMIT_LOAD - Allow cap to be used to load untagged data;
                         ;; requires PERMIT_LOAD_CAPABILITY to load tagged value
     (s   (nbp s 1))     ;; PERMIT_STORE - Allow cap to be used to store untagged data;
                         ;; requires PERMIT_STORE_CAPABILITY to store tagged value
     (lc  (nbp lc 1))    ;; PERMIT_LOAD_CAPABILITY - Allow cap to load caps with valid
                         ;; tags; requires PERMIT_LOAD
     (sc  (nbp sc 1))    ;; PERMIT_STORE_CAPABILITY - Allow cap to store caps with valid
                         ;; tags; requires PERMIT_STORE
     (slc (nbp slc 1))   ;; PERMIT_STORE_LOCAL_CAPABILITY - Allow cap to store non-global
                         ;; caps; requires PERMIT_STORE and PERMIT_STORE_CAPABILITY
     (sl  (nbp sl 1))    ;; PERMIT_SEAL - Allow cap to seal another cap with otype equal to
                         ;; this cap's base + offset
     (ci  (nbp ci 1))    ;; PERMIT_CINVOKE - Allow this sealed cap to be used with CInvoke instruction
     (usl (nbp usl 1))   ;; PERMIT_UNSEAL - Allow cap to unseal another cap with otype 
                         ;; equal to this cap's base + offset
     (cid (nbp cid 1)))) ;; PERMIT_SET_CID - Allow architectural compartment ID to be set
                         ;; to this cap's base + offset using CSetCID instruction

;; CHERI Concentrate capability permission bits
(std::defaggregate ccperm
    ((gb  (nbp gb 1))    ;; GLOBAL - Allow cap to be stored via caps that do
                         ;; not themselves have PERMIT_STORE_LOCAL_CAPABILITY set
     (x   (nbp x 1))     ;; PERMIT_EXECUTE - Allow cap to be used in PCC register as cap
                         ;; for program counter, constraining control flow
     (l   (nbp l 1))     ;; PERMIT_LOAD - Allow cap to be used to load untagged data;
                         ;; requires PERMIT_LOAD_CAPABILITY to load tagged value
     (s   (nbp s 1))     ;; PERMIT_STORE - Allow cap to be used to store untagged data;
                         ;; requires PERMIT_STORE_CAPABILITY to store tagged value
     (lc  (nbp lc 1))    ;; PERMIT_LOAD_CAPABILITY - Allow cap to load caps with valid
                         ;; tags; requires PERMIT_LOAD
     (sc  (nbp sc 1))    ;; PERMIT_STORE_CAPABILITY - Allow cap to store caps with valid
                         ;; tags; requires PERMIT_STORE
     (slc (nbp slc 1))   ;; PERMIT_STORE_LOCAL_CAPABILITY - Allow cap to store non-global
                         ;; caps; requires PERMIT_STORE and PERMIT_STORE_CAPABILITY
     (sl  (nbp sl 1))    ;; PERMIT_SEAL - Allow cap to seal another cap with otype equal to
                         ;; this cap's base + offset
     (ci  (nbp ci 1))    ;; PERMIT_CINVOKE - Allow this sealed cap to be used with CInvoke instruction
     (usl (nbp usl 1))   ;; PERMIT_UNSEAL - Allow cap to unseal another cap with otype 
                         ;; equal to this cap's base + offset
     (cid (nbp cid 1))   ;; PERMIT_SET_CID - Allow architectural compartment ID to be set
                         ;; to this cap's base + offset using CSetCID instruction
     (asr (nbp asr 1))   ;; ACCESS_SYSTEM_REGISTERS - Allow access to privileged
                         ;; processor permitted by the architecture (e.g., by virtue
                         ;; of being in supervisor mode), with architecture-specific implications. 
     (up0 (nbp up0 1))   ;; UPERMS bit 0 - software-defined permission bit
     (up1 (nbp up1 1))   ;; UPERMS bit 1 
     (up2 (nbp up3 1))   ;; UPERMS bit 2 
     (up3 (nbp up3 1)))) ;; UPERMS bit 3 

(std::defaggregate ccap
 ((perms ccperm-p)
  (otype (unsigned-byte-p *motw* otype))
  (bound (unsigned-byte-p *cmplen* bound))))

(define perms-bits-to-ccperm ((perms (nbp perms *pw*)))
  :verify-guards nil
  (b* ((gb  (nb-ash 15 1 perms))
       (x   (nb-ash 14 1 perms))
       (l   (nb-ash 13 1 perms))
       (s   (nb-ash 12 1 perms))
       (lc  (nb-ash 11 1 perms))
       (sc  (nb-ash 10 1 perms))
       (slc (nb-ash 9 1 perms))
       (sl  (nb-ash 8 1 perms))
       (ci  (nb-ash 7 1 perms))
       (usl (nb-ash 6 1 perms))
       (cid (nb-ash 5 1 perms))
       (asr (nb-ash 4 1 perms))
       (up0 (nb-ash 3 1 perms))
       (up1 (nb-ash 2 1 perms))
       (up2 (nb-ash 1 1 perms))
       (up3 (nb-ash 0 1 perms)))
      (ccperm gb x l s lc sc slc sl ci usl cid asr up0 up1 up2 up3)))

(def-gl-thm perms-bits-to-ccperm-guard
    :hyp   (and (nbp perms *pw*)
                (natp shift)
                (< shift *pw*))
    :concl (nbp (nb-ash shift 1 perms) 1)
    :g-bindings `((perms ,(gl::g-int 0 1 (1+ *pw*)))
                  (shift ,(gl::g-int (1+ *pw*) 1 5))))

(verify-guards perms-bits-to-ccperm
               :hints (("Goal" :use
                               ((:instance perms-bits-to-ccperm-guard (shift 1))
                                (:instance perms-bits-to-ccperm-guard (shift 2))
                                (:instance perms-bits-to-ccperm-guard (shift 3))
                                (:instance perms-bits-to-ccperm-guard (shift 4))
                                (:instance perms-bits-to-ccperm-guard (shift 5))
                                (:instance perms-bits-to-ccperm-guard (shift 6))
                                (:instance perms-bits-to-ccperm-guard (shift 7))
                                (:instance perms-bits-to-ccperm-guard (shift 8))
                                (:instance perms-bits-to-ccperm-guard (shift 9))
                                (:instance perms-bits-to-ccperm-guard (shift 10))
                                (:instance perms-bits-to-ccperm-guard (shift 11))
                                (:instance perms-bits-to-ccperm-guard (shift 12))
                                (:instance perms-bits-to-ccperm-guard (shift 13))
                                (:instance perms-bits-to-ccperm-guard (shift 14))
                                (:instance perms-bits-to-ccperm-guard (shift 15))))))

;;--------------------------------------------------------------------
;; acap from mcap conversion
;;--------------------------------------------------------------------

;; definition of architectual and memory capabilities
(define aotp (aot)
  (or (and (natp aot)
           (<= aot *max_otype*))
      (and (nbp aot *xlen*)
           (< (- (expt 2 *xlen*) 17)
              aot))))
(in-theory (enable aotp))

(std::defaggregate acap
    ((tag    (nbp tag 1))
     (flags  (nbp flags 1))
     (perms  (nbp perms *pw*)) 
     (ot     aotp)                              
     (offset (nbp offset *xlen*))
     (base   (nbp base *xlen*)) ;; base + offset --> pointer addr
     (len    (and (natp len) (<= len (expt 2 *xlen*)))) ;; base + len    --> upper bound
  ))       

(define cReservedOTypep ((cap acap-p))
  :returns (sealed booleanp)
  :guard-hints (("Goal" :in-theory (disable return-type-of-acap->ot)
                        :use ((:instance return-type-of-acap->ot
                                         (x cap)))))
 (b* ((ot (acap->ot cap)))
     (< *max_otype* ot)))
 
(defun mcapp (mcap)
  (declare (xargs :guard t))
  (and (nbp mcap *clen*)
       (compressionp (nb-ash *xlen* *cmplen* mcap))))

;; mcap to acap when mcap = 0
(define null-mcap-to-acap ()
  (b* ((perms 0)
       (flags 0)
       (tag 0)
       (ot (1- (expt 2 *xlen*))) 
       (offset 0)
       (base 0)
       (len (expt 2 *xlen*)))
    (acap tag flags perms ot offset base len)))

(define mot-to-aot ((mot (nbp mot *motw*)))
  :returns (aot aotp :hyp :guard)
  (if (< *max_otype* mot)
      (+ (- (expt 2 *xlen*) 17)
         (- mot *max_otype*))
      mot))

(encapsulate
 ()
 (local (defthm bounds-p-property
            (implies (bounds-p (decode-compression c addr))
                     (and (natp (bounds->top (decode-compression c addr)))
                          (<= (bounds->top (decode-compression c addr)) (expt 2 *xlen*))
                          (nbp (bounds->base (decode-compression c addr)) *xlen*)))
          :hints (("goal" :in-theory (disable return-type-of-bounds->base
                                              return-type-of-bounds->top)
                          :use ((:instance return-type-of-bounds->base
                                           (x (decode-compression c addr)))
                                (:instance return-type-of-bounds->top
                                           (x (decode-compression c addr))))))))
 (local (def-gl-thm mot-otlen
            :hyp   (mcapp mcap)
            :concl (or (and (natp (mot-to-aot (logand (ash mcap -91) 262143)))
                            (<= (mot-to-aot (logand (ash mcap -91) 262143)) *max_otype*))
                       (and (nbp (mot-to-aot (logand (ash mcap -91) 262143)) *xlen*)
                            (< 18446744073709551599
                               (mot-to-aot (logand (ash mcap -91) 262143)))))
            :g-bindings `((mcap ,(gl::g-int 0 1 (1+ *clen*))))))


 (local (defthm helper-2 
            (implies (mcapp mcap)
                     (integerp
                      (+ (logand (ash mcap 0)
                                 18446744073709551615)
                         (- (bounds->base (decode-compression (logand (ash mcap -64) 134217727)
                                                              (logand (ash mcap 0)
                                                                      18446744073709551615)))))))
          :hints (("Goal" :use ((:instance  bounds-p-property
                                            (c (logand (ash mcap -64) 134217727))
                                            (addr (logand (ash mcap 0) 18446744073709551615))))
                          :in-theory (disable bounds-p-property)))))

 (local (defthm helper-3
            (implies (mcapp mcap)
                     (integerp
                      (+ (- (bounds->base (decode-compression (logand (ash mcap -64) 134217727)
                                                              (logand (ash mcap 0)
                                                                      18446744073709551615))))
                         (bounds->top (decode-compression (logand (ash mcap -64) 134217727)
                                                          (logand (ash mcap 0)
                                                                  18446744073709551615))))))
          :hints (("Goal" :use ((:instance  bounds-p-property
                                            (c (logand (ash mcap -64) 134217727))
                                            (addr (logand (ash mcap 0) 18446744073709551615))))
                          :in-theory (disable bounds-p-property)))))
  
 (local (defthm helper-4
            (implies (mcapp mcap)
                     (< (+ (logand (ash mcap 0)
                                   18446744073709551615)
                           (- (bounds->base (decode-compression (logand (ash mcap -64) 134217727)
                                                                (logand (ash mcap 0)
                                                                        18446744073709551615)))))
                        18446744073709551616))
          :hints (("Goal" :use ((:instance  bounds-p-property
                                            (c (logand (ash mcap -64) 134217727))
                                            (addr (logand (ash mcap 0) 18446744073709551615))))
                          :in-theory (disable bounds-p-property)))))

 (local (defthm helper-5
            (implies (mcapp mcap)
                     (<=
                      (+ (- (bounds->base (decode-compression (logand (ash mcap -64) 134217727)
                                                              (logand (ash mcap 0)
                                                                      18446744073709551615))))
                         (bounds->top (decode-compression (logand (ash mcap -64) 134217727)
                                                          (logand (ash mcap 0)
                                                                  18446744073709551615))))
                      18446744073709551616))
          :hints (("Goal" :use ((:instance  bounds-p-property
                                            (c (logand (ash mcap -64) 134217727))
                                            (addr (logand (ash mcap 0) 18446744073709551615))))
                          :in-theory (disable bounds-p-property)))))

 ;; Non-zero mcap to acap
 (define mcap-to-acap-non-null ((mcap (and (mcapp mcap)
                                           (not (zp mcap)))))
   :returns (acap acap-p :hyp :guard)
   :guard-hints (("Goal" :in-theory (disable bounds-p-property)
                         :use ((:instance bounds-p-property
                                          (c (logand (ash mcap -64) 134217727))
                                          (addr (logand (ash mcap 0) 18446744073709551615)))))
                 ("Subgoal 22" :use ((:instance mot-otlen)))
                 ("Subgoal 20" :use ((:instance mot-otlen)))
                 ("Subgoal 16" :use ((:instance mot-otlen)))
                 ("Subgoal 15" :use ((:instance mot-otlen)))
                 ("Subgoal 7" :use ((:instance mot-otlen)))
                 ("Subgoal 5" :use ((:instance mot-otlen)))
                 ("Subgoal 2"  :use ((:instance mot-otlen)))
                 ("Subgoal 1"  :use ((:instance mot-otlen))))
   (b* ((perms (nb-ash (- *clen* *pw*) *pw* mcap))
        (tag (nb-ash (+ *motw* *cmplen* *xlen* *flglen*) 1 mcap)) 
        (flags (nb-ash (+ *motw* *cmplen* *xlen*) *flglen* mcap)) 
        (mot (nb-ash (+ *cmplen* *xlen*) *motw* mcap))      
        (ot (mot-to-aot mot))
        (cmp (nb-ash *xlen* *cmplen* mcap))
        (addr (nb-ash 0 *xlen* mcap))
        (bounds (decode-compression cmp addr))
        (base (bounds->base bounds))
        (top (bounds->top bounds))
        (offset (- addr base))
        (len (- top base)))
       (if (or (< addr base)
               (< top base))
           (change-acap (null-mcap-to-acap) :tag 0)
           (acap tag flags perms ot offset base len)))))


;; mcap to acap
(define mcap-to-acap ((mcap (nbp mcap *clen*)))
  :returns (acap acap-p)
  ;; :verify-guards nil
  (if (or (zp mcap) (not (mcapp mcap)))
      (null-mcap-to-acap)
      (mcap-to-acap-non-null mcap)))

;; check if acap should be converted to null mcap
(define null-acap-p ((acap (acap-p acap)))
  (equal (null-mcap-to-acap) acap))

(define aot-to-mot ((aot aotp))
  (nb-ash 0 *motw* aot))

;; perms :: _ :: tags :: flags :: ot :: comp :: addr
(define acap-to-mcap ((acap acap-p))
  ;; :returns (mcap mcapp)
  :verify-guards nil
  (if (null-acap-p acap)
      0
      (b* ((mbounds (encode-compression (acap->len acap)
                                        (acap->base acap)))
           (mot (aot-to-mot (acap->ot acap))))
          (logior (ash (acap->perms acap) (- *clen* *pw*))
                  (ash (acap->tag acap)
                       (+ *motw* *cmplen* *xlen* *flglen*))
                  (ash (acap->flags acap)
                       (+ *motw* *cmplen* *xlen*))
                  (ash mot (+ *cmplen* *xlen*))
                  (ash mbounds *xlen*)
                  (+ (acap->base acap) (acap->offset acap))))))


(def-gl-thm amcap-conversion-eq-small-seg
    :hyp   (and (valid-b-l-p base len)
                (nbp tag 1)
                (nbp flags *flglen*)
                (nbp perms *pw*)
                (aotp ot)
                (nbp offset *xlen*)
                (< len (expt 2 *TW*))
                (< offset len))
    :concl (equal (mcap-to-acap (acap-to-mcap (acap tag flags perms ot offset base len)))
                  (acap tag flags perms ot offset base len))
    :g-bindings `((tag ,(gl::g-int 0 1 2))
                  (flags ,(gl::g-int 2 1 2))
                  (perms ,(gl::g-int 4 1 (1+ *pw*)))
                  (ot ,(gl::g-int 21 4 65))
                  (offset ,(gl::g-int 22 4 65))
                  (base ,(gl::g-int 23 4 65))
                  (len ,(gl::g-int 24 4 66))))

(def-gl-thm amcap-conversion-eq-large-seg
    :hyp   (and (valid-b-l-p base len)
                (E+3-aligned-p base len)
                (nbp tag 1)
                (nbp flags *flglen*)
                (nbp perms *pw*)
                (aotp ot)
                (nbp offset *xlen*)
                (<= (expt 2 *TW*) len)
                (< offset len))
    :concl (equal (mcap-to-acap (acap-to-mcap (acap tag flags perms ot offset base len)))
                  (acap tag flags perms ot offset base len))
    :g-bindings `((tag ,(gl::g-int 0 1 2))
                  (flags ,(gl::g-int 2 1 2))
                  (perms ,(gl::g-int 4 1 (1+ *pw*)))
                  (ot ,(gl::g-int 21 4 65))
                  (offset ,(gl::g-int 22 4 65))
                  (base ,(gl::g-int 23 4 65))
                  (len ,(gl::g-int 24 4 66))))

;; ---------------------------------------------------------------------------
;; END of Cheri compression encode/decode + architectural/memory capabilities
;; ---------------------------------------------------------------------------

;; Some lemmas for translating between reading different word sizes
(defthm rm16-rm08
 (b* ((byte0 (rm08 (n24    addr) y86-64))
      (byte1 (rm08 (n24+ 1 addr) y86-64)))
     (implies (y86-64p y86-64)
              (equal (rm16 addr y86-64)
                     (n16 (logior      byte0
                                  (ash byte1 8))))))
 :hints (("Goal" :in-theory (enable rm08 rm16))))


(defthm rm32-rm08
 (b* ((byte0 (rm08 (n24    addr) y86-64))
      (byte1 (rm08 (n24+ 1 addr) y86-64))
      (byte2 (rm08 (n24+ 2 addr) y86-64))
      (byte3 (rm08 (n24+ 3 addr) y86-64)))
     (implies (y86-64p y86-64)
              (equal (rm32 addr y86-64)
                     (n32 (logior      byte0
                                  (ash byte1 8)
                                  (ash byte2 16)
                                  (ash byte3 24))))))
 :hints (("Goal" :in-theory (enable rm08 rm32))))


(defthm rm32-rm08
 (b* ((byte0 (rm08 (n24    addr) y86-64))
      (byte1 (rm08 (n24+ 1 addr) y86-64))
      (byte2 (rm08 (n24+ 2 addr) y86-64))
      (byte3 (rm08 (n24+ 3 addr) y86-64)))
     (implies (y86-64p y86-64)
              (equal (rm32 addr y86-64)
                     (n32 (logior      byte0
                                  (ash byte1 8)
                                  (ash byte2 16)
                                  (ash byte3 24))))))
 :hints (("Goal" :in-theory (enable rm08 rm32))))

(defthm rm64-rm08
 (b* ((byte0 (rm08 (n24    addr) y86-64))
      (byte1 (rm08 (n24+ 1 addr) y86-64))
      (byte2 (rm08 (n24+ 2 addr) y86-64))
      (byte3 (rm08 (n24+ 3 addr) y86-64))
      (byte4 (rm08 (n24+ 4 addr) y86-64))
      (byte5 (rm08 (n24+ 5 addr) y86-64))
      (byte6 (rm08 (n24+ 6 addr) y86-64))
      (byte7 (rm08 (n24+ 7 addr) y86-64)))
     (implies (y86-64p y86-64)
              (equal (rm64 addr y86-64)
                     (n64 (logior      byte0
                                  (ash byte1 8)
                                  (ash byte2 16)
                                  (ash byte3 24)
                                  (ash byte4 32)
                                  (ash byte5 40)
                                  (ash byte6 48)
                                  (ash byte7 56))))))
 :hints (("Goal" :in-theory (enable rm08 rm64))))

(defthm rm128-rm08
 (b* ((byte0 (rm08 (n24    addr) y86-64))
      (byte1 (rm08 (n24+ 1 addr) y86-64))
      (byte2 (rm08 (n24+ 2 addr) y86-64))
      (byte3 (rm08 (n24+ 3 addr) y86-64))
      (byte4 (rm08 (n24+ 4 addr) y86-64))
      (byte5 (rm08 (n24+ 5 addr) y86-64))
      (byte6 (rm08 (n24+ 6 addr) y86-64))
      (byte7 (rm08 (n24+ 7 addr) y86-64))
      (byte8 (rm08 (n24+ 8 addr) y86-64))
      (byte9 (rm08 (n24+ 9 addr) y86-64))
      (byte10 (rm08 (n24+ 10 addr) y86-64))
      (byte11 (rm08 (n24+ 11 addr) y86-64))
      (byte12 (rm08 (n24+ 12 addr) y86-64))
      (byte13 (rm08 (n24+ 13 addr) y86-64))
      (byte14 (rm08 (n24+ 14 addr) y86-64))
      (byte15 (rm08 (n24+ 15 addr) y86-64)))
     (implies (y86-64p y86-64)
              (equal (rm128 addr y86-64)
                     (n128 (logior      byte0
                                   (ash byte1 8)
                                   (ash byte2 16)
                                   (ash byte3 24)
                                   (ash byte4 32)
                                   (ash byte5 40)
                                   (ash byte6 48)
                                   (ash byte7 56)
                                   (ash byte8 64)
                                   (ash byte9 72)
                                   (ash byte10 80)
                                   (ash byte11 88)
                                   (ash byte12 96)
                                   (ash byte13 104)
                                   (ash byte14 112)
                                   (ash byte15 120))))))
 :hints (("Goal" :in-theory (enable rm08 rm128))))

(define get-capability ((addr n64p) (y86-64 y86-64p))
  (b* ((mcap (rm128 addr y86-64)))
      (mcap-to-acap mcap)))

(define get-reg-capability ((rd n04p) (y86-64 y86-64p))
  :guard (< rd 15)
  :returns (acap acap-p)
  (b* ((raw (n128 (rgfi rd y86-64))))
      (mcap-to-acap raw)))

;; temp constant for sentry otype
(defconst *sentry* (- (expt 2 *otw*) 2))
(defconst *unsealed* (- (expt 2 *otw*) 1))

;; A capability is sealed if its otype is 
;; not within 2^xlen - 16 <= otype < 2^xlen
(define cSealedp ((cap acap-p))
 :returns (sealed booleanp)
 :guard-hints (("goal''" :in-theory (disable return-type-of-acap->ot)
                         :use ((:instance return-type-of-acap->ot
                                          (x cap)))))
 (b* ((ot (acap->ot cap))) 
     (not (and (<= (- (expt 2 *xlen*) 16) ot) 
               (<  ot (expt 2 *xlen*))))))

;; A capability is valid if its tag bit is set
(define cValidp ((cap acap-p))
 :returns (sealed booleanp)
 (b* ((tag (acap->tag cap))) 
     (= tag 1)))

;; TODO: check
;; Has reserved otype?
;; (define cReservedOTypep ((cap acap-p))
;;  :returns (sealed booleanp)
;;  (b* ((ot (acap->ot cap)))
;;      (and (<= (- (expt 2 *otw*) 16) ot)
;;           (< ot (- (expt 2 *otw*) 2)))))

;; Returns true if capability can Permit Seal (if perms[7] == 1)
(define cPermitSealp ((cap acap-p))
 :returns (permitSeal booleanp)
 (b* ((perms (acap->perms cap)))
     (< 0 (nb-ash (- *pw* 8) 1 perms))))

;; Returns true if capability can Permit UnSeal (if perms[9] == 1)
(define cPermitUnsealp ((cap acap-p))
 :returns (permitUnseal booleanp)
 (b* ((perms (acap->perms cap)))
     (< 0 (nb-ash (- *pw* 10) 1 perms))))

;; Returns true if capability can Permit Global (if perms[0] == 1)
(define cPermitGlobal ((cap acap-p))
 :returns (permitGlobal booleanp)
 (b* ((perms (acap->perms cap)))
     (< 0 (nb-ash (- *pw* 1) 1 perms))))

;; Returns true if capability can Permit Execute (if perms[1] == 1)
(define cPermitExecute ((cap acap-p))
 :returns (permitExecute booleanp)
 (b* ((perms (acap->perms cap)))
     (< 0 (nb-ash (- *pw* 2) 1 perms))))

;; Returns perms that change global (index = 0)
(define setGlobal ((perms (nbp perms *pw*)) (glob booleanp))
  :returns (p n16p :hyp :guard)
  (if glob
      (logior perms (ash 1 15))
      (logand perms (lognot (ash 1 15)))))

;; Get address aka cursor
(define cGetAddress ((cap acap-p))
 :enabled t
 :returns (addr (or (nbp addr *xlen*)
                    (= addr -1)))
 (b* ((offset (+ (acap->base cap)
                 (acap->offset cap))))
     (if (nbp offset *xlen*)
         offset
         -1)))

;; Get base
(define cGetBase ((cap acap-p))
 :enabled t
 :returns (base (nbp base *xlen*) :hyp :guard
 :hints (("Goal''" :use ((:instance return-type-of-acap->base
                                             (x cap))))))
 (acap->base cap))

;; Get top
(define cGetTop ((cap acap-p))
 :enabled t
 :returns (top (or (and (natp top)
                        (<= top (expt 2 *xlen*)))
                   (= top -1))
               :hyp :guard)
 (b* ((top (+ (acap->base cap)
              (acap->len cap))))
     (if (and (natp top) (<= top (expt 2 *xlen*)))
        top 
        -1)))

(define invalid-valp ((val natp))
  :enabled t
  :returns (invalidp booleanp)
  (= val -1))

;; Get offset
(define cGetOffset ((cap acap-p))
 :enabled t
 :returns (offset (nbp offset *xlen*) :hyp :guard
 :hints (("Goal''" :use ((:instance return-type-of-acap->offset
                                             (x cap))))))
 (acap->offset cap))

;; Get perms
(define cGetPerms ((cap acap-p))
 :enabled t
 :returns (perms (nbp perms *pw*) :hyp :guard
 :hints (("Goal''" :use ((:instance return-type-of-acap->perms
                                             (x cap))))))
 (acap->perms cap))

;; Get tag
(define cGetTag ((cap acap-p))
 :enabled t
 :returns (tag (nbp tag 1) :hyp :guard
 :hints (("Goal''" :use ((:instance return-type-of-acap->tag
                                             (x cap))))))
 (acap->tag cap))

;; Get otype
(define cGetOtype ((cap acap-p))
 :enabled t
 :returns (otype aotp :hyp :guard
                 :hints (("Goal" :in-theory (disable return-type-of-acap->ot)
                                   :use ((:instance return-type-of-acap->ot
                                             (x cap))))))
 (acap->ot cap))

;; Get 1-bit flags
(define cGetFlags ((cap acap-p))
 :ignore-ok t
 :enabled t
 :returns (flags (nbp flags 1) :hyp :guard
 :hints (("Goal''" :use ((:instance return-type-of-acap->flags
                                             (x cap))))))
 (acap->flags cap))

;; Get High Half
(define cGetHi ((cap acap-p))
 :ignore-ok t
 :enabled t
 :verify-guards nil
 :returns (hi (nbp hi *xlen*))
 (n64 (ash (acap-to-mcap cap) (- *xlen*))))

(define cSetTag ((cap acap-p) (b (nbp b 1)))
 :enabled t
 :returns (new-cap acap-p :hyp :guard
                   :hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                                         (:instance return-type-of-acap->len (x cap))
                                         (:instance return-type-of-acap->base (x cap))
                                         (:instance return-type-of-acap->offset (x cap))
                                         (:instance return-type-of-acap->ot (x cap))
                                         (:instance return-type-of-acap->flags (x cap))))))
 :guard-hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                             (:instance return-type-of-acap->len (x cap))
                             (:instance return-type-of-acap->base (x cap))
                             (:instance return-type-of-acap->offset (x cap))
                             (:instance return-type-of-acap->ot (x cap))
                             (:instance return-type-of-acap->flags (x cap)))))
 (change-acap cap :tag b))

;; returns (t, new-cap) if offset is representable,
;;                      where new-cap->offset = n-offset
;; returns (nil,   cap) if offset is unrepresentable.
(define cSetOffset ((cap acap-p)
                    (n-offset natp))
  :enabled t
  :returns (sflag-cap (and (booleanp (car sflag-cap))
                           (acap-p (cdr sflag-cap)))
                      :hyp :guard
                      :hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                                            (:instance return-type-of-acap->len (x cap))
                                            (:instance return-type-of-acap->base (x cap))
                                            (:instance return-type-of-acap->offset (x cap))
                                            (:instance return-type-of-acap->ot (x cap))
                                            (:instance return-type-of-acap->tag (x cap))
                                            (:instance return-type-of-acap->flags (x cap))))))
 :guard-hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                             (:instance return-type-of-acap->len (x cap))
                             (:instance return-type-of-acap->base (x cap))
                             (:instance return-type-of-acap->offset (x cap))
                             (:instance return-type-of-acap->ot (x cap))
                             (:instance return-type-of-acap->tag (x cap))
                             (:instance return-type-of-acap->flags (x cap)))))
  (b* ((len (acap->len cap)))
      (if (< n-offset len) ;; offset < len -> new offset representable
          (cons t
                (change-acap cap :offset n-offset))
          (cons nil cap))))
          

;; returns (t,   new-cap) if addr is representable,
;; returns (nil, new-cap) if addr is not representable,
;;                        where new-cap->addr = n-addr
(define cSetAddr ((cap acap-p)
                  (n-addr (nbp n-addr *clen*)))
  :enabled t
  :returns (sflag-cap (and (booleanp (car sflag-cap))
                           (acap-p (cdr sflag-cap)))
                      :hyp :guard
                      :hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                                            (:instance return-type-of-acap->len (x cap))
                                            (:instance return-type-of-acap->base (x cap))
                                            (:instance return-type-of-acap->offset (x cap))
                                            (:instance return-type-of-acap->ot (x cap))
                                            (:instance return-type-of-acap->tag (x cap))
                                            (:instance return-type-of-acap->flags (x cap)))
                                      :in-theory (disable return-type-of-acap->base))))
  :guard-hints (("Subgoal 1" :use (:instance return-type-of-acap->base (x cap))
                             :in-theory (disable return-type-of-acap->base))
                ("Subgoal 2" :use (:instance return-type-of-acap->base (x cap))
                             :in-theory (disable return-type-of-acap->base)))
  (b* ((base (cGetBase cap)))
      (if (< n-addr base)
          (cons nil cap)
          (cSetOffset cap (- n-addr
                             (acap->base cap))))))


;; if bounds are aligned at E+3, then they can be represented exactly
(with-arithmetic-help-5
    (define cBoundExact ((E (nbp E 6))
                         (n-base (nbp n-base *xlen*))
                         (n-top (and (natp n-top)
                                     (<= n-top (expt 2 *xlen*)))))
      :enabled t
      :returns (exact-p booleanp)
      (and (= (logand (1- (expt 2 (+ E 3))) n-base) 0)
           (= (logand (1- (expt 2 (+ E 3))) n-top) 0))))
       

(define cSetBounds ((cap acap-p)
                    (n-base (nbp n-base *xlen*))
                    (n-top (and (natp n-top)
                                (<= n-top (expt 2 *xlen*))
                                (<= n-base n-top)))
                    (E (nbp E 6)))
  :enabled t
  :guard-hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                              (:instance return-type-of-acap->len (x cap))
                              (:instance return-type-of-acap->base (x cap))
                              (:instance return-type-of-acap->offset (x cap))
                              (:instance return-type-of-acap->ot (x cap))
                              (:instance return-type-of-acap->tag (x cap))
                              (:instance return-type-of-acap->flags (x cap)))))
  :returns (exactp-cap (and (booleanp (car exactp-cap))
                            (acap-p (cdr exactp-cap)))
                       :hyp :guard
                       :hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                                             (:instance return-type-of-acap->len (x cap))
                                             (:instance return-type-of-acap->base (x cap))
                                             (:instance return-type-of-acap->offset (x cap))
                                             (:instance return-type-of-acap->ot (x cap))
                                             (:instance return-type-of-acap->tag (x cap))
                                             (:instance return-type-of-acap->flags (x cap))))))
  (b* ((new-cap (change-acap cap :base n-base))
       (new-cap (change-acap new-cap :len (- n-top n-base)))
       (bound-exactp (cBoundExact E n-base n-top)))
      (cons bound-exactp new-cap)))


(define cSetPerms ((cap acap-p)
                   (perms (nbp perms *pw*)))
  :enabled t
  :returns (new-cap acap-p :hyp :guard
                    :hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                                          (:instance return-type-of-acap->len (x cap))
                                          (:instance return-type-of-acap->base (x cap))
                                          (:instance return-type-of-acap->offset (x cap))
                                          (:instance return-type-of-acap->ot (x cap))
                                          (:instance return-type-of-acap->tag (x cap))
                                          (:instance return-type-of-acap->flags (x cap))))))
  :guard-hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                              (:instance return-type-of-acap->len (x cap))
                              (:instance return-type-of-acap->base (x cap))
                              (:instance return-type-of-acap->offset (x cap))
                              (:instance return-type-of-acap->ot (x cap))
                              (:instance return-type-of-acap->tag (x cap))
                              (:instance return-type-of-acap->flags (x cap)))))
  (change-acap cap :perms perms))


(define cIncOffset ((cap acap-p)
                    (inc (nbp inc *clen*)))
  :enabled t
  :returns (sflag-cap (and (booleanp (car sflag-cap))
                           (acap-p (cdr sflag-cap)))
                      :hyp :guard
                      :hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                                            (:instance return-type-of-acap->len (x cap))
                                            (:instance return-type-of-acap->base (x cap))
                                            (:instance return-type-of-acap->offset (x cap))
                                            (:instance return-type-of-acap->ot (x cap))
                                            (:instance return-type-of-acap->tag (x cap))
                                            (:instance return-type-of-acap->flags (x cap)))
                                      :in-theory (disable return-type-of-acap->offset))))
  :guard-hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                              (:instance return-type-of-acap->len (x cap))
                              (:instance return-type-of-acap->base (x cap))
                              (:instance return-type-of-acap->offset (x cap))
                              (:instance return-type-of-acap->ot (x cap))
                              (:instance return-type-of-acap->tag (x cap))
                              (:instance return-type-of-acap->flags (x cap)))
                        :in-theory (disable return-type-of-acap->offset)))
  (b* ((len (acap->len cap))
       (n-offset (+ (acap->offset cap) inc)))
      (if (and (<= 0 n-offset)
               (< n-offset len))
          (cons t (change-acap cap :offset n-offset))
          (cons nil cap))))

(define cInCapBounds ((cap acap-p)
                      (n-base (nbp n-base *xlen*))
                      (len (nbp len *clen*)))
  :enabled t
  (b* ((base (cGetBase cap))
       (top (cGetTop cap)))
      (and (<= base n-base)
           (<= (+ n-base len) top))))

(define cInvalidateCap ((cap acap-p))
  :enabled t
  :returns (ncap acap-p
                 :hyp :guard
                 :hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                                       (:instance return-type-of-acap->len (x cap))
                                       (:instance return-type-of-acap->base (x cap))
                                       (:instance return-type-of-acap->offset (x cap))
                                       (:instance return-type-of-acap->ot (x cap))
                                       (:instance return-type-of-acap->flags (x cap))))))
  :guard-hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                              (:instance return-type-of-acap->len (x cap))
                              (:instance return-type-of-acap->base (x cap))
                              (:instance return-type-of-acap->offset (x cap))
                              (:instance return-type-of-acap->ot (x cap))
                              (:instance return-type-of-acap->flags (x cap)))))
  (change-acap cap :tag 0))

;; cut otype to just *otw*
(define cSealCap ((cap acap-p)
                  (otype (nbp otype *motw*)))
  :enabled t
  :returns (ncap acap-p
                 :hyp :guard
                 :hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                                       (:instance return-type-of-acap->len (x cap))
                                       (:instance return-type-of-acap->base (x cap))
                                       (:instance return-type-of-acap->offset (x cap))
                                       (:instance return-type-of-acap->ot (x cap))
                                       (:instance return-type-of-acap->tag (x cap))
                                       (:instance return-type-of-acap->flags (x cap)))
                                 :in-theory (enable mot-to-aot))))
  :guard-hints (("Goal" :use ((:instance return-type-of-acap->perms (x cap))
                              (:instance return-type-of-acap->len (x cap))
                              (:instance return-type-of-acap->base (x cap))
                              (:instance return-type-of-acap->offset (x cap))
                              (:instance return-type-of-acap->ot (x cap))
                              (:instance return-type-of-acap->tag (x cap))
                              (:instance return-type-of-acap->flags (x cap)))
                        :in-theory (enable mot-to-aot)))
  (change-acap cap :ot (mot-to-aot otype)))

(with-arithmetic-help-5
    (defthm nbp-longer-bits
        (implies (and (nbp i b)
                      (<= b b0)
                      (natp b)
                      (natp b0))
                 (nbp i b0))))

;; -----------------------------------------------------------
;; Deprecated
;; -----------------------------------------------------------
;; CAPABILITIES                                 
; This is an initial attempt to model capabilities.  I initially
; tried to add them to the state stobj, but ran into all kinds of
; difficulties.  

; Each capability has the form (start-addr end-addr permissions).  An
; operation is allowed only if it is allowed by some capability.  If
; an operation is attempted that is not allowed, an exception is
; raised and execution terminates.

; An access attempt will be of the form (read addr number-of-bytes) or
; (write addr number-of-bytes).  Currently, I'm insisting that the
; entire block be sanctioned by one capability.  This is, I don't
; allow read on an 8-byte block where the first 4 bytes are allowed by
; one capability and the second 4 by another.  Might consider adding
; that later.


; For this version, I'm adding an initial simple version of
; capabilities to the state.  These are not memory-resident, but
; rather a list of capabilities.  At first I tried to add them to the
; state stobj, but this didn't work very well because stobjs
; apparently don't allow components whose sizes can vary arbitrarily.

; Each capability has the form (start-addr end-addr permissions).  An
; operation is allowed only if it is allowed by some capability.  If
; an operation is attempted that is not allowed, an exception is
; raised and execution terminates.

(defmacro cap-start-addr (cap) `(car ,cap))
(defmacro cap-end-addr (cap) `(cadr ,cap))
(defmacro cap-permissions (cap) `(caddr ,cap))

(defun cap-permissionp (p) 
  (declare (xargs :guard t))
  (member-equal p '(r w rw)))

(defun capabilityp (cap)
  (declare (xargs :guard t))
  (and ;; The first four hyps imply that 
       ;; (true-listp cap) and (equal (len cap) 3)
       (consp cap)
       (consp (cdr cap))
       (consp (cddr cap))
       (null (cdddr cap))
       ;; >> should these be n64p ?
       (n24p (cap-start-addr cap))
       (n24p (cap-end-addr cap))
       (<= (cap-start-addr cap) (cap-end-addr cap))
       (cap-permissionp (cap-permissions cap))))

(defun capability-listp (l)
  (declare (xargs :guard t))
  ; >> I previously had (endp l) here, but that throws
  ;    a verify-guard error because endp has the guard 
  ;    of (or (consp l) (eq l nil)). 
  (if (atom l)
      t
    (and (capabilityp (car l))
	 (capability-listp (cdr l)))))

;; For now I'm going to assume that a single capability allows access
;; to the entire range.  Might change that later.

(defun cap-allows-readp (cap addr n)
  ;; The capability allows read access to all of the 
  ;; addresses [addr .. addr + (n - 1)].  E.g., if 
  ;; n = 8, want read access to [addr .. addr+7].
  (declare (xargs :guard t))
  (and (capabilityp cap)
       (n24p addr)
       (posp n)
       (n24p (1- (+ addr n)))
       (<= (cap-start-addr cap) addr)
       (<= (1- (+ addr n)) (cap-end-addr cap))
       (or (equal (cap-permissions cap) 'r)
	   (equal (cap-permissions cap) 'rw))))

(defun cap-allows-writep (cap addr n)
  ;; The capability allows write access to all of the 
  ;; addresses [addr .. addr + (n - 1)].  E.g., if 
  ;; n = 8, want write access to [addr .. addr+7].
  (declare (xargs :guard t))
  (and (capabilityp cap)
       (n24p addr)
       (posp n)
       (n24p (1- (+ addr n)))
       (<= (cap-start-addr cap) addr)
       (<= (1- (+ addr n)) (cap-end-addr cap))
       (or (equal (cap-permissions cap) 'w)
	   (equal (cap-permissions cap) 'rw))))

(defun cap-list-allows-readp (cap-list addr n)
  ; (declare (xargs :guard (true-listp cap-list)))
  (declare (xargs :guard t))
  (if (atom cap-list)
      nil
    (or (cap-allows-readp (car cap-list) addr n)
	(cap-list-allows-readp (cdr cap-list) addr n))))

(defun cap-list-allows-writep (cap-list addr n)
  ;(declare (xargs :guard (true-listp cap-list)))
  (declare (xargs :guard t))
  (if (atom cap-list)
      nil
    (or (cap-allows-writep (car cap-list) addr n)
	(cap-list-allows-writep (cdr cap-list) addr n))))
