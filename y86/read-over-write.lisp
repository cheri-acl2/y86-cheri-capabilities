; read-over-write.lisp                              

(in-package "ACL2")

; All theorems in this file are local except for those that are to be exported,
; which are labeled with the comment "; Exported theorem:".  We use def-gl-thm
; (actually a local version) so dispatch lemmas that were seen to be needed
; upon analysis of simplification checkpoints.  Without def-gl-thm, we would
; consider using one of these libraries:

; (local (include-book "arithmetic-5/top" :dir :system))
; (local (include-book "rtl/rel11/lib/top" :dir :system))

; That would perhaps take considerable work, however, because wm08 uses lognot,
; which doesn't appear in many lemmas in the above libraries.  Less important
; is that we might have to disable some rules, e.g. ash-rewrite in the latter
; case.  Fortunately, finding lemmas in either of the above books isn't hard,
; for example using the find-lemmas utility:

; (local (include-book "misc/find-lemmas" :dir :system))

(include-book "y86-64-state")

;; Several of these are redundant from y86-64-state.lisp.

; Exported theorem:
(defthm rgfi-!rgfi
  ;; Read over write to a register.
  (equal (rgfi i (!rgfi i v y86-64))
         v)
  :hints (("Goal" :in-theory (enable rgfi !rgfi))))

; Exported theorem:
(defthm rgfi-read-through-different-address-!rgfi
  ;; Read over write to a different register.
  (implies (and (n04p i)
                (n04p j)
                (not (equal i j)))
           (equal (rgfi i (!rgfi j v y86-64))
                  (rgfi i y86-64)))
  :hints (("Goal" :in-theory (enable rgfi !rgfi))))

; Exported theorem:
(defthm memi-!memi
  ;; Read over write to a memory address.
  (equal (memi i (!memi i v y86-64))
         v)
  :hints (("Goal" :in-theory (enable memi !memi))))
