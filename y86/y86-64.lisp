; y86-64.lisp                             

; (ld "y86-64.lisp" :ld-pre-eval-print t)
; (certify-book "y86-64" ?)
; (include-book "y86-64")

(in-package "ACL2")

(include-book "helpers")
(include-book "std/util/bstar" :dir :system)
(include-book "capabilities")

;; constant write and read capability addresses.
(defconst *r-cap-ub*  1099)
(defconst *r-cap-lb*  1098)
(defconst *w-cap-ub* 1999)
(defconst *w-cap-lb* 1998)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                  ;;
;;                        The Y86 simulator                         ;;
;;                                                                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Before we execute each instruction, we check to see that the entire
; instruction can be read and :RIP updated (and eventually stored)
; without breaking any addressing invariant (meaning what range of
; addresses can be stored in machine registers).  Generally, we don't
; allow reads and/or writes to memory to "wrap around" -- not that
; this doesn't make sense from a hardware perspective, but when
; programming with a system that include memory management it can't be
; relied on to work.

(defun y86-condition-code (y86-64 condition)
  ;; This generates the boolean from the three condition
  ;; codes that corresponds to the various logical 
  ;; conditions on conditional instructions like cmove,
  ;; jumps, etc. 
  (declare (xargs :guard (and (natp condition)
                              (< condition 7))
                  :stobjs (y86-64)))
  (b* ((condition (u03 condition))
       (of (u01 (of y86-64)))
       (sf (u01 (sf y86-64)))
       (zf (u01 (zf y86-64))))
    (logand
     1                ;; Why do I need this?
                      ;; Seems to be there to force a 1-bit result.
     (case condition
       ;; Yes
       (0 1)
       ;; less-equal (le)                     (SF ^ OF) | ZF
       (1 (logior (logxor sf of) zf))
       ;; less (l)                            (SF ^ OF) 
       (2 (logxor sf of))
       ;; equal (e,z)                         ZF
       (3 zf)
       ;; not equal (ne,nz)                   ~ZF
       (4 (lognot zf)) ; Negate ZF
       ;; greater-equal (ge)                  ~(SF ^ OF)
       (5 (logxor (logxor sf of) 1))
       ;; greater (g)                         ~(SF ^ OF) & ~ZF
       (6 (logxor (logior (logxor sf of) zf) 1))
       ;; Otherwise
       (t 0)))))

(defthm natp-y86-condition-code
  ;; Computing the condition yields a natp.
  (implies (y86-64p y86-64)
           (natp (y86-condition-code y86-64 condition)))
  :rule-classes :type-prescription)

(defthm bound-y86-condition-code
  ;; Computing the condition yields a natp in range [0...1].
  (implies (y86-64p y86-64)
           (and (<= 0 (y86-condition-code y86-64 condition))
                (< (y86-condition-code y86-64 condition) 2)))
  :rule-classes :linear)

(in-theory (disable y86-condition-code))

; Each instruction is defined individually, and afterwards, there is
; an instruction dispatch function.

;; HALT Instruction

(defund y86-halt (y86-64)
  ;; HALT only sets the status; doesn't update the PC.
  ;; There's no need for memory probe.
  (declare (xargs :guard t
                  :stobjs (y86-64)))
    (b* ((pc (rip y86-64)))
        (!ms (list :halt-at-location pc) y86-64)))

;; NOP Instruction

(defund y86-nop (y86-64)
  ;; NOP instruction.  Only updates the pc by 1. 
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 1-byte instruction?
       ((unless (< pc *2^64-2*))
        (!ms (list :at-location pc
                   :instruction 'nop
                   :rip-too-large t)
             y86-64))

       ;; Update PC
       ;; Note that we've already checked that that's OK, so don't 
       ;; need to wrap the pc computation.
       (y86-64 (!rip (+ pc 1) y86-64)))
    y86-64))

;; CMOVE Instruction
; This is a register-register operation
(defund y86-cmove (y86-64 condition)
  ;; CMOVE: conditional move instruction.
  (declare (xargs :guard (and (natp condition)
                              (< condition 7))
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 2-byte instruction?
       ((unless (< pc *2^64-3*))
        (!ms (list :at-location pc
                   :instruction 'cmove
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 1) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ;; Y86 has no R15.
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'cmove
                   :with-condition condition
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Condition true?
       (move? (y86-condition-code y86-64 condition))

       ;; When condition true, update register B
       (y86-64 (if (= move? 1)
                   (!rgfi rB
                          (rgfi rA y86-64)
                          y86-64)
                 y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 2) y86-64)))
    y86-64))

;; IRMOVQ Instruction

;; The condition is specified in the function nibble.
(defund y86-move-imm (y86-64)
  ;; IRMOVQ instruction.  
  (declare (xargs :guard t 
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 10-byte instruction?
       ((unless (< pc *2^64-11*))
        (!ms (list :at-location pc
                   :instruction 'irmovq
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 1) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((unless (and (= rA 15)
                     (not (= rB 15))))
        (!ms (list :at-location pc
                   :instruction 'irmovq
                   :ra rA :rb rB
                   :reason 'illegal-reg-specified)
             y86-64))

       ;; When condition true, imm -> rb
       ;; imm is the 64-bit quantity at pc + 2.
       (y86-64 (!rgfi rB
		      (rm64 (+ pc 2) y86-64)
		      y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 10) y86-64)))
    y86-64))

;; RRMOVQ Instruction
;; This instruction requires checking WRITE capability for the destination. 

(defund y86-rA-to-mem-at-rb+D (y86-64 cap-list)
  ;; RMMOVQ: register to memory move
  (declare (xargs :guard t 
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 10 byte instruction?
       ((unless (< pc *2^64-11*))
        (!ms (list :at-location pc
                   :instruction 'rmmovq
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 1) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'rmmovq
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; ra -> D(rb)
       (disp (rm64 (+ pc 2) y86-64))
       ;; Note, truncating displacement calculation
       (addr (n64+ (rgfi rB y86-64) disp))

       ;; Computed address too large?
       ;; Room in memory for 8-byte value without wrapping?
       ((unless (<= addr *2^64-8*))
        (!ms (list :at-location pc
                   :instruction 'rmmovq
                   :addr addr
                   :reason 'target-address-too-large)
             y86-64))

       ;; This is the check that the write to target is 
       ;; allowed by some capability in the capability list
       ;; argument. 
       ((unless (cap-list-allows-writep cap-list addr 8))
	(!ms (list :at-location pc
		   :instruction 'rmmovq
		   :addr addr
		   :reason 'no-write-capability)
	     y86-64 ))

       (ub (rm08 *w-cap-ub* y86-64))
       (lb (rm08 *w-cap-lb* y86-64))
       ((unless (and (<= lb addr) (<= addr ub)))
        (!ms (list :at-location pc
                   :instruction 'rmmovq
                   :addr addr
                   :reason 'addr-not-in-write-capability-bounds)
             y86-64))
       
       (rA-val (rgfi rA y86-64))

       (y86-64 (wm128 addr rA-val y86-64))
       ;; Update PC
       (y86-64 (!rip (+ pc 10) y86-64)))
      y86-64))

;; MRMOVQ Instruction
;; This instruction requires checking READ capability for the source

(defund y86-mem-at-rA+D-to-rB (y86-64 cap-list)
  ;; MRMOVQ: memory to regiser move
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for 10-byte instruction?
       ((unless (< pc *2^64-11*))
        (!ms (list :at-location pc
                         :instruction 'mrmovq
                         :rip-too-large t)
                   y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 1) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'mrmovq
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; D(rB) -> rA
       (disp (rm64 (+ pc 2) y86-64))
       ;; Note, truncating displacement calculation
       (addr (n64+ (rgfi rB y86-64) disp))

       ;; Computed address too large?
       ;; Read 8 bytes without wrapping?
       ((unless (<= addr *2^64-8*))
        (!ms (list :at-location pc
                   :instruction 'mrmovq
                   :addr addr
                   :reason 'target-address-too-large)
             y86-64))

       ;; This is the check that the read from source is 
       ;; allowed by some capability in the capability list
       ;; argument. 
       ((unless (cap-list-allows-readp cap-list addr 8))
	(!ms (list :at-location pc
       ;; this used to say rmmovq but now fixed
		   :instruction 'mrmovq 
		   :addr addr
		   :reason 'no-read-capability)
	     y86-64 ))
       (ub (rm08 *r-cap-ub* y86-64))
       (lb (rm08 *r-cap-lb* y86-64))
       ((unless (and (<= lb addr) (<= addr ub)))
        (!ms (list :at-location pc
                   :instruction 'mrmovq
                   :addr addr
                   :reason 'addr-not-in-read-capability-bonds)
             y86-64))

       (mem-data (rm64 addr y86-64))
       (y86-64 (!rgfi rA mem-data y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 10) y86-64)))
      y86-64))

;; This is used in some subsequent instructions

(defund y86-ALU-results-store-flgs (zf sf of y86-64)
  ;; Given new values of zf, sf, of flags, update
  ;; state with those values. 
  (declare (xargs :guard (and (n01p zf)
                              (n01p sf)
                              (n01p of))
                  :stobjs (y86-64)))
  (b* ((y86-64 (!zf zf y86-64))
       (y86-64 (!sf sf y86-64))
       (y86-64 (!of of y86-64)))
      y86-64))

;; XORQ Instruction

(defund y86-rA-xor-rB-to-rB (y86-64)
  ;; XORQ instruction
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 3-byte instruction?
       ((unless (< pc *2^64-3*))
        (!ms (list :at-location pc
                   :instruction 'xorq
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 1) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'xorq
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       ;; Get two register values
       (rA-val (rgfi rA y86-64))
       (rB-val (rgfi rB y86-64))

       ;; Calculate result and flags
       (result (logxor rA-val rB-val))
       (zf (if (= result 0) 1 0))
       (sf (n01 (ash result -63)))
       (of 0)

       ;; Store result and flags
       (y86-64 (!rgfi rB result y86-64))
       (y86-64 (y86-ALU-results-store-flgs zf sf of y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 2) y86-64)))
    y86-64))

;; ANDQ Instruction

(defund y86-rA-and-rB-to-rB (y86-64)
  ;; ANDQ instruction
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 2-byte instruction?
       ((unless (< pc *2^64-3*))
        (!ms (list :at-location pc
                   :instruction 'andq
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 1) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'andq
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       (rA-val (rgfi rA y86-64))
       (rB-val (rgfi rB y86-64))

       ;; Calculate result and flags
       (result (logand rA-val rB-val))
       (zf (if (= result 0) 1 0))
       (sf (n01 (ash result -63)))
       (of 0)

       ;; Store result and flags
       (y86-64 (!rgfi rB result y86-64))
       (y86-64 (y86-ALU-results-store-flgs zf sf of y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 2) y86-64)))
    y86-64))

;; ADDQ Instuction

(defund y86-rA-+-rB-to-rB (y86-64)
  ;; ADDQ instruction
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for 2-byte instruction?
       ((unless (< pc *2^64-3*))
        (!ms (list :at-location pc
                   :instruction 'addq
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 1) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'addq
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       (rA-val (rgfi rA y86-64))
       (rB-val (rgfi rB y86-64))

       ;; Calculate result
       (result (n64+ rA-val rB-val))

       ;; Calculate flags
       (zf  (if (= result 0) 1 0))
       (sfr (n01 (ash result -63)))
       (sfa (n01 (ash rA-val -63)))
       (sfb (n01 (ash rB-val -63)))
       (of  (if (= sfa sfb) ;; If equal sign, then all equal or overflow
                (if (= sfr sfa) 0 1)
              0))

       ;; Store result and flags
       (y86-64 (!rgfi rB result y86-64))
       (y86-64 (y86-ALU-results-store-flgs zf sfr of y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 2) y86-64)))
    y86-64))

;; SUBQ Instruction

(defund y86-rB---rA-to-rB (y86-64)
  ;; SUBQ instruction
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for 2-byte instruction?
       ((unless (< pc *2^64-3*))
        (!ms (list :at-location pc
                   :instruction 'subq
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 1) y86-64))
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((if (or (= rA 15) (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'subq
                   :ra rA :rb rB
                   :reason 'reg-15-specified)
             y86-64))

       (rA-val (rgfi rA y86-64))
       (rB-val (rgfi rB y86-64))
       ;; Calculate result
       ;; Take twos complement of rA-val by complement + 1.
       (rA-val-not (n64 (lognot rA-val)))


       (rA-val-tc (n64+ rA-val-not 1))
       (result (n64+ rB-val rA-val-tc))

       ;; To see if there was overflow:
       ;; coercing to n64p due to 128-bit sized registers
       (exact-result (- (n64-to-i64 (n64 rB-val)) (n64-to-i64 (n64 rA-val))))
       (integer-result (n64-to-i64 result))

       ;; Calculate flags
       (zf (if (= result 0) 1 0))
       (sf (n01 (ash result -63)))
       (of (if (= integer-result exact-result) 0 1))


       ;; Store result and flags
       (y86-64 (!rgfi rB result y86-64))
       (y86-64 (y86-ALU-results-store-flgs zf sf of y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 2) y86-64)))
    y86-64))

;; JXX Instruction
(defund y86-cjump (y86-64 condition)
  ;; Conditional jump instruction:
  (declare (xargs :guard (and (natp condition)
                              (< condition 7))
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for 9-byte instruction
       ((unless (< pc *2^64-10*))
        (!ms (list :at-location pc
                   :instruction 'cjump
                   :rip-too-large t)
             y86-64))

       ;; Condition true?
       (jump? (y86-condition-code y86-64 condition))

       ;; When condition true, update PC; otherwise, PC <- PC + 9
       (y86-64 (if (= jump? 1)
                   (!rip (rm64 (+ pc 1) y86-64) y86-64)
                 (!rip (+ pc 9) y86-64))))
    y86-64))

;; CALL Instruction
(defund y86-call (y86-64)
  ;; CALL instruction.
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for a 9-byte instruction?
       ((unless (< pc *2^64-10*))
        (!ms (list :at-location pc
                   :instruction 'call
                   :rip-too-large t)
             y86-64))

       ;; Note, new PC read from stack before stack updated because
       ;; current instruction might be inside the stack!
       (pc+9 (+ pc 9))
       (rsp (rgfi (reg-to-num :rsp) y86-64))
       ((if (< rsp 8))
        (!ms (list :at-location pc
                   :instruction 'call
                   :rsp rsp
                   :rsp 'rsp-less-than-8)
             y86-64))

       ;; Decrement stack pointer, push return pointer, update RSP
       (rsp-8 (- rsp 8))
       ;; Coercing to n64p due to new 128-bit registers
       (y86-64 (wm64 (n64 rsp-8) pc+9 y86-64))

       (y86-64 (!rgfi (reg-to-num :rsp) rsp-8 y86-64))

       ;; Update PC (Y86 RIP)
       (call-addr (rm64 (+ pc 1) y86-64))
       (y86-64 (!rip call-addr y86-64)))

      y86-64))

;; RET Instruction
(defund y86-ret (y86-64)
  ;; RET instruction
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* (;; No Memory Probe because PC is replaced.
       ;; Get return address and update stack pointer
       (rsp (rgfi (reg-to-num :rsp) y86-64))
       ((unless (< rsp *2^64-8*))
        (!ms (list :at-location (rip y86-64)
                   :instruction 'ret
                   :rsp-too-large rsp)
             y86-64))

       (rsp+8 (+ rsp 8))
       (y86-64 (!rgfi (reg-to-num :rsp) rsp+8 y86-64))

       ;; Update PC
       (new-pc (rm64 rsp y86-64)) ; Note use of initial RSP address
       (y86-64 (!rip new-pc y86-64)))
      y86-64))

;; PUSHQ Instruction

(defund y86-pushq (y86-64)
  ;; PUSHQ instruction: push contents of register onto the stack
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for 2-byte instruction?
       ((unless (< pc *2^64-3*))
        (!ms (list :at-location pc
                   :instruction 'pushq
                   :rip-too-large t)
                   y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 1) y86-64))
       ;; rB must be 15
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((unless (and (not (= rA 15))
                     (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'pushq
                   :ra rA :rb rB
		   :reason 'reg-15-problem)
             y86-64))

       (rsp (rgfi (reg-to-num :rsp) y86-64))
       ((unless (<= 8 rsp))
        (!ms (list :at-location pc
                   :instruction 'pushq
                   :rsp rsp
                   :rsp-too-small 'rsp-less-than-8)
             y86-64))

       (rsp-8 (n64- rsp 8))
       (valA (rgfi rA y86-64)) ; Read first, as it might be :rsp!
       (y86-64 (!rgfi (reg-to-num :rsp) rsp-8 y86-64))
       ;; Coercing to n64p due to new 128-bit registers
       (y86-64 (wm64 (n64 rsp-8) (n64 valA) y86-64))

       ;;  Update PC
       (y86-64 (!rip (+ pc 2) y86-64)))
      y86-64))

;; POPQ Instruction
(defund y86-popq (y86-64)
  ;; POPQ instruction: pop stack into register
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for 2-byte instruction?
       ((unless (< pc *2^64-3*))
        (!ms (list :at-location pc
                   :instruction 'popq
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 1) y86-64))
       ;; rB must be 15
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((unless (and (not (= rA 15))
                     (= rB 15)))
        (!ms (list :at-location pc
                   :instruction 'popq
                   :ra rA :rb rB
		   :reason 'reg-15-problem)
             y86-64))

       ;; Ordering is critical when :rsp is target, if POPQ needs to
       ;; overwrite the contents of the :rsp register.
       (rsp (rgfi (reg-to-num :rsp) y86-64))
       ((unless (< rsp *2^64-8*))
        (!ms (list :at-location pc
                   :instruction 'popq
                   :rsp rsp)
             y86-64))
       ;; Don't really need n64+ because we've already seen that it's
       ;; not within 8 of the top of memory. 
       (rsp+8 (n64+ rsp 8))

       ;; Read from the stack:
       (MemAtStackPt (rm64 rsp y86-64))
       ;; Update stack pointer
       (y86-64 (!rgfi (reg-to-num :rsp) rsp+8 y86-64))
       ;; Stack value to rA
       (y86-64 (!rgfi rA MemAtStackPt y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 2) y86-64)))
    y86-64))

;; IADDQ Instruction

(defund y86-imm-add (y86-64)
  ;; IADDQ instruction: 
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for 10-byte instruction?
       ((unless (< pc *2^64-11*))
        (!ms (list :at-location pc
                   :instruction 'iaddq
                   :rip-too-large t)
             y86-64))

       ;; Decode registers
       (rA-rB (rm08 (+ pc 1) y86-64))
       ;; rA must be 15; previously said rB must be 15.
       (rB (n04 rA-rB))
       (rA (n04 (ash rA-rB -4)))

       ;; Register decoding problem?
       ((unless (and (= rA 15)
		     (not (= rB 15))))
        (!ms (list :at-location pc
                   :instruction 'imm-add
                   :ra rA :rb rB
                   :reason 'reg-15-problem)
             y86-64))

       ;; imm + rb -> rb
       (y86-64 (!rgfi rB
		      (n64+ (rm64 (+ pc 2) y86-64)
			    (rgfi rB y86-64))
		      y86-64))

       ;; Update PC
       (y86-64 (!rip (+ pc 10) y86-64)))
    y86-64))


;; LEAVE Instruction
(defund y86-leave (y86-64)
  ;; LEAVE instruction: new instruction, equivalent to:
  ;;    RRMOVQ :rbp, :rsp
  ;;    POPQ   :rbp
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))

       ;; Memory Probe: room for 1-byte instruction
       ((unless (< pc *2^64-2*))
        (!ms (list :at-location pc
                   :instruction 'leave
                   :rip-too-large t)
             y86-64))

       (rsp (rgfi (reg-to-num :rsp) y86-64))
       ((unless (< rsp *2^64-8*))
        (!ms (list :at-location pc
                   :instruction 'leave
                   :rsp rsp
                   :rsp-too-large t)
             y86-64))

       ;; :rsp <- :rbp  ;; Set RSP to RBP (frame pointer)
       (y86-64 (!rgfi (reg-to-num :rsp)
                      (rgfi (reg-to-num :rbp) y86-64)
                      y86-64))

              ;; popq :rbp
       ;; Coercing to n64p due to new 128-bit registers
       (y86-64 (!rgfi (reg-to-num :rbp)
                      (rm64 (n64 (rgfi (reg-to-num :rsp) y86-64)) y86-64)
                      y86-64))
       (rsp+8 (n64+ rsp 8))

       (y86-64 (!rgfi (reg-to-num :rsp) rsp+8 y86-64))
       ;; Update PC
       (y86-64 (!rip (+ pc 1) y86-64)))
    y86-64))

;; Handle Illegal Opcode

(defund y86-illegal-opcode (y86-64)
  ;; Report an illegal opcode in status flag.
  (declare (xargs :guard t
                  :stobjs (y86-64)))
    (b* ((pc (rip y86-64))
         (byte-at-pc (rm08 pc y86-64)))
        (!ms (list :illegal-opcode byte-at-pc
                   :at-location pc)
                   y86-64)))

; Main instruction dispatch.

;; >> This didn't previously dispatch LEAVE.  I added it at #D0.

(defund y86-step (y86-64 cap-list)
  (declare (xargs :guard t
                  :stobjs (y86-64)))
  (b* ((pc (rip y86-64))
       (byte-at-pc (rm08 pc y86-64))
       (nibble-1 (logand byte-at-pc #xF0)))
      (case nibble-1

        ;; halt:  Stop the machine
        (#x00
         (case byte-at-pc
           (#x00 (y86-halt y86-64))
           (t    (y86-illegal-opcode y86-64))))

        ;; nop:  No-operation
        (#x10
         (case byte-at-pc
           (#x10 (y86-nop y86-64))
           (t    (y86-illegal-opcode y86-64))))

        ;; rrmovq, cmovle, cmovl, cmove, cmovne, cmovge, cmovg:
        ;; Conditional register-to-register move
        (#x20
         (case byte-at-pc
           (#x20 (y86-cmove y86-64 0))   ; rrmovq
           (#x21 (y86-cmove y86-64 1))   ; cmovle
           (#x22 (y86-cmove y86-64 2))   ; cmovl
           (#x23 (y86-cmove y86-64 3))   ; cmove
           (#x24 (y86-cmove y86-64 4))   ; cmovne
           (#x25 (y86-cmove y86-64 5))   ; cmovge
           (#x26 (y86-cmove y86-64 6))   ; cmovg
           (t   (y86-illegal-opcode y86-64))))

        ;; irmovq:  Conditional immediate-to-register move
	;; >>> Note that this doesn't match what's in the assembler,
	;; >>> which would need assembler op codes for these. 
        (#x30
         (case byte-at-pc
           (#x30 (y86-move-imm y86-64))  ; irmovq
           (t    (y86-illegal-opcode y86-64))))

;            (#x31 (y86-move-imm y86-64 1))  ; imm-move le
;            (#x32 (y86-move-imm y86-64 2))  ; imm-move l
;            (#x33 (y86-move-imm y86-64 3))  ; imm-move e
;            (#x34 (y86-move-imm y86-64 4))  ; imm-move ne
;            (#x35 (y86-move-imm y86-64 5))  ; imm-move ge
;            (#x36 (y86-move-imm y86-64 6))  ; imm-move g
;            (t    (y86-illegal-opcode y86-64))))

        ;; rmmovq:  rA -> D(rB))
        (#x40
         (case byte-at-pc
           (#x40 (y86-rA-to-mem-at-rb+D y86-64 cap-list))
           (t    (y86-illegal-opcode y86-64))))

        ;; mrmovq:  D(rA) -> rB
        (#x50
         (case byte-at-pc
           (#x50 (y86-mem-at-rA+D-to-rB y86-64 cap-list))
           (t    (y86-illegal-opcode y86-64))))

        ;; Arithmetic operations
        (#x60
         (case byte-at-pc
           ;; ra + rb -> rb
           (#x60 (y86-rA-+-rB-to-rB y86-64))
           ;; rb - ra -> rb
           (#x61 (y86-rB---rA-to-rB y86-64))
           ;; ra & rb -> rb
           (#x62 (y86-rA-and-rB-to-rB y86-64))
           ;; ra ^ rb -> rb
           (#x63 (y86-rA-xor-rB-to-rB y86-64))
           (t    (y86-illegal-opcode y86-64))))

        ;; jmp, jle, jl, je, jne, jge, jg:  Conditional jump
        (#x70
         (case byte-at-pc
           (#x70 (y86-cjump y86-64 0))
           (#x71 (y86-cjump y86-64 1))
           (#x72 (y86-cjump y86-64 2))
           (#x73 (y86-cjump y86-64 3))
           (#x74 (y86-cjump y86-64 4))
           (#x75 (y86-cjump y86-64 5))
           (#x76 (y86-cjump y86-64 6))
           (t    (y86-illegal-opcode y86-64))))

        ;; call:  :rip -> (--rsp);  imm -> PC
        (#x80
         (case byte-at-pc
           (#x80 (y86-call y86-64))
           (t    (y86-illegal-opcode y86-64))))

        ;; ret:  (:rsp--) -> :rip
        (#x90
         (case byte-at-pc
           (#x90 (y86-ret y86-64))
           (t    (y86-illegal-opcode y86-64))))

        ;; pushq:  rA -> (--rsp)
        (#xA0
         (case byte-at-pc
           (#xA0 (y86-pushq y86-64))
           (t    (y86-illegal-opcode y86-64))))

        ;; popq:  (rsp--) -> rA
        (#xB0
         (case byte-at-pc
           (#xB0 (y86-popq y86-64))
           (t    (y86-illegal-opcode y86-64))))

        ;; iaddq:  imm + rb -> rb
        (#xC0
         (case byte-at-pc
           (#xC0 (y86-imm-add y86-64))  
           (t    (y86-illegal-opcode y86-64))))


        (#xD0 (y86-leave y86-64))
        ;(#xD0 (y86-illegal-opcode y86-64))

        (#xE0 (y86-illegal-opcode y86-64))

        ;; noop:  No-operation
        (#xF0
         (case byte-at-pc
           (#xF0 (y86-nop y86-64))  ;; Dispatch performance test.
           (t    (y86-illegal-opcode y86-64))))

        (t (y86-illegal-opcode y86-64)))))

;; The Y86 Interpreter

(defund y86 (y86-64 n cap-list)
  ;; The Y86 interpreter with step count n,
  (declare (xargs :guard (natp n)
                  :measure (acl2-count n)
                  :stobjs (y86-64)))
  (if (mbe :logic (zp n) :exec (= n 0))
      y86-64
    ;; If status is other than nil, halt.
    (if (ms y86-64)
        y86-64
      (let ((y86-64 (y86-step y86-64 cap-list)))
        (y86 y86-64 (1- n) cap-list)))))

(defthmd y86-opener
  (implies (and (syntaxp (quotep n))
		(not (zp n))
		(not (ms y86-64)))
	   (equal (y86 y86-64 n cap-list)
		  (y86 (y86-step y86-64 cap-list)
		       (1- n)
		       cap-list)))
  :hints (("Goal" :in-theory (enable y86))))

(defthm y86-opener-zp
  (implies (and (syntaxp (quotep n))
		(zp n))
	   (equal (y86 y86-64 n cap-list)
		  y86-64))
  :hints (("Goal" :in-theory (enable y86))))

(defthm y86-opener-1
  (implies (not (ms y86-64))
	   (equal (y86 y86-64 1 cap-list)
		  (y86-step y86-64 cap-list)))
  :hints (("Goal" :in-theory (enable y86))))


(defthmd y86-opener-ms
  (implies (and (not (zp m))
		(not (ms y86-64))
		(ms (y86-step y86-64 cap-list)))
	   (equal (y86 y86-64 m cap-list)
		  (y86-step y86-64 cap-list)))
  :hints (("Goal" :in-theory (enable y86))))

(defthmd y86-opener-plus
  (implies (and (natp n)
		(natp m))
	   (equal (y86 y86-64 (+ m n) cap-list)
		  (y86 (y86 y86-64 m cap-list) n cap-list)))
  :hints (("Goal" :in-theory (enable y86 y86-opener-ms))))
  
; Now, we define a version of the y86 simulator with a means to halt
; execution based on a set of conditions.

; >> This seems to be building a primitive debugger.  See how to use this.

(defun symbol-integerp-alistp (alst)
  ;; Recognize a truelistp of pairs (symbol . integer).
  (declare (xargs :guard t))
  (if (atom alst)
      (null alst)
    (let ((key-val (car alst)))
      (and (consp key-val)
           (let ((key (car key-val))
                 (val (cdr key-val)))
             (and (symbolp key)
                  (integerp val)
                  (symbol-integerp-alistp (cdr alst))))))))

(defthm alistp-symbol-nat-alistp
  (implies (symbol-integerp-alistp a)
           (alistp a))
  :rule-classes :forward-chaining)

(defthm acl2-integerp-cdr-assoc-symbol-table
  (implies (and (symbol-integerp-alistp symbol-table)
                (assoc-equal expr symbol-table))
           (and (acl2-numberp (cdr (assoc-equal expr symbol-table)))
                (integerp     (cdr (assoc-equal expr symbol-table))))))

(defund =% (x y)
  (declare (xargs :guard (and (integerp x)
                              (integerp y))))
  (if (= x y) 1 0))

(defund 2% (x width)
  ;; Truncate x to the width rightmost bits.
  (declare (xargs :guard (and (integerp x)
                              (natp width))))
  (logand x (1- (expt 2 width))))

(defun y86-exprp (expr symbol-table)
  ;; Recognizer for an expression in this simple language.
  (declare (xargs :guard (symbol-integerp-alistp symbol-table)
                  :measure (acl2-count expr)))
  ;; An atom must be a symbol defined in the alist.
  (if (atom expr)
      (and (symbolp expr)
           (or (assoc expr symbol-table)
               (cw "Symbol not found: ~p0.~%" expr)))
    
    (let ((fn (car expr))
          (args (cdr expr)))
      (and (true-listp args)
           (consp (car args))
           (case fn
             ((quote 2^ not)
              (integerp (car args)))
             ((byte qword) (natp (car args)))
             (rgfi (and (natp (car args))
                        (< (car args) 15)))
             ((of sf zf) t)
             ((+ - * 2% = and ior xor)
              (and (consp (cadr args))
                   (y86-exprp (car args) symbol-table)
                   (y86-exprp (cadr args) symbol-table)))
             (otherwise nil))))))

(defun y86-expr-eval (y86-64 expr symbol-table)
  (declare (xargs :guard (and (symbol-integerp-alistp symbol-table)
                              (y86-exprp expr symbol-table))
                  :stobjs (y86-64)
                  :verify-guards nil))
  (if (atom expr)
      (cdr (assoc expr symbol-table))
    (let ((fn (car expr))
          (args (cdr expr)))
      (case fn
        (quote (car expr))
        (2^    (expt 2 (y86-expr-eval y86-64 (car expr)  symbol-table)))
        (not   (lognot (y86-expr-eval y86-64 (car expr)  symbol-table)))
        (byte  (rm08   (y86-expr-eval y86-64 (car expr)  symbol-table)
                       y86-64))
        (qword (rm64   (y86-expr-eval y86-64 (car expr)  symbol-table)
                       y86-64))
        (of    (of     y86-64))
        (sf    (sf     y86-64))
        (zf    (zf     y86-64))
        (+     (+      (y86-expr-eval y86-64 (car args)  symbol-table)
                       (y86-expr-eval y86-64 (cadr args) symbol-table)))
        (-     (-      (y86-expr-eval y86-64 (car args)  symbol-table)
                       (y86-expr-eval y86-64 (cadr args) symbol-table)))
        (*     (*      (y86-expr-eval y86-64 (car args)  symbol-table)
                       (y86-expr-eval y86-64 (cadr args) symbol-table)))
        (2%    (2%     (y86-expr-eval y86-64 (car args)  symbol-table)
                       (nfix (y86-expr-eval y86-64 (cadr args) symbol-table))))
        (=     (=%     (y86-expr-eval y86-64 (car args)  symbol-table)
                       (y86-expr-eval y86-64 (cadr args) symbol-table)))
        (and   (logand (y86-expr-eval y86-64 (car args)  symbol-table)
                       (y86-expr-eval y86-64 (cadr args) symbol-table)))
        (ior   (logior (y86-expr-eval y86-64 (car args)  symbol-table)
                       (y86-expr-eval y86-64 (cadr args) symbol-table)))
        (xor   (logxor (y86-expr-eval y86-64 (car args)  symbol-table)
                      (y86-expr-eval y86-64 (cadr args) symbol-table)))
        (otherwise 0)))))

(defthm acl2-integerp-y86-expr-eval
  (implies (and (y86-64p y86-64)
                (symbol-integerp-alistp symbol-table)
                (y86-exprp expr symbol-table))
           (and (acl2-numberp (y86-expr-eval y86-64 expr symbol-table))
                (integerp (y86-expr-eval y86-64 expr symbol-table)))))

(verify-guards y86-expr-eval)

(in-theory (disable y86-expr-eval))

(defund y86-debug (y86-64 condition symbol-table n cap-list)
  (declare (xargs :guard (and (symbol-integerp-alistp symbol-table)
                              (y86-exprp condition symbol-table)
                              (natp n))
                  :measure (acl2-count n)
                  :stobjs (y86-64)))
  (if (mbe :logic (zp n) :exec (= n 0))
      y86-64
    (if (ms y86-64)
        y86-64
      (if (= 0 (y86-expr-eval y86-64 condition symbol-table))
          (!ms :condition-failed y86-64)
        (let ((y86-64 (y86-step y86-64 cap-list)))
          (y86-debug y86-64 condition symbol-table (1- n) cap-list))))))


; State lemmas.

;; Each of the instructions preserves a good state.

(defthm y86-64p-y86-halt
  (implies (y86-64p y86-64)
           (y86-64p (y86-halt y86-64)))
  :hints (("Goal" :in-theory (enable y86-halt))))

(defthm y86-64p-y86-nop
  (implies (y86-64p y86-64)
           (y86-64p (y86-nop y86-64)))
  :hints (("Goal" :in-theory (enable y86-nop))))

(defthm y86-64p-y86-cmove
  (implies (y86-64p y86-64)
           (y86-64p (y86-cmove y86-64 condition)))
  :hints (("Goal" :in-theory (enable y86-cmove))))

;; This lemma is needed in order for ACL2 to recognize that updating
;; 128-bit registers with 64-bit values is still valid; more than one theorem in
;; this file requires it
(local
 (defthm n128p-of-n64p
  (implies (n64p v) (n128p v))))

(defthm y86-64p-y86-move-imm
  (implies (y86-64p y86-64)
           (y86-64p (y86-move-imm y86-64)))
  :hints (("Goal" :in-theory (enable y86-move-imm))))

;; added the cap-list argument to the following two lemmas.

(defthm y86-64p-y86-rA-to-mem-at-rb+D
  (implies (y86-64p y86-64)
           (y86-64p (y86-rA-to-mem-at-rb+D y86-64 cap-list)))
  :hints (("Goal" :in-theory (enable y86-rA-to-mem-at-rb+D))))

(defthm y86-64p-y86-mem-at-rA+D-to-rB
  (implies (y86-64p y86-64)
           (y86-64p (y86-mem-at-rA+D-to-rB y86-64 cap-list)))
  :hints (("Goal" :in-theory (enable y86-mem-at-rA+D-to-rB))))

(defthm y86-64p-y86-ALU-results-store-flgs
  (implies (and (n01p zf)
                (n01p sf)
                (n01p of)
                (y86-64p y86-64))
           (y86-64p (y86-ALU-results-store-flgs zf sf of y86-64)))
  :hints (("Goal" :in-theory (enable y86-ALU-results-store-flgs))))

(defthm y86-64p-y86-rA-xor-rB-to-rB
  (implies (y86-64p y86-64)
           (y86-64p (y86-rA-xor-rB-to-rB y86-64)))
  :hints (("Goal" :in-theory (enable y86-rA-xor-rB-to-rB))))

(defthm y86-64p-y86-rA-and-rB-to-rB
  (implies (y86-64p y86-64)
           (y86-64p (y86-rA-and-rB-to-rB y86-64)))
  :hints (("Goal" :in-theory (enable y86-rA-and-rB-to-rB))))

(defthm y86-64p-y86-rA-+-rB-to-rB
  (implies (y86-64p y86-64)
           (y86-64p (y86-rA-+-rB-to-rB y86-64)))
  :hints (("Goal" :in-theory (enable y86-rA-+-rB-to-rB))))

(defthm y86-64p-y86-rB---rA-to-rB
  (implies (y86-64p y86-64)
           (y86-64p (y86-rB---rA-to-rB y86-64)))
  :hints (("Goal" :in-theory (enable y86-rB---rA-to-rB))))

(defthm y86-64p-y86-cjump
  (implies (and (natp condition)
                (< condition 7)
                (y86-64p y86-64))
           (y86-64p (y86-cjump y86-64 condition)))
  :hints (("Goal" :in-theory (enable y86-cjump))))

(defthm y86-64p-y86-call
  (implies (y86-64p y86-64)
           (y86-64p (y86-call y86-64)))
  :hints (("Goal" :in-theory (enable y86-call))))

(defthm y86-64p-y86-ret
  (implies (y86-64p y86-64)
           (y86-64p (y86-ret y86-64)))
  :hints (("Goal" :in-theory (enable y86-ret))))

(defthm y86-64p-y86-pushq
  (implies (y86-64p y86-64)
           (y86-64p (y86-pushq y86-64)))
  :hints (("Goal" :in-theory (enable y86-pushq))))

(defthm y86-64p-y86-popq
  (implies (y86-64p y86-64)
           (y86-64p (y86-popq y86-64)))
  :hints (("Goal" :in-theory (enable y86-popq))))

(defthm y86-64p-y86-imm-add
  (implies (y86-64p y86-64)
           (y86-64p (y86-imm-add y86-64)))
  :hints (("Goal" :in-theory (enable y86-imm-add))))

(defthm y86-64p-y86-leave
  (implies (y86-64p y86-64)
           (y86-64p (y86-leave y86-64)))
  :hints (("Goal" :in-theory (enable y86-leave))))

(defthm y86-64p-y86-illegal-opcode
  (implies (y86-64p y86-64)
           (y86-64p (y86-illegal-opcode y86-64)))
  :hints (("Goal" :in-theory (enable y86-illegal-opcode))))

(defthm y86-64p-y86-step
  (implies (y86-64p y86-64)
           (y86-64p (y86-step y86-64 cap-list)))
  :hints (("Goal" :in-theory (enable y86-step))))

(defthm y86-64p-y86
  (implies (y86-64p y86-64)
           (y86-64p (y86 y86-64 n cap-list)))
  :hints (("Goal" :in-theory (enable y86))))

(defthm y86-64p-y86-debug
  (implies (y86-64p y86-64)
           (y86-64p (y86-debug y86-64 condition symbol-table n cap-list)))
  :hints (("Goal" :in-theory (enable y86-debug))))

(defthm ms-over-store-flags
  (equal (ms (y86-alu-results-store-flgs zf sf of y86-64))
	 (ms y86-64))
  :hints (("Goal" :in-theory (enable ms y86-alu-results-store-flgs
				     !zf !sf !of))))

(defthm flg-accessors-over-y86-alu-results-store-flgs
  (and (equal (zf (y86-alu-results-store-flgs z s o y86-64)) z)
       (equal (sf (y86-alu-results-store-flgs z s o y86-64)) s)
       (equal (of (y86-alu-results-store-flgs z s o y86-64)) o))
  :hints (("Goal" :in-theory (enable !zf !sf !of zf sf of
				     y86-alu-results-store-flgs))))

(defthm rm64-over-y86-alu-results-store-flgs
  (equal (rm64 addr (y86-alu-results-store-flgs z s o y86-64))
	 (rm64 addr y86-64))
  :hints (("Goal" :in-theory (enable !zf !sf !of zf sf of
				     y86-alu-results-store-flgs
				     memi rm64))))

(defthm rgfi-over-y86-alu-results-store-flgs
  (equal (rgfi i (y86-alu-results-store-flgs z s o y86-64))
	 (rgfi i y86-64))
  :hints (("Goal" :in-theory (enable !zf !sf !of zf sf of
				     y86-alu-results-store-flgs
				     rgfi))))

(defthm rm08-over-y86-alu-results-store-flgs
  (equal (rm08 addr (y86-alu-results-store-flgs zf sf of y86-64))
	 (rm08 addr y86-64))
  :hints (("Goal" :in-theory (enable rm08 y86-alu-results-store-flgs memi
				     !of !sf !zf))))
