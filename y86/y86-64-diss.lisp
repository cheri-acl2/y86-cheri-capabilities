;  y86-64-diss.lisp                  
; (ld "y86-64-diss.lisp" :ld-pre-eval-print t)

(in-package "ACL2")

(include-book "y86-64-asm")

; This is a disassembler for Y86 binary code, that includes the IADDL
; and the LEAVE instructions.

; This dissembler produced assembler-program sequences that can be
; given to our y86 assembler.  There are several core issues about
; disassembling y86 binary:

(defun diss-bytes (addr n y86-mem)
  ;; This just extracts the byte sequence of length n from memory
  ;; beginning at addr.  It returns a list of (address . byte-value)
  ;; pairs. 
  (declare (xargs :guard (and (n64p addr)
                              (n60p n)
                              (n64p-n08p-alistp y86-mem))))
  (let ((n (u60 n)))
    (if (mbe :logic (zp n)
             :exec  (= n 0))
        nil
      (cons (cons addr (hons-get addr y86-mem))
            (diss-bytes (n64+ addr 1)
                        (1- n)
                        y86-mem)))))

