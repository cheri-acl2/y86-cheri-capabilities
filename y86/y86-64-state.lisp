; y86-64-state.lisp                            

; (ld "y86-64-state.lisp" :ld-pre-eval-print t)

; We model the X86 state with two arrays: one array of 64-bit values
; for the registers and a hashtable of 8-bit values for memory
; addresses  There are a large number of registers, as this data
; structure holds all of the supervisor data as well.

(in-package "ACL2")

(include-book "misc-events")
(include-book "operations")
(include-book "constants")
;(include-book "y86-64-state")

; This is the basic state object for running the y86. 

(defstobj y86-64

; Notice that the array fields use UNSIGNED-BYTE types rather than
; types (SATISFIES N64P) and (SATISFIES N08P).  The reason is that we
; want to help the host Lisp system allocate these arrays using
; minimal real (physical) memory.

  ;; The model register file has a simple structure; it just an array
  ;; of 64-bit words.
  ;; >> This is 16, but should actually be 15.  This is compensated later
  ;;    by restrictions on the asm.
  (rgf :type (array (unsigned-byte 128) (*y86-64-reg-names-len*))
       :initially 0
       :resizable nil)

  ;; The program counter.
  (rip :type (unsigned-byte 64)
       :initially 0)

  ;; The processor flag register -- modeled after the X86.
  (zf :type (unsigned-byte 1) :initially 0)
  (sf :type (unsigned-byte 1) :initially 0)
  (of :type (unsigned-byte 1) :initially 0)

  ;; The y86 model memory is intended to represent a 2^64 byte physical
  ;; memory.  We maintain the byte-level semantics specified by the
  ;; x86 specification; however, our notion of a canonical address is
  ;; only the first 2^24 byte addresses. (Note gives the size, not the
  ;; largest address, which would be 1 less.)
  (mem :type (array (unsigned-byte 8) (*y86-64-max-mem-address*))
       :initially 0
       :resizable nil)

  ;; The state of the ACL2 model.  This flag is not part of the y86
  ;; processor; it is used to signal problems with model state, such
  ;; as the processor is halted.  While this flag is NIL, the
  ;; processor model is OK; otherwise, the flag indicates the problem.
  (ms :type t :initially nil)

  :inline t
  :non-memoizable t

  ;; The register update functions have same names as the access
  ;; functions, but with a "!" prefix.
  :renaming
  ((update-rgfi !rgfi)
   (update-rip  !rip)
   (update-zf   !zf)
   (update-sf   !sf)
   (update-of   !of)
   (update-memi !memi)
   (update-ms   !ms)))

; Event to observe all events introduced by DEFSTOBJ.

(defun get-stobj-raw-defs (form state)
  (declare (xargs :mode :program :stobjs (state)))
  (let* ((name (cadr form))
         (args (cddr form))
         (wrld (w state))
         (template (defstobj-template name args wrld)))
    (defstobj-raw-defs name template nil wrld)))

; Lemmas to help with proofs about STOBJs that hold X86 state.  Our
; goal is to prove a nice set of forward-chaining lemmas, as our
; predicates seem nicely set up for that.

(in-theory (disable nth))  ; Because NTH used to select object from
                           ; the y86-64 state.

; Lemmas below are in the same order as the fields declare in the
; X86-64 STOBJ above.

; We first deal with the STOBJ read lemmas

; RGF read lemmas

(defthm natp-nth-of-rgf
  ;; Read the register file.
  (implies (and (rgfp x)
                (integerp i)
                (<= 0 i)
                (< i (len x)))
           (natp (nth i x)))
  :rule-classes :type-prescription
  :hints (("Goal" :in-theory (e/d (nth) ()))))

(defthm nth-of-rgf-<-18446744073709551616
  ;; Contents of register are in range [0..2^64-1].
  (implies (and (rgfp x)
                (integerp i)
                (<= 0 i)
                (< i (len x)))
           (and (<= 0 (nth i x))
                (< (nth i x) (expt 2 128))));18446744073709551616)))
  :rule-classes :linear
  :hints (("Goal" :in-theory (e/d (nth) ()))))

(defthm natp-rgfi
  ;; Contents of register are nat.
  (implies (and (force (y86-64p y86))
                (force (n04p i)))
           (natp (rgfi i y86)))
  :rule-classes :type-prescription)

(defthm rgfi-less-than-expt-2-128
  ;; Contents of register are in range [0..2^128-1].
  ;; >> Allows i = 15. 
  (implies (and (y86-64p y86-64)
                (n04p i))
           (and (<= 0 (rgfi i y86-64))
                (< (rgfi i y86-64) (expt 2 128))));18446744073709551616)))
  :rule-classes :linear)


; RIP read lemmas

(defthm natp-rip
  ;; RIP is a nat.
  (implies (force (y86-64p y86-64))
           (natp (rip y86-64)))
  :rule-classes :type-prescription)

(defthm rip-less-than-expt-2-32
  ;; RIP is in range [0...2^64-1].
  (implies (y86-64p y86-64)
           (and (<= 0 (rip y86-64))
                (< (rip y86-64) 18446744073709551616)))
  :rule-classes :linear)

; FLG read lemmas

(defthm natp-of
  ;; Overflow flag is a nat.
  (implies (force (y86-64p y86-64))
           (natp (of y86-64)))
  :rule-classes :type-prescription)

(defthm of-less-than-2
  ;; Overflow flag is in range [0...1].
  (implies (y86-64p y86-64)
           (and (<= 0 (of y86-64))
                (< (of y86-64) 2)))
  :rule-classes :linear)

(defthm natp-sf
  ;; Sign flag is a nat.
  (implies (force (y86-64p y86-64))
           (natp (sf y86-64)))
  :rule-classes :type-prescription)

(defthm sf-less-than-2
  ;; Sign flag is in range [0...1].
  (implies (y86-64p y86-64)
           (and (<= 0 (sf y86-64))
                (< (sf y86-64) 2)))
  :rule-classes :linear)

(defthm natp-zf
  ;; Zero flag is a nat.
  (implies (force (y86-64p y86-64))
           (natp (zf y86-64)))
  :rule-classes :type-prescription)

(defthm zf-less-than-2
  ;; Zero flag is in range [0...1].
  (implies (y86-64p y86-64)
           (and (<= 0 (zf y86-64))
                (< (zf y86-64) 2)))
  :rule-classes :linear)

; MEM read lemmas

(defthm natp-nth-of-mem
  ;; Contents of memory (bytes) are nats.
  (implies (and (memp x)
                (integerp i)
                (<= 0 i)
                (< i (len x)))
           (natp (nth i x)))
  :rule-classes :type-prescription
  :hints (("Goal" :in-theory (e/d (nth)))))

(defthm nth-of-mem-<=-256
  ;; Contents of memory (bytes) are in range [0...255].
  (implies (and (memp x)
                (integerp i)
                (<= 0 i)
                (< i (len x)))
           (and (<= 0 (nth i x))
                (< (nth i x) 256)))
  :rule-classes :linear
  :hints (("Goal" :in-theory (e/d (nth) ()))))

(defthm natp-memi-when-n30p-addr
  ;; Elements of memory from state with 24-bit address
  ;; are nats.
  (implies (and (force (y86-64p y86-64))
                (force (n24p addr)))
           (natp (memi addr y86-64)))
  :rule-classes (:rewrite :type-prescription))

(defthm memi-less-than-expt-2-8
  ;; Elements of memory from state with 24-bit address
  ;; are in range [0...255].
  (implies (and (y86-64p y86-64)
                (n24p addr))
           (< (memi addr y86-64) 256))
  :rule-classes :linear)

; We wonder if the two lemmas about UPDATE-xxx would be better as
; :FORWARD-CHAINING rules (or, as both :REWRITE and :FORWARD-CHAINING
; rules), using *MEM-SIZE-IN-BYTES* and *REG-SIZE-IN-DWRDS* in the
; hypotheses instead of LEN.

(defthm length-is-len-when-not-stringp
  ;; Length is messy because it requires splitting on the 
  ;; case of a string.  
  (implies (not (stringp x))
           (equal (length x)
                  (len x)))
  :hints (("Goal" :in-theory (e/d (length) ()))))

; RGF update lemmas

(defthm rgfp-update-nth
  ;; Update to a register with a 64-bit nat is still a register.
  (implies (and (rgfp x)
                (integerp i)
                (<= 0 i)
                (< i (len x))
                (n128p v))
           (rgfp (update-nth i v x))))

(defthm y86-64p-update-rgfi-n04p
  ;; Update to a register from state with a 64-bit nat is still a register.
  (implies (and (y86-64p y86-64)
		;; >> This allows update to r15. 
                (n04p i)
                (n128p v))
           (y86-64p (!rgfi i v y86-64))))

; RIP update lemma

(defthm y86-64p-update-rip
  ;; Updating the RIP with a 64-bit nat preserves a legal state.
  (implies (and (y86-64p y86-64)
                (n64p v))
           (y86-64p (!rip v y86-64))))

; FLAGS update lemmas

(defthm y86-64p-update-of
  ;; Updating the overflow flag with a 1-bit nat preserves a legal state.
  (implies (and (y86-64p y86-64)
                (n01p v))
           (y86-64p (!of v y86-64))))

(defthm y86-64p-update-sf
  ;; Updating the sign flag with a 1-bit nat preserves a legal state.
  (implies (and (y86-64p y86-64)
                (n01p v))
           (y86-64p (!sf v y86-64))))

(defthm y86-64p-update-zf
  ;; Updating the zero flag with a 1-bit nat preserves a legal state.
  (implies (and (y86-64p y86-64)
                (n01p v))
           (y86-64p (!zf v y86-64))))

; Memory update lemmas

(defthm memp-update-nth
  ;; Updating a memory with an 8-bit nat preserves a legal
  ;; memory.
  (implies (and (memp x)
                (integerp i)
                (<= 0 i)
                (< i (len x))
                (n08p v))
           (memp (update-nth i v x))))

(defthm y86-64p-update-memi-n24p
  ;; Updating the y86 memory with an 8-bit nat preserves a legal
  ;; y86 state.
  (implies (and (force (y86-64p y86-64))
                (force (n24p i))
                (force (n08p v)))
           (y86-64p (!memi i v y86-64))))

; Model-State update lemma

(defthm y86-64p-update-ms
  ;; If the state is legal, updating the status flag
  ;; with an arbitrary value preserves a legal state.
  (implies (force (y86-64p y86-64))
           (y86-64p (!ms v y86-64))))


; Some additional state lemmas.

;; >> The following is true only because *rfgi* is declared 
;;    to be an array of length 16.  But there are actually 
;;    only 15 registers.

(defthm len-y86-64-rgf
  ;; The length of the register file is 16.
  (implies (y86-64p y86-64)
           (equal (len (nth *rgfi* y86-64))
                  *y86-64-reg-names-len*)))

(defthm len-y86-64-mem
  ;; The length of the memory is 2^24
  (implies (y86-64p y86-64)
           (equal (len (nth *memi* y86-64))
		  ;; This is currently 2^24.
                  *y86-64-max-mem-address*)))

(defthm y86-64p-properties
  (implies (y86-64p y86-64)
           (and (true-listp y86-64)
		;; There are 7 components of the state.
                (equal (len y86-64) 7)

		;; The 0th component is the register file.
                (rgfp (nth 0 y86-64))
                (equal (len (nth 0 y86-64))
                       *y86-64-reg-names-len*)

		;; The next four are: RIP, ZF, SF, OF.
                (ripp (nth 1 y86-64))
                (zfp  (nth 2 y86-64))
                (sfp  (nth 3 y86-64))
                (ofp  (nth 4 y86-64))

		;; The memory if the 5th component.
                (memp (nth 5 y86-64))
                (equal (len (nth 5 y86-64))
                       *y86-64-max-mem-address*)

		;; Status is the 6th component.
                (msp  (nth 6 y86-64))
                ))
  :hints (("Goal" :in-theory (enable y86-64p rgfp ripp zfp sfp ofp memp msp)))
  :rule-classes :forward-chaining)

; Hopefully, we have proven all the facts we need about the y86-64
; machine state.

(in-theory (disable y86-64p
                    rgfp rgfi !rgfi
                    ripp rip  !rip
                    zfp  zf   !zf
                    sfp  sf   !sf
                    zfp  of   !of
                    memp memi !memi
                    msp  ms   !ms))


; Read/Write memory routines

; !!! These memory routines truncate all addresses to the lower 24 bits. !!!


;; adding rm 16
(defun rm16 (addr y86-64)
  ;; Truncate address to 24 bits and access 2 consecutive bytes 
  ;; from state's memory. Assemble into 16-bit quantity in little
  ;; endian fashion.  Note that this allows wrap around, but that
  ;; will be precluded in the asm by constraints there.
  (declare (xargs :guard (n64p addr)
                  :stobjs (y86-64)))
  (let* ((byte0 (memi (n24    addr) y86-64))
         (byte1 (memi (n24+ 1 addr) y86-64)))
    (n16 (logior byte0
                 (ash byte1 8)))))

(defthm natp-rm16
  ;; Reading a 16-bit quantity from memory yields
  ;; a 16-bit natp.
  (implies (y86-64p y86-64)
           (n16p (rm16 addr y86-64)))
  :rule-classes :type-prescription)

(defthm bound-rm16
  ;; Reading a 64-bit quantity from memory yields
  ;; a value in range [0...2^16-1].
  (implies (y86-64p y86-64)
           (and (<= 0 (rm16 addr y86-64))
                (< (rm16 addr y86-64) (expt 2 16)))))

(in-theory (disable rm16))



;; adding rm 32
(defun rm32 (addr y86-64)
  ;; Truncate address to 24 bits and access 4 consecutive bytes 
  ;; from state's memory. Assemble into 32-bit quantity in little
  ;; endian fashion.  Note that this allows wrap around, but that
  ;; will be precluded in the asm by constraints there.
  (declare (xargs :guard (n64p addr)
                  :stobjs (y86-64)))
  (let* ((byte0 (memi (n24    addr) y86-64))
         (byte1 (memi (n24+ 1 addr) y86-64))
         (byte2 (memi (n24+ 2 addr) y86-64))
         (byte3 (memi (n24+ 3 addr) y86-64)))

    (n32 (logior byte0
                 (ash byte1 8)
                 (ash byte2 16)
                 (ash byte3 24)))))

(defthm natp-rm32
  ;; Reading a 32-bit quantity from memory yields
  ;; a 32-bit natp.
  (implies (y86-64p y86-64)
           (n32p (rm32 addr y86-64)))
  :rule-classes :type-prescription)

(defthm bound-rm32
  ;; Reading a 64-bit quantity from memory yields
  ;; a value in range [0...2^32-1].
  (implies (y86-64p y86-64)
           (and (<= 0 (rm32 addr y86-64))
                (< (rm32 addr y86-64) (expt 2 32)))))

(in-theory (disable rm32))



(defun rm08 (addr y86-64)
  ;; Truncate address to 24 bits and access from state's memory.
  ;; Result is one byte.
  (declare (xargs :guard (n64p addr)
                  :stobjs (y86-64)))
  (memi (n24 addr) y86-64))

(defthm natp-rm08
  ;; Byte read is a natp.
  (implies (y86-64p y86-64)
           (natp (rm08 addr y86-64)))
  :rule-classes :type-prescription)

(defthm bound-rm08
  ;; Byte read is in range [0...255].
  (implies (y86-64p y86-64)
           (and (<= 0 (rm08 addr y86-64))
                (< (rm08 addr y86-64) 256))))

(in-theory (disable rm08))

(defun wm08 (addr v y86-64)
  ;; Truncate address to 24 bits and write an 8-bit nat
  ;; to state's memory. 
  (declare (xargs :guard (and (n64p addr)
                              (n08p v))
                  :stobjs (y86-64)))
  ;; >> Not sure why it's done this way.
  (prog2$
   nil ; (cw "Address: ~p0, Byte: ~p1~%" addr v)
   (!memi (n24 addr) v y86-64)))

(defthm y86-64p-wr08
  ;; Writing an 8-bit nat to memory preserves legal state.
  (implies (and (n08p v)
                (y86-64p y86-64))
           (y86-64p (wm08 addr v y86-64))))

(in-theory (disable wm08))

(defun rm64 (addr y86-64)
  ;; Truncate address to 24 bits and access 8 consecutive bytes 
  ;; from state's memory. Assemble into 64-bit quantity in little
  ;; endian fashion.  Note that this allows wrap around, but that
  ;; will be precluded in the asm by constraints there.
  (declare (xargs :guard (n64p addr)
                  :stobjs (y86-64)))
  (let* ((byte0 (memi (n24    addr) y86-64))
         (byte1 (memi (n24+ 1 addr) y86-64))
         (byte2 (memi (n24+ 2 addr) y86-64))
         (byte3 (memi (n24+ 3 addr) y86-64))
         (byte4 (memi (n24+ 4 addr) y86-64))
         (byte5 (memi (n24+ 5 addr) y86-64))
         (byte6 (memi (n24+ 6 addr) y86-64))
         (byte7 (memi (n24+ 7 addr) y86-64)))

    (n64 (logior byte0
                 (ash byte1 8)
                 (ash byte2 16)
                 (ash byte3 24)
                 (ash byte4 32)
                 (ash byte5 40)
                 (ash byte6 48)
                 (ash byte7 56)))))

(defthm natp-rm64
  ;; Reading a 64-bit quantity from memory yields
  ;; a 64-bit natp.
  (implies (y86-64p y86-64)
           (n64p (rm64 addr y86-64)))
  :rule-classes :type-prescription)

(defthm bound-rm64
  ;; Reading a 64-bit quantity from memory yields
  ;; a value in range [0...2^64-1].
  (implies (y86-64p y86-64)
           (and (<= 0 (rm64 addr y86-64))
                (< (rm64 addr y86-64) 18446744073709551616))))

(in-theory (disable rm64))

(defun wm64 (addr v y86-64)
  ;; Given a 64-bit address, 64-bit value, and legal state, truncate
  ;; address to 24 bits. Extract the bytes from the value, write them
  ;; into memory at the next 8 addresses in little endian fashion. 
  ;; Note that this allows addresses to wrap, but that's precluded
  ;; in the asm by constraints there. 
  (declare (xargs :guard (and (n64p addr)
                              (n64p v))
                  :stobjs (y86-64)))
  (let* ((addr0 (n24    addr))
         (addr1 (n24+ 1 addr))
         (addr2 (n24+ 2 addr))
         (addr3 (n24+ 3 addr))
         (addr4 (n24+ 4 addr))
         (addr5 (n24+ 5 addr))
         (addr6 (n24+ 6 addr))
         (addr7 (n24+ 7 addr))
	 ;
         (byte0 (n08      v     ))
         (byte1 (n08 (ash v  -8)))
         (byte2 (n08 (ash v -16)))
         (byte3 (n08 (ash v -24)))
         (byte4 (n08 (ash v -32)))
         (byte5 (n08 (ash v -40)))
         (byte6 (n08 (ash v -48)))
         (byte7 (n08 (ash v -56)))
	 ;
         (y86-64 (!memi addr0 byte0 y86-64))
         (y86-64 (!memi addr1 byte1 y86-64))
         (y86-64 (!memi addr2 byte2 y86-64))
         (y86-64 (!memi addr3 byte3 y86-64))
         (y86-64 (!memi addr4 byte4 y86-64))
         (y86-64 (!memi addr5 byte5 y86-64))
         (y86-64 (!memi addr6 byte6 y86-64))
         (y86-64 (!memi addr7 byte7 y86-64)))
    y86-64))

(defthm y86-64p-wm64
  ;; Writing a 64-bit nat to memory in the state, preserves
  ;; a legal state.
  (implies (and (n64p v)
                (y86-64p y86-64))
           (y86-64p (wm64 addr v y86-64))))

(in-theory (disable wm64))


;; Adding 128-bit operations for capabilities

(defun rm128 (addr y86-64)
  ;; Truncate address to 24 bits and access 16 consecutive bytes 
  ;; from state's memory. Assemble into 128-bit quantity in little
  ;; endian fashion.  Note that this allows wrap around, but that
  ;; will be precluded in the asm by constraints there.
  (declare (xargs :guard (n64p addr)
                  :stobjs (y86-64)))
  (let* ((byte0 (memi (n24    addr) y86-64))
         (byte1 (memi (n24+ 1 addr) y86-64))
         (byte2 (memi (n24+ 2 addr) y86-64))
         (byte3 (memi (n24+ 3 addr) y86-64))
         (byte4 (memi (n24+ 4 addr) y86-64))
         (byte5 (memi (n24+ 5 addr) y86-64))
         (byte6 (memi (n24+ 6 addr) y86-64))
         (byte7 (memi (n24+ 7 addr) y86-64))
         (byte8 (memi (n24+ 8 addr) y86-64))
         (byte9 (memi (n24+ 9 addr) y86-64))
         (byte10 (memi (n24+ 10 addr) y86-64))
         (byte11 (memi (n24+ 11 addr) y86-64))
         (byte12 (memi (n24+ 12 addr) y86-64))
         (byte13 (memi (n24+ 13 addr) y86-64))
         (byte14 (memi (n24+ 14 addr) y86-64))
         (byte15 (memi (n24+ 15 addr) y86-64)))

    (n128 (logior byte0
                  (ash byte1 8)
                  (ash byte2 16)
                  (ash byte3 24)
                  (ash byte4 32)
                  (ash byte5 40)
                  (ash byte6 48)
                  (ash byte7 56)
                  (ash byte8 64)
                  (ash byte9 72)
                  (ash byte10 80)
                  (ash byte11 88)
                  (ash byte12 96)
                  (ash byte13 104)
                  (ash byte14 112)
                  (ash byte15 120)))))

(defthm natp-rm128
  ;; Reading a 128-bit quantity from memory yields
  ;; a 128-bit natp.
  (implies (y86-64p y86-64)
           (n128p (rm128 addr y86-64)))
  :rule-classes :type-prescription)

(defthm bound-rm128
  ;; Reading a 128-bit quantity from memory yields
  ;; a value in range [0...2^128-1].
  (implies (y86-64p y86-64)
           (and (<= 0 (rm128 addr y86-64))
                (< (rm128 addr y86-64) (expt 2 128)))))

(in-theory (disable rm64))
(defun wm128 (addr v y86-64)
  ;; Given a 64-bit address, 64-bit value, and legal state, truncate
  ;; address to 24 bits. Extract the bytes from the value, write them
  ;; into memory at the next 16 addresses in little endian fashion. 
  ;; Note that this allows addresses to wrap, but that's precluded
  ;; in the asm by constraints there. 
  (declare (xargs :guard (and (n64p addr)
                              (n128p v))
                  :stobjs (y86-64)))
  (let* ((addr0 (n24    addr))
         (addr1 (n24+ 1 addr))
         (addr2 (n24+ 2 addr))
         (addr3 (n24+ 3 addr))
         (addr4 (n24+ 4 addr))
         (addr5 (n24+ 5 addr))
         (addr6 (n24+ 6 addr))
         (addr7 (n24+ 7 addr))
         (addr8 (n24+ 8 addr))
         (addr9 (n24+ 9 addr))
         (addr10 (n24+ 10 addr))
         (addr11 (n24+ 11 addr))
         (addr12 (n24+ 12 addr))
         (addr13 (n24+ 13 addr))
         (addr14 (n24+ 14 addr))
         (addr15 (n24+ 15 addr))
	 ;
         (byte0 (n08      v     ))
         (byte1 (n08 (ash v  -8)))
         (byte2 (n08 (ash v -16)))
         (byte3 (n08 (ash v -24)))
         (byte4 (n08 (ash v -32)))
         (byte5 (n08 (ash v -40)))
         (byte6 (n08 (ash v -48)))
         (byte7 (n08 (ash v -56)))
         (byte8 (n08 (ash v -64)))
         (byte9 (n08 (ash v -72)))
         (byte10 (n08 (ash v -80)))
         (byte11 (n08 (ash v -88)))
         (byte12 (n08 (ash v -96)))
         (byte13 (n08 (ash v -104)))
         (byte14 (n08 (ash v -112)))
         (byte15 (n08 (ash v -120)))
	 ;
         (y86-64 (!memi addr0 byte0 y86-64))
         (y86-64 (!memi addr1 byte1 y86-64))
         (y86-64 (!memi addr2 byte2 y86-64))
         (y86-64 (!memi addr3 byte3 y86-64))
         (y86-64 (!memi addr4 byte4 y86-64))
         (y86-64 (!memi addr5 byte5 y86-64))
         (y86-64 (!memi addr6 byte6 y86-64))
         (y86-64 (!memi addr7 byte7 y86-64))
         (y86-64 (!memi addr8 byte8 y86-64))
         (y86-64 (!memi addr9 byte9 y86-64))
         (y86-64 (!memi addr10 byte10 y86-64))
         (y86-64 (!memi addr11 byte11 y86-64))
         (y86-64 (!memi addr12 byte12 y86-64))
         (y86-64 (!memi addr13 byte13 y86-64))
         (y86-64 (!memi addr14 byte14 y86-64))
         (y86-64 (!memi addr15 byte15 y86-64)))
    y86-64))

(defthm y86-64p-wm128
  ;; Writing a 128-bit nat to memory in the state, preserves
  ;; a legal state.
  (implies (and (n128p v)
                (y86-64p y86-64))
           (y86-64p (wm128 addr v y86-64))))

(in-theory (disable wm128))


; Update lemmas
;; >>> Many of these could probably be combined.

(defthm rgfi-!rgfi
  ;; Accessing a register after writing it returns the 
  ;; value written.
  (equal (rgfi i (!rgfi i v y86-64))
         v)
  :hints (("Goal" :in-theory (enable rgfi !rgfi))))

(defthm rgfi-read-through-different-address-!rgfi
  ;; Writing register i doesn't change the value of register
  ;; j, if i != j.
  (implies (and (n04p i)
                (n04p j)
                (not (equal i j)))
           (equal (rgfi i (!rgfi j v y86-64))
                  (rgfi i y86-64)))
  :hints (("Goal" :in-theory (enable rgfi !rgfi))))

(defthm rip-!rip
  ;; Accessing RIP after writing it returns the value
  ;; written.
  (equal (rip (!rip v y86-64))
         v)
  :hints (("Goal" :in-theory (enable rip !rip))))

(defthm memi-!memi
  ;; Accessing a memory location after writing it returns the value
  ;; written.
  (equal (memi i (!memi i v y86-64))
         v)
  :hints (("Goal" :in-theory (enable memi !memi))))

;; I moved these from read-over-write.lisp because they were the 
;;     only lemmas there that weren't redundant. 

(defthm memi-read-through-different-address-!memi
  ;; Read over write to a different memory address.
  (implies (and (n64p i)
                (n64p j)
                (not (equal i j)))
           (equal (memi i (!memi j v y86-64))
                  (memi i y86-64)))
  :hints (("Goal" :in-theory (enable memi !memi))))

;; I was surprised that for many of these read over write
;;     types of lemmas, I don't need to know that I'm operating
;;     on a y86-64p.

(defthm rm08-wm08
  ;; Read over write to a memory address in the state.
  (implies (and ;(y86-64p y86-64)
                (n32p i)
                (n08p v))
           (equal (rm08 i (wm08 i v y86-64))
                  v))
  :hints (("Goal" :in-theory (enable rm08 wm08))))

;; Miscellaneous other lemmas I found I needed about state:

(defthm ms-over-!ms
  (equal (ms (!ms v y86-64)) v)
  :hints (("Goal" :in-theory (enable ms !ms))))

(defthm ms-over-!rip
  (equal (ms (!rip v y86-64))
	 (ms y86-64))
  :hints (("Goal" :in-theory (enable ms !rip))))

(defthm ms-over-!rgfi
  (equal (ms (!rgfi r v y86-64))
	 (ms y86-64))
  :hints (("Goal" :in-theory (enable ms !rgfi))))

(defthm rgfi-over-!rip
  (equal (rgfi r (!rip v y86-64))
	 (rgfi r y86-64))
  :hints (("Goal" :in-theory (enable rgfi !rip))))

(defthmd rm64-from-successive-bytes
  ;; This should be useful when we know the actual values
  ;; stored at 8 successive bytes. 
  ;; I'm keeping it disabled because I don't want it firing
  ;; on every instance of rm64.
  ;; >> could I add a syntaxp hyp to this to restrict it to
  ;;    fire only when I have constants.
  (implies (and ;(y86-64p y86-64)
		(n24p addr)
		(n24p (+ 1 addr))
		(n24p (+ 2 addr))
		(n24p (+ 3 addr))
		(n24p (+ 4 addr))
		(n24p (+ 5 addr))
		(n24p (+ 6 addr))
		(n24p (+ 7 addr))
		(equal (rm08 addr y86-64) n0)
		(equal (rm08 (+ 1 addr)  y86-64) n1)
		(equal (rm08 (+ 2 addr)  y86-64) n2)
		(equal (rm08 (+ 3 addr)  y86-64) n3)
		(equal (rm08 (+ 4 addr)  y86-64) n4)
		(equal (rm08 (+ 5 addr)  y86-64) n5)
		(equal (rm08 (+ 6 addr)  y86-64) n6)
		(equal (rm08 (+ 7 addr)  y86-64) n7))
	   (equal (rm64 addr y86-64)
		  (n64 (logior n0
			       (ash n1 8)
			       (ash n2 16)
			       (ash n3 24)
			       (ash n4 32)
			       (ash n5 40)
			       (ash n6 48)
			       (ash n7 56)))))
  :hints (("Goal" :in-theory (enable rm64 rm08))))

(defthm memi-over-!ms
  (equal (memi addr (!ms val y86-64))
	 (memi addr y86-64))
  :hints (("Goal" :in-theory (enable memi !ms))))

(defthm rm64-over-!ms
  (equal (rm64 addr (!ms val y86-64))
	 (rm64 addr y86-64))
  :hints (("Goal" :in-theory (enable rm64))))

(defthm memi-over-!rip
  ;; Access to memory is not disturbed by write to rip.
  (equal (memi addr (!rip v y86-64))
	 (memi addr y86-64))
  :hints (("Goal" :in-theory (enable memi !rip))))

(defthm rm64-over-!rip
  ;; Reading a qword from memory is not affected by 
  ;; changes to rip.
  (equal (rm64 addr (!rip v y86-64))
	 (rm64 addr y86-64))
  :hints (("Goal" :in-theory (enable rm64))))

(defthm memi-over-!rgfi
  ;; Access to memory is not disturbed by write to regs.
  (equal (memi addr (!rgfi reg v y86-64))
	 (memi addr y86-64))
  :hints (("Goal" :in-theory (enable memi !rgfi))))

(defthm rm64-over-!rgfi
  ;; Reading a qword from memory is not affected by 
  ;; changes to regs.
  (equal (rm64 addr (!rgfi reg v y86-64))
	 (rm64 addr y86-64))
  :hints (("Goal" :in-theory (enable rm64))))

(defthm !rgfi-!rip-commute
  (equal (!rgfi r v1 (!rip v2 y86-64))
	 (!rip v2 (!rgfi r v1 y86-64)))
  :hints (("Goal" :in-theory (enable y86-64p !rgfi !rip))))

(defthm !rip-over-!rip
  (equal (!rip v1 (!rip v2 y86-64))
	 (!rip v1 y86-64))
  :hints (("Goal" :in-theory (enable !rip y86-64p))))

(defthm !rfgi-over-!rfgi
  (implies (and (reg-nump r1)
		(reg-nump r2))
	   (and (implies (equal r1 r2)
			 (equal (!rgfi r1 v1 (!rgfi r2 v2 y86-64))
				(!rgfi r1 v1 y86-64)))
		(implies (< r2 r1)
			 (equal (!rgfi r1 v1 (!rgfi r2 v2 y86-64))
				(!rgfi r2 v2 (!rgfi r1 v1 y86-64))))))
  :hints (("Goal" :in-theory (enable y86-64p !rgfi))))

(defthm ms-over-!memi
  (equal (ms (!memi addr val y86-64))
	 (ms y86-64))
  :hints (("Goal" :in-theory (enable ms !memi))))

(defthm ms-over-wm64
  (equal (ms (wm64 addr val y86-64))
	 (ms y86-64))
  :hints (("Goal" :in-theory (enable wm64))))

(defthm !memi-commutes-with-!rip
  (equal (!memi x v
		(!rip y y86-64))
	 (!rip y (!memi x v y86-64)))
  :hints (("Goal" :in-theory (enable !rip !memi))))

(defthm mv64-commutes-with-!rip
  (equal (wm64 x v
	       (!rip y y86-64))
	 (!rip y (wm64 x v y86-64)))
  :hints (("Goal" :in-theory (enable wm64))))

(defthm !memi-commutes-with-!rgfi
  (equal (!memi x v
		(!rgfi r y y86-64))
	 (!rgfi r y (!memi x v y86-64)))
  :hints (("Goal" :in-theory (enable !rgfi !memi))))

(defthm mv64-commutes-with-!rgfi
  (equal (wm64 x v
	       (!rgfi r y y86-64))
	 (!rgfi r y (wm64 x v y86-64)))
  :hints (("Goal" :in-theory (enable wm64))))

(defthm logand-lessp-24
   (implies (n24p n)
	    (equal (logand n *2^24-1*)
		   n))
   :hints (("Goal" :use (:instance logand-lessp (k 24)))))

(defthm logand-lessp-64
   (implies (n64p n)
	    (equal (logand n *2^64-1*)
		   n))
   :hints (("Goal" :use (:instance logand-lessp (k 64)))))

(defthm rm08-over-!memi-different-address
  (implies (and ;(y86-64p y86-64)
		(n64p addr1)
		(n64p addr2)
		(not (equal (n24 addr1) (n24 addr2))))
	   (equal (rm08 addr1 (!memi addr2 v y86-64))
		  (rm08 addr1 y86-64)))
  :hints (("Goal" :in-theory (enable rm08 memi !memi))))

(defthm rm08-over-!rip
  (equal (rm08 addr (!rip v y86-64))
	 (rm08 addr y86-64))
  :hints (("Goal" :in-theory (enable rm08 !rip memi))))

(defthm rm08-over-!rgfi
  (equal (rm08 addr (!rgfi i v y86-64))
	 (rm08 addr y86-64))
  :hints (("Goal" :in-theory (enable rm08 !rgfi memi))))

(encapsulate
 ()
 (local (include-book "centaur/gl/gl" :dir :system))
 (local (def-gl-thm RM64-OVER-WM64-gl-thm
	  :hyp (and (natp v)
		    (< V (EXPT 2 64)))
	  :concl (EQUAL (LOGAND (LOGIOR (LOGAND V 255)
					(ASH (LOGAND (ASH V -8) 255) 8)
					(ASH (LOGAND (ASH V -16) 255) 16)
					(ASH (LOGAND (ASH V -24) 255) 24)
					(ASH (LOGAND (ASH V -32) 255) 32)
					(ASH (LOGAND (ASH V -40) 255) 40)
					(ASH (LOGAND (ASH V -48) 255) 48)
					(ASH (LOGAND (ASH V -56) 255) 56))
				*2^64-1*)  
			V)
	  :g-bindings `((v (:g-number ,(fromto 0 65))))))
 ;
  (defthm rm64-over-wm64-lemma
    (implies (n64p v)
	     (EQUAL (LOGAND (LOGIOR (LOGAND V 255)
					 (ASH (LOGAND (ASH V -8) 255) 8)
					 (ASH (LOGAND (ASH V -16) 255) 16)
					 (ASH (LOGAND (ASH V -24) 255) 24)
					 (ASH (LOGAND (ASH V -32) 255) 32)
					 (ASH (LOGAND (ASH V -40) 255) 40)
					 (ASH (LOGAND (ASH V -48) 255) 48)
					 (ASH (LOGAND (ASH V -56) 255) 56))
				 *2^64-1*)
			 V))
    :hints (("Goal" :use (:instance rm64-over-wm64-gl-thm)))))

(with-arithmetic-help-5
 (defthm logand-plus-crock1
   (implies (and (n64p i)
		 (posp x)
		 (posp y)
		 (< x 8)
		 (< y 8)
		 (not (equal x y)))
	    (not (equal (logand (+ x i) *2^24-1*)
			(logand (+ y i) *2^24-1*)))))
 ;
 (defthm logand-plus-crock2
   (implies (and (n64p i)
		 (posp x)
		 (< x 8))
	    (not (equal (logand (+ x i) *2^24-1*)
			(logand i *2^24-1*))))))

(defthm rm64-wm64
   ;; Read over write to a memory address in the state.
   (implies (and ;(y86-64p y86-64)
		 (n64p i)
		 (n64p v))
	    (equal (rm64 i (wm64 i v y86-64))
		   v))
   :hints (("Goal" :in-theory (enable rm64 wm64))))

(defthm rgfi-over-wm64
  (equal (rgfi i (wm64 addr val y86-64))
	 (rgfi i y86-64))
  :hints (("Goal" :in-theory (enable rgfi wm64 !memi))))

(defthm flg-accessors-over-!rip
  (and (equal (zf (!rip addr y86-64))
	      (zf y86-64))
       (equal (sf (!rip addr y86-64))
	      (sf y86-64))
       (equal (of (!rip addr y86-64))
	      (of y86-64)))
  :hints (("Goal" :in-theory (enable !rip zf sf of))))

(with-arithmetic-help-5
 (defthm logand-n64p-with-self1
   (implies (n64p n)
	    (equal (logand n n)
		   n))))

(defthm rgfp-true-listp
  (implies (rgfp regs)
	   (true-listp regs))
  :hints (("Goal" :in-theory (enable rgfp))))

(encapsulate
 ()
 (local (defthm update-nth-nth
	  (implies (and (true-listp l)
			(natp i)
			(< i (len l)))
		   (equal (update-nth i (nth i l) l)
			  l))
	  :hints (("Goal" :in-theory (enable nth)
		   :induct (nth i alst)))))
 ;
 (defthm !rgfi-over-rgfi-same-reg
   ;; >> I do need the first hyp for this one, but perhaps
   ;;    a weaker hyp would do.  Try true-listp.
   (implies (and (y86-64p y86-64)
		 (reg-nump i))
	    (equal (!rgfi i (rgfi i y86-64) y86-64)
		   y86-64))
   :hints (("Goal" :in-theory (enable !rgfi rgfi y86-64p)))))

(defthmd rm64-from-successive-bytes-corollary
  (IMPLIES (AND (n24p addr)
		(< (+ 7 addr) *2^24-8*))
	   (EQUAL (RM64 addr Y86-64)
		  (N64 (LOGIOR (rm08 addr y86-64)
			       (ASH (rm08 (+ 1 addr) y86-64) 8)
			       (ASH (rm08 (+ 2 addr) y86-64) 16)
			       (ASH (rm08 (+ 3 addr) y86-64) 24)
			       (ASH (rm08 (+ 4 addr) y86-64) 32)
			       (ASH (rm08 (+ 5 addr) y86-64) 40)
			       (ASH (rm08 (+ 6 addr) y86-64) 48)
			       (ASH (rm08 (+ 7 addr) y86-64) 56)))))
  :hints (("Goal" :use (:instance rm64-from-successive-bytes
				  (n0 (rm08 addr y86-64))
				  (n1 (rm08 (+ 1 addr) y86-64))
				  (n2 (rm08 (+ 2 addr) y86-64))
				  (n3 (rm08 (+ 3 addr) y86-64))
				  (n4 (rm08 (+ 4 addr) y86-64))
				  (n5 (rm08 (+ 5 addr) y86-64))
				  (n6 (rm08 (+ 6 addr) y86-64))
				  (n7 (rm08 (+ 7 addr) y86-64))))))

(defthm rm08-over-wm64-disjoint-addresses
  (implies (and (n24p addr)
		(n24p addr2)
		(< addr2 *2^24-8*)
		(or (< addr (n24 addr2))
		    (< (n24 (+ 7 addr2)) addr)))
	   (equal (rm08 addr (wm64 addr2 val y86-64))
		  (rm08 addr y86-64)))
  :hints (("Goal" :in-theory (enable wm64))))

(defthm rm08-over-!ms
  (equal (rm08 addr (!ms val y86-64))
	 (rm08 addr y86-64))
  :hints (("Goal" :in-theory (enable rm08 !ms memi))))

(defthm rm08-over-wm64-same-address
  (implies (and (n24p addr)
		(< addr *2^24-8*))
	   (equal (rm08 addr (wm64 addr val y86-64))
		  (logand val 255)))
  :hints (("Goal" :in-theory (enable rm08 wm64))))

(defthm rm08-over-wm64-in-qword
  (implies (and (n24p addr)
		(< addr *2^24-8*))
	   (and (equal (rm08 addr (wm64 addr val y86-64))
		       (logand val 255))
		(equal (rm08 (+ 1 addr) (wm64 addr val y86-64))
		       (logand (ash val -8) 255))
		(equal (rm08 (+ 2 addr) (wm64 addr val y86-64))
		       (logand (ash val -16) 255))
		(equal (rm08 (+ 3 addr) (wm64 addr val y86-64))
		       (logand (ash val -24) 255))
		(equal (rm08 (+ 4 addr) (wm64 addr val y86-64))
		       (logand (ash val -32) 255))
		(equal (rm08 (+ 5 addr) (wm64 addr val y86-64))
		       (logand (ash val -40) 255))
		(equal (rm08 (+ 6 addr) (wm64 addr val y86-64))
		       (logand (ash val -48) 255))
		(equal (rm08 (+ 7 addr) (wm64 addr val y86-64))
		       (logand (ash val -56) 255))
		))
  :hints (("Goal" :in-theory (enable rm08 wm64))))

#||
(defthm rm08-!memi
  (implies (and (y86-64p y86-64)
                (n64p i)
                (n64p j)
                (< i j)
                (equal (logand j 3) 0))
           (equal (rm08 i 
                  (rm08 i y86-64)))
  :hints (("Goal" :in-theory (enable rm08)))))



(defthm rgfi-read-through-different-address-!rgfi
  (implies (and (n04p i)
                (n04p j)
                (not (equal i j)))
           (equal (rgfi i (!rgfi j v y86-64))
                  (rgfi i y86-64)))
  :hints (("Goal" :in-theory (enable rgfi !rgfi))))
||#

