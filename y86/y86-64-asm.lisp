;  y86-64-asm.lisp                          

; (ld "y86-64-asm.lisp" :ld-pre-eval-print t)
; (certify-book "y86-64-asm" ?)

(in-package "ACL2")

; !!!  During GUARD proof, CW commands repeatedly executed and that
; !!!  output is printed to the user.

(include-book "std/util/bstar" :dir :system)

(include-book "misc-events")
(include-book "operations")
(include-book "constants")

; This is an assembler for the Y86 simulator, that includes the IADDQ
; and the LEAVE instructions.

; Input to our Y86 assembler is given as a proper list containing
; instructions, symbols (labels), and assembler directives.

; This is a two-pass assembler.  The first pass reads the entire input
; and returns a (fast) association list with symbols (labels) paired
; with their memory address.  The second pass creates an association
; list pairing memory byte-addresses with their contents; the actual
; Y86 memory (STOBJ) image is initialized using this association list.

; The assembler maintains a memory address <asm-addr> where the next
; assembler directive or instruction will take effect, and then the
; <asm-addr> is altered as necessary.  For example, if a NOP
; instruction is found, the byte #x10 will be inserted into an
; evolving memory image at address <asm-addr> and then, <asm-addr>
; will be increased by 1 (byte).  Blank space is one or more spaces
; newlines and/or tabs, and assembler directives can be spaced and
; formatted as desired.

; The assembler includes directives that associate a label with the
; current value of <asm-addr>, that change <asm-addr>, and that create
; specific data to be placed in the memory.

;   <label>       ; Name for specific <asm-addr>

;; >> Is it OK for pos to decrease the count?

;   (pos   <n>)   ; Set <asm-addr> to <n>
;   (align <n>)   ; <asm-addr> set to (+ <asm-addr>
;                                      (MOD <asm-addr> (expt 2 <n>)))
;   (space <n>)   ; <asm-addr> set to (+ <asm-addr> <n>)

; Write sequence of given bytes (qwords, characters) starting at
; address <asm-addr>, and advance <asm-addr> by the number of bytes
; required for the data.  Strings are converted into a list of
; characters, and then written as characters.

;   (byte  <a> <b> <c> ...)  ; Write byte(s)        <a> <b> <c> ...
;   (qword <a> <b> <c> ...)  ; Write 64-bit word(s) <a> <b> <c> ...
;   (char  #\a #\b #\c ...)  ; Write character(s)   #\a #\b #\c ...
;   (string    "abc...def")  ; Write string as sequence of characters

;; >> Do I need to add LEAVE to this?

; Y86 (machine) control, NOP instructions

;   (halt)
;   (nop)

; Y86 move instruction

; (rrmovq <reg-id-or-n04p> <reg-id-or-n04p>)

; Y86 conditional move instructions

; (cmovle <reg-id-or-n04p> <reg-id-or-n04p>)
; (cmovl  <reg-id-or-n04p> <reg-id-or-n04p>)
; (cmove  <reg-id-or-n04p> <reg-id-or-n04p>)
; (cmovne <reg-id-or-n04p> <reg-id-or-n04p>)
; (cmovge <reg-id-or-n04p> <reg-id-or-n04p>)
; (cmovg  <reg-id-or-n04p> <reg-id-or-n04p>)

; Y86 register move instructions

; (irmovq <label-or-n64p> <reg-id-or-n04p>)
; (rmmovq <reg-id-or-n04p> <label-or-n64p> (<reg-id-or-n04p>))
; (mrmovq <label-or-n64p> (<reg-id-or-n04p>) <reg-id-or-n04p>)

; Y86 logical/arithmetic instructions

; (addq  <reg-id-or-n04p> <reg-id-or-n04p>)
; (subq  <reg-id-or-n04p> <reg-id-or-n04p>)
; (andq  <reg-id-or-n04p> <reg-id-or-n04p>)
; (xorq  <reg-id-or-n04p> <reg-id-or-n04p>)
; (iaddq <label-or-n64p>  <reg-id-or-n04p>)

; Y86 flow of control instructions

; (jmp <label-or-n64p>)
; (jle <label-or-n64p>)
; (jl  <label-or-n64p>)
; (je  <label-or-n64p>)
; (jne <label-or-n64p>)
; (jge <label-or-n64p>)
; (jg  <label-or-n64p>)

; (call <label-or-n64p>)
; (ret)
; (leave)

; Y86 stack push and pop instructions

; (pushq <reg-id-or-n04p>)
; (popq  <reg-id-or-n04p>)

(defun symbol-or-n64-listp (lst)
  (declare (xargs :guard t))
  (if (atom lst)
      (null lst)
    (and (or (symbolp (car lst))
             (n64p (car lst)))
         (symbol-or-n64-listp (cdr lst)))))

; Evolving memory image produced by the assembler is kept in a "fast"
; association list.  Below we define functions to read and write the
; evolving, byte-addressed memory image.

(defun n64p-n08p-alistp (alst)
  ;; Recognizes an alist of form: ( (addr . value) ... ). 
  (declare (xargs :guard t))
  (if (atom alst)
      t
    (if (atom (car alst))
        nil
      (let ((symbol (caar alst))
            (val    (cdar alst))
            (rest   (cdr  alst)))
        (and (n64p symbol)
             (n08p val)
             (n64p-n08p-alistp rest))))))

;;----------------------------------------------------------------------
;;----------------------------------------------------------------------

;; MEMORY ACCESS FUNCTIONS

; Assembler memory access functions, where the program and data being
; created are placed.

(defun r08 (address memory)
  ;; Read a byte from memory. 
  (declare (xargs :guard (and (n64p address)
                              (n64p-n08p-alistp memory))))
  (let ((addr-byte (hons-get address memory)))
    (if (atom addr-byte)
        (or (cw "r08: no value found at address ~p0.~%" address) 0)
      (cdr addr-byte))))

(defthm natp-r08
  (implies (n64p-n08p-alistp memory)
           (natp (r08 addr-byte memory)))
  :rule-classes :type-prescription)

(defthm bound-r08
  (implies (n64p-n08p-alistp memory)
           (and (<= 0 (r08 address memory))
                (< (r08 address memory) 256)))
  :rule-classes :linear)

(in-theory (disable r08))

(defun r16 (address memory)
  ;; Read a word from memory. 
  (declare (xargs :guard (and (n64p address)
                              (n64p-n08p-alistp memory))))
  (let ((byte0 (r08 address memory))
        (byte1 (r08 (n64+ address 1) memory)))
    (n16+ (ash byte1 8) byte0)))

(defthm natp-r16
  (implies (n64p-n08p-alistp memory)
           (natp (r16 addr-byte memory)))
  :rule-classes :type-prescription)

(defthm bound-r16
  (implies (n64p-n08p-alistp memory)
           (and (<= 0 (r16 address memory))
                (< (r16 address memory) 65536)))
  :rule-classes :linear)

(in-theory (disable r16))

(defun r32 (address memory)
  ;; Read a double word from memory.
  (declare (xargs :guard (and (n64p address)
                              (n64p-n08p-alistp memory))))
  (let ((byte0 (r08 address memory))
        (byte1 (r08 (n64+ address 1) memory))
        (byte2 (r08 (n64+ address 2) memory))
        (byte3 (r08 (n64+ address 3) memory)))
    (n32+ (ash byte3 24)
          (+ (ash byte2 16)
             (+ (ash byte1 8) byte0)))))

(defthm natp-r32
  (implies (n64p-n08p-alistp memory)
           (natp (r32 addr-byte memory)))
  :rule-classes :type-prescription)

(defthm bound-r32
  (implies (n64p-n08p-alistp memory)
           (and (<= 0 (r32 address memory))
                (< (r32 address memory) 4294967296)))
  :rule-classes :linear)

(in-theory (disable r32))

(defun r64 (address memory)
  ;; Read a quadword from memory.
  (declare (xargs :guard (and (n64p address)
                              (n64p-n08p-alistp memory))))
  (let ((byte0 (r08 address memory))
        (byte1 (r08 (n64+ address 1) memory))
        (byte2 (r08 (n64+ address 2) memory))
        (byte3 (r08 (n64+ address 3) memory))
        (byte4 (r08 (n64+ address 4) memory))
        (byte5 (r08 (n64+ address 5) memory))
        (byte6 (r08 (n64+ address 6) memory))
        (byte7 (r08 (n64+ address 7) memory)))
    (n64+ (ash byte7 56)
          (+ (ash byte6 48)
             (+ (ash byte5 40)
                (+ (ash byte4 32)
                   (+ (ash byte3 24)
                      (+ (ash byte2 16)
                         (+ (ash byte1 8)
                            byte0)))))))))

(defthm natp-r64
  (implies (n64p-n08p-alistp memory)
           (natp (r64 addr-byte memory)))
  :rule-classes :type-prescription)

(defthm bound-r64
  (implies (n64p-n08p-alistp memory)
           (and (<= 0 (r08 address memory))
                (< (r08 address memory) 18446744073709551616)))
  :rule-classes :linear)

(in-theory (disable r64))

(defun w08 (address value memory)
  ;; Write a byte to memory. 
  (declare (xargs :guard (and (n64p address)
                              (n08p value)
                              (n64p-n08p-alistp memory))))
  (hons-acons address value memory))

(defthm n64p-n08p-alistp-w08
  (implies (and (n64p address)
                (n08p value)
                (n64p-n08p-alistp memory))
           (n64p-n08p-alistp (w08 address value memory))))

(in-theory (disable w08))

(defun w16 (address value memory)
  ;; Write a word to memory.
  (declare (xargs :guard (and (n64p address)
                              (n16p value)
                              (n64p-n08p-alistp memory))))
  (let ((byte0 (n08 value))
        (byte1 (n08 (ash value -8))))
    (let* ((memory (hons-acons       address    byte0 memory))
           (memory (hons-acons (n64+ address 1) byte1 memory)))
      memory)))

(defthm n64p-n08p-alistp-w16
  (implies (and (n64p address)
                (n16p value)
                (n64p-n08p-alistp memory))
           (n64p-n08p-alistp (w16 address value memory))))

(in-theory (disable w16))

(defun w32 (address value memory)
  ;; Write a double-word to memory.
  (declare (xargs :guard (and (n64p address)
                              (n32p value)
                              (n64p-n08p-alistp memory))))
  (let ((byte0 (n08 value))
        (byte1 (n08 (ash value -8)))
        (byte2 (n08 (ash value -16)))
        (byte3 (n08 (ash value -24))))
    (let* ((memory (hons-acons       address    byte0 memory))
           (memory (hons-acons (n64+ address 1) byte1 memory))
           (memory (hons-acons (n64+ address 2) byte2 memory))
           (memory (hons-acons (n64+ address 3) byte3 memory)))
      memory)))

(defthm n64p-n08p-alistp-w32
  (implies (and (n64p address)
                (n32p value)
                (n64p-n08p-alistp memory))
           (n64p-n08p-alistp (w32 address value memory))))

(in-theory (disable w32))

(defun w64 (address value memory)
  ;; Write a quadword to memory.
  (declare (xargs :guard (and (n64p address)
                              (n64p value)
                              (n64p-n08p-alistp memory))))
  (let ((byte0 (n08      value     ))
        (byte1 (n08 (ash value  -8)))
        (byte2 (n08 (ash value -16)))
        (byte3 (n08 (ash value -24)))
        (byte4 (n08 (ash value -32)))
        (byte5 (n08 (ash value -40)))
        (byte6 (n08 (ash value -48)))
        (byte7 (n08 (ash value -56))))
    (let* ((memory (hons-acons       address    byte0 memory))
           (memory (hons-acons (n64+ address 1) byte1 memory))
           (memory (hons-acons (n64+ address 2) byte2 memory))
           (memory (hons-acons (n64+ address 3) byte3 memory))
           (memory (hons-acons (n64+ address 4) byte4 memory))
           (memory (hons-acons (n64+ address 5) byte5 memory))
           (memory (hons-acons (n64+ address 6) byte6 memory))
           (memory (hons-acons (n64+ address 7) byte7 memory)))
      memory)))

(defthm n64p-n08p-alistp-w64
  (implies (and (n64p address)
                (n64p value)
                (n64p-n08p-alistp memory))
           (n64p-n08p-alistp (w64 address value memory))))

(in-theory (disable w64))

;;----------------------------------------------------------------------
;;----------------------------------------------------------------------
;; UTILITIES FOR ASSEMBLING:

; Convert register name to register number; otherwise, return NIL.

(defun reg-id (reg)
  ;; Convert register name to number, as stored
  ;; in *Y86-64-REG-NUMBERS*.  Return nil if not
  ;; symbol or not there.
  (declare (xargs :guard t))
  (and (symbolp reg) (reg-to-num reg)))

(defthm natp-reg-id
  (or (null (reg-id x))
      (natp (reg-id x)))
  :rule-classes :type-prescription)

(defthm reg-id-<-16
  (implies (reg-id x)
           (and (<= 0 (reg-id x))
                (< (reg-id x) 16)))
  :rule-classes :linear)

(in-theory (disable reg-id))

; Symbol table recognizer

(defun symbol-n64p-alistp (alst)
  ;; Recognize an alist of the form:
  ;; ( (symbol . n64) ... )
  (declare (xargs :guard t))
  (if (atom alst)
      t
    (if (atom (car alst))
        nil
      (let ((symbol (caar alst))
            (val    (cdar alst))
            (rest   (cdr  alst)))
        (and (symbolp symbol)
             (n64p val)
             (symbol-n64p-alistp rest))))))

(defun add-label-address-pair (sym addr symbol-table-alist)
  ;; Add (symbol . n64) pair to front of alist.
  (declare (xargs :guard (and (symbolp sym)
                              (n64p addr)
                              (symbol-n64p-alistp symbol-table-alist))))
  (hons-acons sym addr symbol-table-alist))

(defthm symbol-alist-add-label-address-pair
  (implies (and (symbolp sym)
                (n64p addr)
                (symbol-n64p-alistp symbol-table-alist))
           (symbol-n64p-alistp
            (add-label-address-pair sym addr symbol-table-alist))))

(in-theory (disable add-label-address-pair))

; Align address to mod-2^n boundary

(defun align-to-mod-n (count mod-amount)
  ;; Align address to mod-2^n boundary
  (declare (xargs :guard (and (n64p count)
                              (n64p mod-amount)
                              (not (eql mod-amount 0)))
                  :guard-hints (("Goal" :in-theory (e/d () (mod))))))
  (let* ((over-by (n64 (mod count mod-amount))))
    (if (= over-by 0)
        count
      (n64+ (n64 (- mod-amount over-by)) count))))

(defthm natp-align-to-mod-n
  (implies (n64p count)
           (natp (align-to-mod-n count mod-amount)))
  :rule-classes :type-prescription)

(defthm align-to-mod-n-<-18446744073709551616
  (implies (n64p count)
           (and (<= 0 (align-to-mod-n count mod-amount))
                (< (align-to-mod-n count mod-amount) 18446744073709551616)))
  :rule-classes :linear)

(in-theory (disable align-to-mod-n))

; Database of information for assembler.

;; >> This never seems to be used.  Also, the arities of byte
;;    qword, char seem n-ary in fact. 

; (defconst *Arity*
;   '((pos      1)
;     (align    1)
;     (space    1)
;     (byte     1)
;     (qword    1)
;     (char     1)
;     (string   1)
;     (nop      0)
;     (halt     0)
;     (rrmovq   2)
;     (cmovle   2)
;     (cmovl    2)
;     (cmove    2)
;     (cmovne   2)
;     (cmovge   2)
;     (cmovg    2)
;     (irmovq   2)
;     (rmmovq   3)
;     (mrmovq   3)
;     (addq     2)
;     (subq     2)
;     (andq     2)
;     (xorq     2)
;     (jmp      1)
;     (jle      1)
;     (jl       1)
;     (je       1)
;     (jne      1)
;     (jge      1)
;     (jg       1)
;     (call     1)
;     (ret      0)
;     (pushq    1)
;     (popq     1)
;     (iaddq    2)
;     (leave    0)
;     (noop     0)
;     ))

; Instruction values.

(defconst *Instructions*
  '((halt     0)
    (nop      1)
    (rrmovq   2)
    (cmovle   2 1)
    (cmovl    2 2)
    (cmove    2 3)
    (cmovne   2 4)
    (cmovge   2 5)
    (cmovg    2 6)
    (irmovq   3)
    (rmmovq   4)
    (mrmovq   5)
    (addq     6 0)
    (subq     6 1)
    (andq     6 2)
    (xorq     6 3)
    (jmp      7 0)
    (jle      7 1)
    (jl       7 2)
    (je       7 3)
    (jne      7 4)
    (jge      7 5)
    (jg       7 6)
    (call     8)
    (ret      9)
    (pushq   10)
    (popq    11)
    (iaddq   12)
    (leave   13)
    (noop    15) ;; Second NOP instruction
    ))

; !!!  During GUARD proof, CW commands repeated executed and that
; !!!  output is printed to the user.

;;----------------------------------------------------------------------
;;----------------------------------------------------------------------
; RECOGNIZER FOR LEGAL Y86 PROGRAMS:

(defun n08-listp (x)
  (declare (xargs :guard t))
  (if (atom x)
      (null x)
    (and (n08p (car x))
         (n08-listp (cdr x)))))

;; >>> This recognizes addresses, especially in the assembler
;; directives, that are n64p.  Shouldn't those really be n24p?

;; >>> Should this check for duplicate labels?  What's the best way to
;; do that?  I could either have a predicate that checks that no atoms
;; in the list are duplicated.  Or I could add an argument to this 
;; of the labels found so far and make this a helper function for a 
;; call with nil as that argument. 

(defun y86-prog (program)
  ;; Recognizes a legal Y86 assembly language program, including
  ;; assembler directives.
  (declare (xargs :guard t))
  (if (atom program)
      (or (null program)
          (cw "Last CDR of program not NIL.~%"))
    (let ((label-or-instruction (car program))
          (rest-program (cdr program)))
      ;; >> Are duplicate labels OK?
      (if (atom label-or-instruction)
	  ;; It's a label.
          (and (symbolp label-or-instruction)
               (y86-prog rest-program))
	;; It's an instruction or assembler directive.
        (let ((instruction (car label-or-instruction))
              (args (cdr label-or-instruction)))
          (and (symbolp instruction)          ;; the operator
               (true-listp args)
               (case instruction
                 (pos
                  (and (or (and (consp args)  ;; single argument
                                (null (cdr args)))
                           (cw "Format:  (POS <memory-address>).~%"))
		       ;; >> Allows 64 bit address; should this be 24-bit?
                       (or (n64p (car args))
                           (cw "POS: ~p0 must be 0..2^64-1.~%" (car args)))))
                 (align
                  (and (or (and (consp args)
                                (null (cdr args)))
                           (cw "Format:  (ALIGN <size>).~%"))
                       (or (and (n64p (car args))
                                (< 1 (car args)))
			   ;; >> this previously had 2^65.
                           (cw "ALIGN: ~p0 must be 2..2^64-1.~%" (car args)))))
                 (byte
		  ;; byte directive is n-ary with a list of n08p's.
                  (or (n08-listp args)  
                      (cw "Format:  (BYTE <byte0> <byte1> ...).~%")
                      (cw "BYTE: ~p0 must be 0..255 ....~%" args)))
                 (qword
		  ;; qword directive is n-ary with a list of labels/n64p's.
                  (or (symbol-or-n64-listp args)
                      (cw "Format:  (QWORD <symbol-or-64-bit-word> ...).~%")
                      (cw "QWORD: ~p0 must be <symbol> or 0..2^64-1 ...~%" args)))
                 (char
		  ;; char directive is n-ary with a list of characters.
                  (or (character-listp args)
                      (cw "Format:  (CHAR <char0> <char1> ...).~%")
                      (cw "CHAR: ~p0 must be <char> ...~%" args)))
                 (string
		  ;; string directive is unary with a string argument. 
		  ;; It is stored in memory as a list of characters.
                  (and (or (and (consp args)
                                (null (cdr args)))
                           (cw "Format:  (STRING \"<string>\".~%"))
                       (or (stringp (car args))
                           (cw "STRING: ~p0 must be a string.~%" (car args)))))
                 (space
		  ;; space directive is unary
                  (and (or (and (consp args)
                                (null (cdr args)))
                           (cw "Format:  (SPACE <64-bit-word>).~%"))
                       (or (n64p (car args))
                           (cw "SPACE: ~p0 must be 0..2^64-1.~%" (car args)))))
                 (nop
                  (and (or (null args)
                           (cw "Format is: (NOP).~%"))))
                 (halt
                  (and (or (null args)
                           (cw "Format is: (HALT).~%"))))
                 (rrmovq
                  (and (or (and (consp args)
                                (consp (cdr args))
                                (null (cddr args)))
                           (cw "Format is:  (RRMOVQ <reg-a> <reg-b>).~%"))
                       (or (reg-id (car args))
                           (cw "RRMOVQ: ~p0 isn't a register ID.~%" (car args)))
                       (or (reg-id (cadr args))
                           (cw "RRMOVQ: ~p0 isn't a register ID.~%" (cadr args)))))
                 ((cmovle cmovl cmove cmovne cmovge cmovg)
                  (and (or (and (consp args)
                                (consp (cdr args))
                                (null (cddr args)))
                           (cw "Format is:  (CMOVL? <reg-a> <reg-b>).~%"))
                       (or (reg-id (car args))
                           (cw "CMOVX: ~p0 isn't a register ID.~%" (car args)))
                       (or (reg-id (cadr args))
                           (cw "CMOVX: ~p0 isn't a register ID.~%" (cadr args)))))
                 (irmovq
                  (and (or (and (consp args)
                                (consp (cdr args))
                                (null (cddr args)))
                           (cw "Format is:  (IRMOVQ <imm> <reg-b>).~%"))
                       (or (symbolp (car args))
                           (n64p (car args))
                           (cw "IRMOVQ: ~p0 must be a label or 0..2^64-1.~%"
                               (car args)))
                       (or (reg-id (cadr args))
                           (cw "IRMOVQ: <reg-b> isn't a register ID.~%"))))
                 (rmmovq
                  (and (or (and (consp args)
                                (consp (cdr args))
                                (consp (cddr args))
                                (null (cdddr args))
                                (consp (caddr args))
                                (null (cdr (caddr args))))
                           (cw "Format:  (RMMOVQ <reg-a> <displacement> (<reg-b>))"))
                       (or (reg-id (car args))
                           (cw "RMMOVQ: ~p0 isn't a register ID.~%" (car args)))
                       (or (symbolp (cadr args))
                           (n64p (cadr args))
                           (cw "RMMOVQ: ~p0 must be a label or 0..2^64-1.~%"
                               (cadr args)))
                       (or (reg-id (car (caddr args)))
                           (cw "RMMOVQ: ~p0 isn't a register ID.~%" (caddr args)))))
                 (mrmovq
                  (and (or (and (consp args)
                                (consp (cdr args))
                                (consp (cddr args))
                                (null (cdddr args))
                                (consp (cadr args))
                                (null (cdr (cadr args))))
                           (cw "Format:  (MRMOVQ <displacement> (<reg-a>) <reg-b>)"))
                       (or (symbolp (car args))
                           (n64p (car args))
                           (cw "MRMOVQ: ~p0 must be a label or 0..2^64-1.~%"
                               (cadr args)))
                       (or (reg-id (car (cadr args)))
                           (cw "MRMOVQ: ~p0 isn't a register ID.~%"
                               (car (cadr args))))
                       (or (reg-id (caddr args))
                           (cw "MRMOVQ: ~p0 isn't a register ID.~%" (caddr args)))))
                 ((addq subq andq xorq)
                  (and (or (and (consp args)
                                (consp (cdr args))
                                (null (cddr args)))
                           (cw "Format:  (<OP> <reg-a> <reg-b>).~%"))
                       (or (reg-id (car args))
                           (cw "<OP>: ~p0 isn't a register ID.~%" (car args)))
                       (or (reg-id (cadr args))
                           (cw "<OP>: ~p0 isn't a register ID.~%" (cadr args)))))
                 ((jmp jle jl je jne jge jg)
                  (and (or (and (consp args)
                                (null (cdr args)))
                           (cw "Format (<Jump-OP> <destination>).~%"))
                       (or (symbolp (car args))
                           (n64p    (car args))
                           (cw "<Jump-OP>: ~p0 must be a label or 0..2^64-1.~%"
                               (car args)))))
                 (call
                  (and (or (and (consp args)
                                (null (cdr args)))
                           (cw "Format (CALL <destination>).~%"))
                       (or (symbolp (car args))
                           (n64p    (car args))
                           (cw "CALL: ~p0 must be a label or 0..2^64-1.~%"
                               (car args)))))
                 (ret
                  (and (or (null args)
                           (cw "Format is: (RET).~%"))))
                 (pushq
                  (and (or (and (consp args)
                                (null (cdr args)))
                           (cw "Format (PUSHQ <reg-id>).~%"))
                       (or (reg-id (car args))
                           (cw "PUSHQ: ~p0 isn't a register ID.~%" (car args)))))
                 (popq
                  (and (or (and (consp args)
                                (null (cdr args)))
                           (cw "Format (POPQ <reg-id>).~%"))
                       (or (reg-id (car args))
                           (cw "POPQ: ~p0 isn't a register ID.~%" (car args)))))
                 (iaddq
                  (and (or (and (consp args)
                                (consp (cdr args))
                                (null (cddr args)))
                           (cw "Format is:  (IADDQ <imm> <reg-b>).~%"))
                       (or (symbolp (car args))
                           (n64p (car args))
                           (cw "IADDQ: ~p0 must be a label or 0..2^64-1.~%"
                               (car args)))
                       (or (reg-id (cadr args))
                           (cw "IADDQ: <reg-b> isn't a register ID.~%"))))
                 (leave
                  (and (or (null args)
                           (cw "Format is: (LEAVE).~%"))))
                 (noop
                  (and (or (null args)
                           (cw "Format is: (NOOP).~%"))))
                 (otherwise (cw "~p0 is an unrecognized directive or instruction.~%"
                                instruction)))
               (y86-prog rest-program)))))))

;;----------------------------------------------------------------------
;;----------------------------------------------------------------------
; CREATE SYMBOL TABLE:

;; Produce a symbol table for a Y86 program of form ( (symbol . addr) ... )
;;    program: the code from which to extract the symbol table
;;    count: the current memory location for this instruction/label
;;    symbol-table-alist: the growing symbol table

(defun y86-symbol-table (program count symbol-table-alist)
  (declare (xargs :guard (and (y86-prog program)
                              (n64p count)
                              (symbol-n64p-alistp symbol-table-alist))))
  (if (atom program)
      symbol-table-alist
    (let ((label-or-instruction (car program))
          (rest-program (cdr program)))
      (if (atom label-or-instruction)
	  ;; Program item is a label.  Add to symbol table with
	  ;; current location.
          (y86-symbol-table rest-program count
                            (add-label-address-pair label-or-instruction
                                                    count
                                                    symbol-table-alist))
	;; Otherwise, item is an instruction or assembler directive.
        (let ((instruction (car label-or-instruction))
              (args (cdr label-or-instruction)))
          (case instruction
	    ;; The Y86 assembler directives.
            (pos
	     ;; >>> Is it OK to move backwards?
             (y86-symbol-table rest-program
                               (car args)
                               symbol-table-alist))
            (align
             (y86-symbol-table rest-program
                               (align-to-mod-n count (car args))
                               symbol-table-alist))
            (byte
             (y86-symbol-table rest-program
                               (n64+ count (len args))
                               symbol-table-alist))
            (qword
             (y86-symbol-table rest-program
                               (n64+ count (* 8 (len args)))
                               symbol-table-alist))
            (char
             (y86-symbol-table rest-program
                               (n64+ count (len args))
                               symbol-table-alist))
            (string
             (y86-symbol-table rest-program
                               (n64+ count (length (car args)))
                               symbol-table-alist))
            (space
             (y86-symbol-table rest-program (n64+ count (car args))
                               symbol-table-alist))
	    ;; The Y86 instructions.
            (nop
             (y86-symbol-table rest-program (n64+ count  1) symbol-table-alist))
            (halt
             (y86-symbol-table rest-program (n64+ count  1) symbol-table-alist))
            (rrmovq
             (y86-symbol-table rest-program (n64+ count  2) symbol-table-alist))
            ((cmovle cmovl cmove cmovne cmovge cmovg)
             (y86-symbol-table rest-program (n64+ count  2) symbol-table-alist))
            (irmovq
             (y86-symbol-table rest-program (n64+ count 10) symbol-table-alist))
            (rmmovq
             (y86-symbol-table rest-program (n64+ count 10) symbol-table-alist))
            (mrmovq
             (y86-symbol-table rest-program (n64+ count 10) symbol-table-alist))
            ((addq subq andq xorq)
             (y86-symbol-table rest-program (n64+ count  2) symbol-table-alist))
            ((jmp jle jl je jne jge jg)
             (y86-symbol-table rest-program (n64+ count  9) symbol-table-alist))
            (call
             (y86-symbol-table rest-program (n64+ count  9) symbol-table-alist))
            (ret
             (y86-symbol-table rest-program (n64+ count  1) symbol-table-alist))
            (pushq
             (y86-symbol-table rest-program (n64+ count  2) symbol-table-alist))
            (popq
             (y86-symbol-table rest-program (n64+ count  2) symbol-table-alist))
            (iaddq
             (y86-symbol-table rest-program (n64+ count 10) symbol-table-alist))
            (leave
             (y86-symbol-table rest-program (n64+ count  1) symbol-table-alist))
            (noop
             (y86-symbol-table rest-program (n64+ count  1) symbol-table-alist))
            (otherwise symbol-table-alist)))))))


(defun label-address (label symbol-table-alist)
  ;; Look up the address corresponding to a label in the symbol table;
  ;; if label doesn't exist, return 0.
  (declare (xargs :guard (and (symbolp label)
                              (symbol-n64p-alistp symbol-table-alist))))
  (let* ((pair (hons-get label symbol-table-alist))
         (addr (if (consp pair)
                   (cdr pair)
                 (or (cw "Label ~p0 was not found in the symbol table.~%"
                         label)
                     0))))
    addr))

(defthm symbol-n64p-alistp-label-address
  (implies (symbol-n64p-alistp symbol-table-alist)
           (and (integerp (label-address label symbol-table-alist))
                (n64p (label-address label symbol-table-alist)))))

(in-theory (disable label-address))

;;----------------------------------------------------------------------
;;----------------------------------------------------------------------
;; ASSEMBLE INSTRUCTIONS AND ASSEMBLER DIRECTIVES.

;; Note that some assembler directive (pos, align) don't yield any
;; code, but only leave zero'd gaps in the memory image.  Others
;; (byte, qword, string) do insert non-zero data into the memory
;; image.

(defun n04-n04-to-n08 (x y)
  ;; Concatenate two nibbles to make a byte.
  (declare (xargs :guard (and (n04p x) (n04p y))))
  (n08 (logior y (ash x 4))))

(defthm natp-n04-n04-to-n08
  (natp (n04-n04-to-n08 x y))
  :rule-classes :type-prescription)

(defthm n04-n04-to-n08-<-256
  (and (<= 0 (n04-n04-to-n08 x y))
       (< (n04-n04-to-n08 x y) 256))
  :rule-classes :linear)

(in-theory (disable n04-n04-to-n08))

;;----------------------------------------------------------------------
; The BYTE assembler directive

(defun write-bytes (count byte-list program-bytes)
  ;; Directive:  (byte <list of bytes>)
  ;; Write elements of byte-list into memory program-bytes
  ;; beginning at address count.
  (declare (xargs :guard (and (n64p count)
                              (n08-listp byte-list)
                              (n64p-n08p-alistp program-bytes))
                  :verify-guards nil))
  (if (atom byte-list)
      program-bytes
    (w08 count
         (car byte-list)
         (write-bytes (n64+ count 1)
                      (cdr byte-list)
                      program-bytes))))

(defthm n64p-n08p-alistp-write-bytes
  (implies (and (n64p count)
                (n08-listp byte-list)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp (write-bytes count byte-list program-bytes))))

(verify-guards write-bytes)
(in-theory (disable write-bytes))

;;----------------------------------------------------------------------
; The QWORD assembler directive

(defun write-qwords (count qword-list symbol-table-alist program-bytes)
  ;; Directive:  (qword <list of qwords>)
  ;; Write elements of qword-list into memory program-bytes
  ;; beginning a address count.  Some of the elements may be symbols,
  ;; which we look up in the symbol-table-alist.
  (declare (xargs :guard (and (n64p count)
                              (symbol-or-n64-listp qword-list)
                              (symbol-n64p-alistp symbol-table-alist)
                              (n64p-n08p-alistp program-bytes))
                  :verify-guards nil))
  (if (atom qword-list)
      program-bytes
    (w64 count
	 ;; If the qword is a symbol, convert to address using the
	 ;; symbol-table.
         (if (symbolp (car qword-list))
             (label-address (car qword-list) symbol-table-alist)
           (car qword-list))
         (write-qwords (n64+ count 8)
                       (cdr qword-list)
                       symbol-table-alist
                       program-bytes))))

(defthm n64p-n08p-alistp-write-qwords
  (implies (and (n64p count)
                (symbol-or-n64-listp qword-list)
                (symbol-n64p-alistp symbol-table-alist)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-qwords count qword-list symbol-table-alist program-bytes))))

(verify-guards write-qwords)
(in-theory (disable write-qwords))

;;----------------------------------------------------------------------
; The CHAR assembler directive

;; Note that a character in ACL2 is represented by a member of the
;; list *standard-chars*.  Examples are: #\a, #\A, #\Newline, #\Space.
;; Function char-code converts a char to its ascii representation.

(defun write-chars (count char-list program-bytes)
  ;; Directive: (char <list of characters>)
  ;; Given a list of chars, write them into memory
  ;; proram-bytes beginning at address count. 
  (declare (xargs :guard (and (n64p count)
                              (character-listp char-list)
                              (n64p-n08p-alistp program-bytes))
                  :verify-guards nil))
  (if (atom char-list)
      program-bytes
    (w08 count
         (char-code (car char-list))
         (write-chars (n64+ count 1)
                      (cdr char-list)
                      program-bytes))))

(defthm n64p-n08p-alistp-write-chars
  (implies (and (n64p count)
                (character-listp char-list)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp (write-chars count char-list program-bytes))))

(verify-guards write-chars)
(in-theory (disable write-chars))

;;----------------------------------------------------------------------
; The STRING assembler directive

(defun write-str (count str program-bytes)
  ;; Directive: (string str)
  ;; Given a string, break into characters, and write
  ;; into memory beginning at address count. 
  (declare (xargs :guard (and (n64p count)
                              (stringp str)
                              (n64p-n08p-alistp program-bytes))))
  (write-chars count (coerce str 'list) program-bytes))

(defthm n64p-n08p-alistp-write-str
  (implies (and (n64p count)
                (stringp str)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp (write-str count str program-bytes))))

(in-theory (disable write-str))

;;----------------------------------------------------------------------
;;----------------------------------------------------------------------
;; For each of the remaining instructions, assemble the instruction
;; into machine code and write the bytes into memory program-bytes
;; beginning at location count. 

; Assemble NOP: #x10

(defun write-nop (count program-bytes)
  ;; Instruction: (nop)
  (declare (xargs :guard (and (n64p count)
                              (n64p-n08p-alistp program-bytes))))
  (w08 count
       (n04-n04-to-n08
        (cadr (assoc 'nop *Instructions*))   
        0)
       program-bytes))

(defthm n64p-n08p-alistp-write-nop
  (implies (and (n64p count)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp (write-nop count program-bytes))))

(in-theory (disable write-nop))

;;----------------------------------------------------------------------
; Assemble HALT: #x00

(defun write-halt (count program-bytes)
  ;; Instruction: (halt)
  (declare (xargs :guard (and (n64p count)
                              (n64p-n08p-alistp program-bytes))))
  (w08 count
       (n04-n04-to-n08
        (cadr (assoc 'halt *Instructions*))
        0)
       program-bytes))

(defthm n64p-n08p-alistp-write-halt
  (implies (and (n64p count)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp (write-halt count program-bytes))))

(in-theory (disable write-nop))

;;----------------------------------------------------------------------
; Assemble RRMOVQ: #x20, rArB

;; Note that rrmovq is actually a special case of cmovq.

(defun write-rrmovq (count RegA RegB program-bytes)
  ;; Instruction: (rrmovq regA regB)
  (declare (xargs :guard (and (n64p count)
                              (reg-id RegA)
                              (reg-id RegB)
                              (n64p-n08p-alistp program-bytes))))
  (let* ((pbytes (w08 count
                      (n04-n04-to-n08
                       (cadr (assoc 'rrmovq *Instructions*))
                       0)
                      program-bytes))
         (RegA-id (reg-id RegA))
         (RegB-id (reg-id RegB))
         (reg-specifier (n04-n04-to-n08 RegA-id RegB-id))
         (pbytes (w08 (n64+ count 1) reg-specifier pbytes)))
    pbytes))

(defthm n64p-n08p-alistp-write-rrmovq
  (implies (and (n64p count)
                (reg-id RegA)
                (reg-id RegB)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-rrmovq count RegA RegB program-bytes))))

(in-theory (disable write-rrmovq))

;;----------------------------------------------------------------------
; Assemble CMOVX: 
;   cmovle:  #x21 rArB
;   cmovl:   #x22 rArB
;   cmove:   #x23 rArB
;   cmovne:  #x24 rArB
;   cmovge:  #x25 rArB
;   cmovg:   #x26 rArB

(defun write-cmovq (count instruction RegA RegB program-bytes)
  ;; Instruction is any of:
  ;;    (cmovle rA rB)
  ;;    (cmovl  rA rB)
  ;;    (cmove  rA rB)
  ;;    (cmovne rA rB)
  ;;    (cmovge rA rB)
  ;;    (cmovg  rA rB)
  (declare (xargs :guard (and (n64p count)
                              (symbolp instruction)
                              (or (equal instruction 'cmovle)
                                  (equal instruction 'cmovl)
                                  (equal instruction 'cmove)
                                  (equal instruction 'cmovne)
                                  (equal instruction 'cmovge)
                                  (equal instruction 'cmovg))
                              (reg-id RegA)
                              (reg-id RegB)
                              (n64p-n08p-alistp program-bytes))))
  (let* ((pbytes (w08 count
                      (n04-n04-to-n08
                       (cadr (assoc instruction *Instructions*))
                       (caddr (assoc instruction *Instructions*)))
                      program-bytes))
         (RegA-id (reg-id RegA))
         (RegB-id (reg-id RegB))
         (reg-specifier (n04-n04-to-n08 RegA-id RegB-id))
         (pbytes (w08 (n64+ count 1) reg-specifier pbytes)))
    pbytes))

(defthm n64p-n08p-alistp-write-cmovq
  (implies (and (n64p count)
                (reg-id RegA)
                (reg-id RegB)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-cmovq count instruction RegA RegB program-bytes))))

(in-theory (disable write-cmovq))

;;----------------------------------------------------------------------
; Assemble IRMOVQ: #x30 FrB <8 bytes of immediate>

;; previously extended the machine code to support a
;; conditional version of this, but didn't extend the assembly
;; language correspondingly.  I removed the condition machine codes.
;; It would be easy to add them back, but would have to invent new
;; conditional immediate move operators for the assembly language.

(defun write-irmovq (count immediate RegB symbol-table-alist program-bytes)
  ;; Instruction is (irmovq immediate regB). 
  (declare (xargs :guard (and (n64p count)
                              (or (symbolp immediate)
                                  (n64p immediate))
                              (reg-id RegB)
                              (symbol-n64p-alistp symbol-table-alist)
                              (n64p-n08p-alistp program-bytes))))
  (let* ((pbytes (w08 count
                      (n04-n04-to-n08
                       (cadr (assoc 'irmovq *Instructions*))
                       0)
                      program-bytes))
         (RegB-id (reg-id RegB))
	 ;; Note rA is substituted by placeholder F.
         (reg-specifier (n04-n04-to-n08 15 RegB-id))
         (pbytes (w08 (n64+ count 1) reg-specifier pbytes))
         (label-addr (if (symbolp immediate)
                         (label-address immediate symbol-table-alist)
                       immediate))
         (pbytes (w64 (n64+ count 2) label-addr pbytes)))
    pbytes))

(defthm n64p-n08p-alistp-write-irmovq
  (implies (and (n64p count)
                (or (symbolp immediate)
                    (n64p immediate))
                (reg-id RegB)
                (symbol-n64p-alistp symbol-table-alist)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-irmovq count immediate RegB symbol-table-alist program-bytes))))

(in-theory (disable write-irmovq))

;;----------------------------------------------------------------------
; Assemble RMMOVQ: #x40 rArB <8 bytes of displacement>

(defun write-rmmovq (count RegA displacement RegB symbol-table-alist program-bytes)
  ;; Instruction: (rmmovq rA displacement (rB))
  (declare (xargs :guard (and (n64p count)
                              (reg-id RegA)
                              (or (symbolp displacement)
                                  (n64p displacement))
                              (reg-id RegB)
                              (symbol-n64p-alistp symbol-table-alist)
                              (n64p-n08p-alistp program-bytes))))
  (let* ((pbytes (w08 count
                      (n04-n04-to-n08
                       (cadr (assoc 'rmmovq *Instructions*))
                       0)
                      program-bytes))
         (RegA-id (reg-id RegA))
         (RegB-id (reg-id RegB))
         (reg-specifier (n04-n04-to-n08 RegA-id RegB-id))
         (pbytes (w08 (n64+ count 1) reg-specifier pbytes))
         (displace (if (symbolp displacement)
                       (label-address displacement symbol-table-alist)
                     displacement))
         (pbytes (w64 (n64+ count 2) displace pbytes)))
    pbytes))

(defthm n64p-n08p-alistp-write-rmmovq
  (implies (and (n64p count)
                (reg-id RegA)
                (or (symbolp displacement)
                    (n64p displacement))
                (reg-id RegB)
                (symbol-n64p-alistp symbol-table-alist)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-rmmovq count RegA displacement RegB
                          symbol-table-alist program-bytes))))

(in-theory (disable write-rmmovq))

;;----------------------------------------------------------------------
; Assemble MRMOVQ: #x50 rArB <8 bytes displacement>

(defun write-mrmovq (count displacement RegA RegB symbol-table-alist program-bytes)
  ;; Instruction: (mrmovq displacement (rB) rA)
  (declare (xargs :guard (and (n64p count)
                              (or (symbolp displacement)
                                  (n64p displacement))
                              (reg-id RegA)
                              (reg-id RegB)
                              (symbol-n64p-alistp symbol-table-alist)
                              (n64p-n08p-alistp program-bytes))))
  (let* ((pbytes (w08 count
                      (n04-n04-to-n08
                       (cadr (assoc 'mrmovq *Instructions*))
                       0)
                      program-bytes))
         (RegA-id (reg-id RegA))
         (RegB-id (reg-id RegB))
         (reg-specifier (n04-n04-to-n08 RegA-id RegB-id))
         (pbytes (w08 (n64+ count 1) reg-specifier pbytes))
         (displace (if (symbolp displacement)
                       (label-address displacement symbol-table-alist)
                     displacement))
         (pbytes (w64 (n64+ count 2) displace pbytes)))
    pbytes))

(defthm n64p-n08p-alistp-write-mrmovq
  (implies (and (n64p count)
                (or (symbolp displacement)
                    (n64p displacement))
                (reg-id RegA)
                (reg-id RegB)
                (symbol-n64p-alistp symbol-table-alist)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-mrmovq count displacement RegA RegB
                          symbol-table-alist program-bytes))))

(in-theory (disable write-mrmovq))

;;----------------------------------------------------------------------
; Assemble ADD, SUB, XOR, AND:
;   (addq rA rB): #x60 rArB
;   (subq rA rB): #x61 rArB
;   (andq rA rB): #x62 rArB
;   (xorq rA rB): #x63 rArB

(defun write-op (count instruction RegA RegB program-bytes)
  (declare (xargs :guard (and (n64p count)
                              (or (equal instruction 'xorq)
                                  (equal instruction 'andq)
                                  (equal instruction 'addq)
                                  (equal instruction 'subq))
                              (reg-id RegA)
                              (reg-id RegB)
                              (n64p-n08p-alistp program-bytes))))
  (let* ((pbytes (w08 count
                      (n04-n04-to-n08
                       (cadr (assoc instruction *Instructions*))
                       (caddr (assoc instruction *Instructions*)))
                      program-bytes))
         (RegA-id (reg-id RegA))
         (RegB-id (reg-id RegB))
         (reg-specifier (n04-n04-to-n08 RegA-id RegB-id))
         (pbytes (w08 (n64+ count 1) reg-specifier pbytes)))
    pbytes))

(defthm n64p-n08p-alistp-write-op
  (implies (and (n64p count)
                (reg-id RegA)
                (reg-id RegB)
                (symbol-n64p-alistp symbol-table-alist)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-op count instruction RegA RegB program-bytes))))

(in-theory (disable write-op))

;;----------------------------------------------------------------------
; Assemble JMP or conditional JMP:
;   jmp:  #x70 <8 bytes of destination>
;   jle:  #x71 <8 bytes of destination>
;   jl:   #x72 <8 bytes of destination>
;   je:   #x73 <8 bytes of destination>
;   jne:  #x74 <8 bytes of destination>
;   jge:  #x75 <8 bytes of destination>
;   jg:   #x76 <8 bytes of destination>

(defun write-jmp (count instruction label symbol-table-alist program-bytes)
  (declare (xargs :guard (and (n64p count)
                              (symbolp instruction)
                              (or (equal instruction 'jmp)
                                  (equal instruction 'jle)
                                  (equal instruction 'jl)
                                  (equal instruction 'je)
                                  (equal instruction 'jne)
                                  (equal instruction 'jge)
                                  (equal instruction 'jg))
                              (or (symbolp label)
                                  (n64p label))
                              (symbol-n64p-alistp symbol-table-alist)
                              (n64p-n08p-alistp program-bytes))))
  (let* ((pbytes (w08 count
                      (n04-n04-to-n08
                       (cadr (assoc instruction *Instructions*))
                       (caddr (assoc instruction *Instructions*)))
                      program-bytes))
         (label-addr (if (symbolp label)
                         (label-address label symbol-table-alist)
                       label))
         (pbytes (w64 (n64+ count 1) label-addr pbytes)))
    pbytes))

(defthm n64p-n08p-alistp-write-jmp
  (implies (and (n64p count)
                (or (symbolp label)
                    (n64p label))
                (symbol-n64p-alistp symbol-table-alist)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-jmp count instruction label symbol-table-alist program-bytes))))

(in-theory (disable write-jmp))

;;----------------------------------------------------------------------
; Assemble subroutine CALL:
;    (call foo): #x80 <8 bytes of destination>

(defun write-call (count label symbol-table-alist program-bytes)
  ;; Instruction: (call destination)
  (declare (xargs :guard (and (n64p count)
                              (or (symbolp label)
                                  (n64p label))
                              (symbol-n64p-alistp symbol-table-alist)
                              (n64p-n08p-alistp program-bytes))))
  (let* ((pbytes (w08 count
                      (n04-n04-to-n08
                       (cadr (assoc 'call *Instructions*))
                       0)
                      program-bytes))
         (label-addr (if (symbolp label)
                         (label-address label symbol-table-alist)
                       label))
         (pbytes (w64 (n64+ count 1) label-addr pbytes)))
    pbytes))

(defthm n64p-n08p-alistp-write-call
  (implies (and (n64p count)
                (or (symbolp label)
                    (n64p label))
                (symbol-n64p-alistp symbol-table-alist)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-call count label symbol-table-alist program-bytes))))

(in-theory (disable write-call))

;;----------------------------------------------------------------------
; Assemble subroutine RETurn:
;   (ret):  #x90

(defun write-ret (count program-bytes)
  ;; Instruction: (ret)
  (declare (xargs :guard (and (n64p count)
                              (n64p-n08p-alistp program-bytes))))
  (w08 count
       (n04-n04-to-n08
        (cadr (assoc 'ret *Instructions*))
        0)
       program-bytes))

(defthm n64p-n08p-alistp-write-ret
  (implies (and (n64p count)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-ret count program-bytes))))

(in-theory (disable write-ret))

;;----------------------------------------------------------------------
; Assemble PUSHQ onto the stack:
;   (pushq rA): #xA0 rAF

(defun write-pushq (count RegA program-bytes)
  ;; Instruction:  (pushq rA)
  (declare (xargs :guard (and (n64p count)
                              (reg-id RegA)
                              (n64p-n08p-alistp program-bytes))))
  (let* ((pbytes (w08 count
                      (n04-n04-to-n08
                       (cadr (assoc 'pushq *Instructions*))
                       0)
                      program-bytes))
         (RegA-id (reg-id RegA))
	 ;; >> Note rB is substituted by placeholder F.
         (reg-specifier (n04-n04-to-n08 RegA-id 15))
         (pbytes (w08 (n64+ count 1) reg-specifier pbytes)))
    pbytes))

(defthm n64p-n08p-alistp-write-pushq
  (implies (and (n64p count)
                (reg-id RegA)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-pushq count RegA program-bytes))))

(in-theory (disable write-pushq))

;;----------------------------------------------------------------------
; Assemble POPQ from the stack:
;   (popq rA):  #xB0 raF

(defun write-popq (count RegA program-bytes)
  ;; Instruction:  (popq rA)
  (declare (xargs :guard (and (n64p count)
                              (reg-id RegA)
                              (n64p-n08p-alistp program-bytes))))
  (let* ((pbytes (w08 count
                      (n04-n04-to-n08
                       (cadr (assoc 'popq *Instructions*))
                       0)
                      program-bytes))
         (RegA-id (reg-id RegA))
	 ;; >> Note rB is substituted by placeholder F.
         (reg-specifier (n04-n04-to-n08 RegA-id 15))
         (pbytes (w08 (n64+ count 1) reg-specifier pbytes)))
    pbytes))

(defthm n64p-n08p-alistp-write-popq
  (implies (and (n64p count)
                (reg-id RegA)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-popq count RegA program-bytes))))

(in-theory (disable write-popq))

;;----------------------------------------------------------------------
; This was conditional in the machine language, but
;  there were no corresponding assembly operators.  I made it
;  non-conditional, at least for this first cut.

; Assemble IADDQ (immediate add):
;   (iaddq #xC0

(defun write-iaddq (count immediate RegB symbol-table-alist program-bytes)
  ;; Instruction:  (iaddq immediate rB)
  (declare (xargs :guard (and (n64p count)
                              (or (symbolp immediate)
                                  (n64p immediate))
                              (reg-id RegB)
                              (symbol-n64p-alistp symbol-table-alist)
                              (n64p-n08p-alistp program-bytes))))
  (let* ((pbytes (w08 count
                      (n04-n04-to-n08
                       (cadr (assoc 'iaddq *Instructions*))
                       0)
                      program-bytes))
         (RegB-id (reg-id RegB))
	 ;; >> Note rA is substituted by placeholder F.
         (reg-specifier (n04-n04-to-n08 15 RegB-id))
         (pbytes (w08 (n64+ count 1) reg-specifier pbytes))
         (label-addr (if (symbolp immediate)
                         (label-address immediate symbol-table-alist)
                       immediate))
         (pbytes (w64 (n64+ count 2) label-addr pbytes)))
    pbytes))

(defthm n64p-n08p-alistp-write-iaddq
  (implies (and (n64p count)
                (or (symbolp immediate)
                    (n64p immediate))
                (reg-id RegB)
                (symbol-n64p-alistp symbol-table-alist)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-iaddq count immediate RegB symbol-table-alist program-bytes))))

(in-theory (disable write-iaddq))

;;----------------------------------------------------------------------
; Assemble LEAVE:
;  (leave):  #xD0

(defun write-leave (count program-bytes)
  ;; Instruction:  (leave)
  (declare (xargs :guard (and (n64p count)
                              (n64p-n08p-alistp program-bytes))))
  (w08 count
       (n04-n04-to-n08
        (cadr (assoc 'leave *Instructions*))
        0)
       program-bytes))

(defthm n64p-n08p-alistp-write-leave
  (implies (and (n64p count)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-leave count program-bytes))))

(in-theory (disable write-leave))

;;----------------------------------------------------------------------
; Assemble NOOP: 
;  (noop): #xF0

(defun write-noop (count program-bytes)
  (declare (xargs :guard (and (n64p count)
                              (n64p-n08p-alistp program-bytes))))
  (w08 count
       (n04-n04-to-n08
        (cadr (assoc 'noop *Instructions*))
        0)
       program-bytes))

(defthm n64p-n08p-alistp-write-noop
  (implies (and (n64p count)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (write-noop count program-bytes))))

(in-theory (disable write-noop))

;;----------------------------------------------------------------------
;;----------------------------------------------------------------------
; Y86 ASSEMBLER
;  program:        program being assembled
;  count:          current address
;  symbol-table:   symbol table
;  program-bytes:  list of machine code bytes being created

(defun y86-asm (program count symbol-table-alist program-bytes)
  (declare (xargs :guard (and (y86-prog program)
                              (n64p count)
                              (symbol-n64p-alistp symbol-table-alist)
                              (n64p-n08p-alistp program-bytes))
                  ;; Guard debug can slow things tremendously.
                  ;; :guard-debug nil
                  :guard-hints
                  (("Goal" :do-not '(preprocess)))))
  (if (atom program)
      program-bytes
    (let ((label-or-instruction (car program))
          (rest-program (cdr program)))
      (if (atom label-or-instruction)
          (y86-asm rest-program count symbol-table-alist program-bytes)
        (let ((instruction (car label-or-instruction))
              (args (cdr label-or-instruction)))
          (case instruction
            (pos
             (y86-asm rest-program (car args)
                      symbol-table-alist program-bytes))
            (align
             (y86-asm rest-program
                      (align-to-mod-n count (car args))
                      symbol-table-alist program-bytes))
            (byte
             (y86-asm rest-program (n64+ count (len args))
                      symbol-table-alist
                      (write-bytes count args
                                   program-bytes)))
            (qword
             (y86-asm rest-program (n64+ count (* 8 (len args)))
                      symbol-table-alist
                      (write-qwords count args
                                    symbol-table-alist program-bytes)))
            (char
             (y86-asm rest-program (n64+ count (len args))
                      symbol-table-alist
                      (write-chars count args program-bytes)))
            (string
             (y86-asm rest-program (n64+ count (length (car args)))
                      symbol-table-alist
                      (write-str count (car args) program-bytes)))
            (space
             (y86-asm rest-program (n64+ count (car args))
                      symbol-table-alist
                      program-bytes))
            (nop
             (y86-asm rest-program (n64+ count 1) symbol-table-alist
                      (write-nop count program-bytes)))
            (halt
             (y86-asm rest-program (n64+ count 1) symbol-table-alist
                      (write-halt count program-bytes)))
            (rrmovq
             (y86-asm rest-program (n64+ count 2) symbol-table-alist
                      (write-rrmovq count
                                    (car args)
                                    (cadr args)
                                    program-bytes)))

            ((cmovle cmovl cmove cmovne cmovge cmovg)
             (y86-asm rest-program (n64+ count 2) symbol-table-alist
                      (write-cmovq count
                                   instruction
                                   (car args)
                                   (cadr args)
                                   program-bytes)))
            (irmovq
             (y86-asm rest-program (n64+ count 10) symbol-table-alist
                      (write-irmovq count
                                    (car args)
                                    (cadr args)
                                    symbol-table-alist
                                    program-bytes)))
            (rmmovq
             (y86-asm rest-program (n64+ count 10) symbol-table-alist
                      (write-rmmovq count
                                    (car args)
                                    (cadr args)
                                    (car (caddr args))
                                    symbol-table-alist
                                    program-bytes)))
            (mrmovq
             (y86-asm rest-program (n64+ count 10) symbol-table-alist
                      (write-mrmovq count
                                    (car args)
                                    (caddr args)
                                    (car (cadr args))
                                    symbol-table-alist
                                    program-bytes)))

            ((addq subq andq xorq)
             (y86-asm rest-program (n64+ count 2) symbol-table-alist
                      (write-op count instruction (car args) (cadr args)
                                program-bytes)))

            ((jmp jle jl je jne jge jg)
             (y86-asm rest-program (n64+ count 9) symbol-table-alist
                      (write-jmp count instruction (car args)
                                 symbol-table-alist program-bytes)))
            (call
             (y86-asm rest-program (n64+ count 9) symbol-table-alist
                      (write-call count
                                  (car args)
                                  symbol-table-alist program-bytes)))
            (ret
             (y86-asm rest-program (n64+ count 1) symbol-table-alist
                      (write-ret count program-bytes)))
            (pushq
             (y86-asm rest-program (n64+ count 2) symbol-table-alist
                      (write-pushq count (car args) program-bytes)))
            (popq
             (y86-asm rest-program (n64+ count 2) symbol-table-alist
                      (write-popq count (car args) program-bytes)))
            (iaddq
             (y86-asm rest-program (n64+ count 10) symbol-table-alist
                      (write-iaddq count
                                   (car args)
                                   (cadr args)
                                   symbol-table-alist
                                   program-bytes)))
            (leave
             (y86-asm rest-program (n64+ count 1) symbol-table-alist
                      (write-leave count program-bytes)))
            (noop
             (y86-asm rest-program (n64+ count 1) symbol-table-alist
                      (write-noop count program-bytes)))
            (otherwise program-bytes)))))))

(defthm n64p-n08p-alistp-write-y86-asm
  (implies (and (y86-prog program)
                (n64p count)
                (symbol-n64p-alistp symbol-table-alist)
                (n64p-n08p-alistp program-bytes))
           (n64p-n08p-alistp
            (y86-asm program count symbol-table-alist program-bytes))))

(in-theory (disable y86-asm))


