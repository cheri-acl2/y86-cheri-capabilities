# CHERI-capabilities simulation and verification
y86 enhanced with Cheri Capabilities

## Description
This repository contains a y86 model enhanced with in-memory Cheri capabilities, along with verified example programs showcasing the modified model. All code in this repository is written in ACL2.

**capabilities.lisp**: definitions of memory and architectural capabilities

**capability-instructions.lisp**: y86-instructions enhanced with capabilities, where proofs of correctness are included as well